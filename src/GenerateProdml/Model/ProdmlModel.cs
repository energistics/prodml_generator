 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Energistics.DataAccess.PRODML200;
using Energistics.DataAccess.PRODML200.ComponentSchemas;
using Energistics.DataAccess.PRODML200.ReferenceData;
using System.Windows.Forms;

namespace GenerateProdml.Model
{
    class ProdmlModel
    {
        #region Get Production Data

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public DataTable getProdData(string connectionString, string query, string prodDate, string assetName, string platformName, string reserviorName, bool hasParam) // Can output Data from here to Interface IModel // Will have to change the params from start date, end date to param1, param2 to make it more generic
        {
            DataTable dtProdData = new DataTable();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    // Create the command and set its properties.
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = query;
                    command.CommandTimeout = 60;

                    if (hasParam) // also need to put null string condition here
                    {
                        // Add the input parameters and set their properties.
                        SqlParameter param1 = new SqlParameter();
                        param1.ParameterName = "@reportDate";
                        param1.SqlDbType = SqlDbType.DateTime;
                        param1.Direction = ParameterDirection.Input;
                        param1.Value = prodDate;

                        SqlParameter param2 = new SqlParameter();
                        param2.ParameterName = "@assetName";
                        param2.SqlDbType = SqlDbType.NVarChar;
                        param2.Direction = ParameterDirection.Input;
                        param2.Value = assetName;

                        SqlParameter param3 = new SqlParameter();
                        param3.ParameterName = "@platformName";
                        param3.SqlDbType = SqlDbType.NVarChar;
                        param3.Direction = ParameterDirection.Input;
                        param3.Value = platformName;

                        SqlParameter param4 = new SqlParameter();
                        param4.ParameterName = "@reserviorName";
                        param4.SqlDbType = SqlDbType.NVarChar;
                        param4.Direction = ParameterDirection.Input;
                        param4.Value = reserviorName;

                        // Add the parameters to the collection. 
                        command.Parameters.Add(param1);
                        command.Parameters.Add(param2);
                        command.Parameters.Add(param3);
                        command.Parameters.Add(param4);
                    }

                    // Open the connection and execute the reader.
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    dtProdData.Load(reader);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(string.Format("An exception occurred while retrieving data from the data source. {0}", ex.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    log.Error("A problem occurred while getting data from source. ", ex);
                }
            }
            return dtProdData;
        }

        #endregion

        #region Get Well Test Production Test Data Object

        public WellTestProductionTestData getWellTestProdTestDataObject()
        {
            //WellTestData
            WellTestProductionTestData WellTestProdTestDataObj = new WellTestProductionTestData();

            //BottomHoleData
            WellTestProdTestDataObj.BottomholeData = new WellTestBottomholeData(); //BottomholeData initialization
            WellTestProdTestDataObj.BottomholeData.BottomholePres = new PressureMeasure(); //BottomholePres initialization

            //ProductionTestResults
            //BasicSedimentAndWater
            WellTestProdTestDataObj.ProductionTestResults = new WellTestProductionTestResults(); //ProductionTestResults initialization
            WellTestProdTestDataObj.ProductionTestResults.BasicSedimentAndWater = new VolumePerVolumeMeasure(); //BasicSedimentAndWater initialization

            // Rates commented
            ////WaterRate
            ////FluidRate
            //WellTestProdTestDataObj.ProductionTestResults.WaterRate = new WellTestFluidRate(); //WaterRate intialization
            //VolumePerTimeMeasure waterRateMeasureObj = new VolumePerTimeMeasure();
            //WellTestProdTestDataObj.ProductionTestResults.WaterRate.FluidRate = waterRateMeasureObj;

            ////OilRate
            ////FluidRate
            //WellTestProdTestDataObj.ProductionTestResults.OilRate = new WellTestFluidRate(); //OilRate initialization
            //VolumePerTimeMeasure oilRateMeasureObj = new VolumePerTimeMeasure();
            //WellTestProdTestDataObj.ProductionTestResults.OilRate.FluidRate = oilRateMeasureObj;

            ////GasVolume
            ////Volume
            //WellTestProdTestDataObj.ProductionTestResults.GasRate = new WellTestFluidRate(); //GasRate initialization
            //VolumePerTimeMeasure gasRateMeasureObj = new VolumePerTimeMeasure();
            //WellTestProdTestDataObj.ProductionTestResults.GasRate.FluidRate = gasRateMeasureObj;

            //WaterVolume
            //Volume
            WellTestProdTestDataObj.ProductionTestResults.WaterVolume = new WellTestTestVolume(); //WaterVolume intialization
            VolumeMeasure waterVolMeasureObj = new VolumeMeasure();
            WellTestProdTestDataObj.ProductionTestResults.WaterVolume.Volume = waterVolMeasureObj;

            //OilVolume
            //Volume
            WellTestProdTestDataObj.ProductionTestResults.OilVolume = new WellTestTestVolume(); //OilRate initialization
            VolumeMeasure oilVolMeasureObj = new VolumeMeasure();
            WellTestProdTestDataObj.ProductionTestResults.OilVolume.Volume = oilVolMeasureObj;

            //GasVolume
            //Volume
            WellTestProdTestDataObj.ProductionTestResults.GasVolume = new WellTestTestVolume(); //GasRate initialization
            VolumeMeasure gasVolMeasureObj = new VolumeMeasure();
            WellTestProdTestDataObj.ProductionTestResults.GasVolume.Volume = gasVolMeasureObj;

            //WellHeadData
            //Temperature
            WellTestProdTestDataObj.WellheadData = new WellTestWellheadData(); //WellheadData initialization
            WellTestProdTestDataObj.WellheadData.Temperature = new ThermodynamicTemperatureMeasure(); //Temperature initialization

            //ChokeOrificeSize
            WellTestProdTestDataObj.WellheadData.ChokeOrificeSize = new LengthMeasure(); //ChokeOrificeSize initialization

            return WellTestProdTestDataObj;
        }

        #endregion

        #region Insert Data

        public void WriteToDatabase(string connString, string tableName, DataTable dtProdData)
        {
            using (SqlConnection connection = new SqlConnection(connString))
            {
                SqlBulkCopy bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction, null);
                bulkCopy.DestinationTableName = tableName;
                bulkCopy.BatchSize = 100;

                foreach (DataColumn col in dtProdData.Columns) // Setting up columns mapping
                {
                    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                }

                connection.Open();
                bulkCopy.WriteToServer(dtProdData);
                connection.Close();
            }
            dtProdData.Clear();
        }

        #endregion

    }
}
