 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using GenerateProdml.View;
using GenerateProdml.Presenter;

namespace GenerateProdml
{
    public partial class ProdmlView : Form, IProdmlView
    {
        private ProdmlPresenter presnterObj;
        public event EventHandler cutMenuEventHandler;
        public event EventHandler copyMenuEventHandler;
        public event EventHandler copyWithHeadersMenuEventHandler;
        public event EventHandler pasteMenuEventHandler;
        public event EventHandler deleteMenuEventHandler;
        public event EventHandler sourceTypeEventHandler;
        public event EventHandler dataGridViewModeEventHandler;
        public event DataGridViewDataErrorEventHandler dataErrorEventHandler;

        public ProdmlView()
        {
            InitializeComponent();
        }

        private void ProdmlView_Load(object sender, EventArgs e)
        {
            presnterObj = new ProdmlPresenter(this);

            //Setting tooltip text for buttons and radio buttons
            toolTip_btn.SetToolTip(this.btn_imp_prodml, "Upload data to the selected data store.");
            toolTip_btn.SetToolTip(this.btn_download_data, "Download data to csv files.");
            toolTip_btn.SetToolTip(this.btn_exec_qry, "Get data for the selected report date.");
            toolTip_btn.SetToolTip(this.btn_exp_prodml, "Generate xml(s) in the PRODML format.");
            toolTip_btn.SetToolTip(this.btn_login, "Log in to the data store.");
            toolTip_btn.SetToolTip(this.rb_src_db, "Choose to retrieve data from relational database.");
            toolTip_btn.SetToolTip(this.rb_src_files, "Choose to retrieve data from xml files in the PRODML format.");
            toolTip_btn.SetToolTip(this.cbx_export_rep_xmls, "Check this to export Reporting Hierarchy & Reporting Entity xml(s).");
        }

        public string reportDate
        {
            get { return dtp_report_dte.Text; }
            set { dtp_report_dte.Text = value; }
        }

        public DataGridView dgvEquipRelibltyData 
        {
            get { return dg_equip_reliblty; }
            set { dg_equip_reliblty = value; }
        }

        public DataGridView dgvProdInfoData
        {
            get { return dg_prod_info; }
            set { dg_prod_info= value; }
        }

        public DataGridView dgvWellTestData 
        {
            get { return dg_well_test; }
            set { dg_well_test = value; }
        }

        public TreeNode hierarchyTopNode
        {
            get { return tv_hierarchy.TopNode; }
            set { tv_hierarchy.Nodes.Add(value); }
        }

        public List<string> srcName
        {
            set { cb_data_src.DataSource = value; }
        }

        public string selectedSrcName
        {
            get { return cb_data_src.SelectedItem.ToString(); }
        }

        public string citationDesc
        {
            get { return tb_citation_description.Text; }
            set { tb_citation_description.Text = value; }
        }

        public string equipDowntimeLabel
        {
            get { return gb_equip_downtime.Text; }
        }

        public string prodLabel
        {
            get { return gb_production.Text; }
        }

        public string validWellTestLabel
        {
            get { return gb_valid_well_test.Text; }
        }

        public string loginUserName
        {
            get { return tb_user_nme.Text; }
            set { tb_user_nme.Text = value; }
        }

        public string loginPassword
        {
            get { return tb_password.Text; }
            set { tb_password.Text = value; }
        }
        
        public bool isLoginPanel
        {
            get { return pnl_login_info.Visible; }
            set { pnl_login_info.Visible = value; }
        }

        public bool isLoginMessage
        {
            get { return lbl_login_msg.Visible; }
            set { lbl_login_msg.Visible = value; }
        }

        public bool isTreeEnabled
        {
            get { return gb_tree.Enabled; }
            set { gb_tree.Enabled = value; }
        }

        public bool isDgvPanelEnabled
        {
            get { return tble_layout_panel.Enabled; }
            set { tble_layout_panel.Enabled = value; }
        }

        public bool isCitationEnabled
        {
            get { return tb_citation_description.Enabled; }
            set { tb_citation_description.Enabled = value; }
        }

        public bool isRprtDteEnabled
        {
            get { return dtp_report_dte.Enabled; }
            set { dtp_report_dte.Enabled = value; }
        }

        public bool isGetDataEnabled
        {
            get { return btn_exec_qry.Enabled; }
            set { btn_exec_qry.Enabled = value; }
        }

        public bool isExportProdmlEnabled
        {
            get { return btn_exp_prodml.Enabled; }
            set { btn_exp_prodml.Enabled = value; }
        }

        public bool isImportProdmlEnabled
        {
            get { return btn_imp_prodml.Enabled; }
            set { btn_imp_prodml.Enabled = value; }
        }

        public bool isExpHierarchyEntityEnabled
        {
            get { return cbx_export_rep_xmls.Enabled; }
            set { cbx_export_rep_xmls.Enabled = value; }
        }

        public bool isSourceDbEnabled
        {
            get { return rb_src_db.Enabled; }
            set { rb_src_db.Enabled = value; }
        }

        public bool isSourceFilesEnabled
        {
            get { return rb_src_files.Enabled; }
            set { rb_src_files.Enabled = value; }
        }

        public bool isExportHierarchyEntity
        {
            get { return cbx_export_rep_xmls.Checked; }
            set { cbx_export_rep_xmls.Checked = value; }
        }

        public bool isSourceDb
        {
            get { return rb_src_db.Checked; }
            set { rb_src_db.Checked = value; }
        }

        public bool isSourceFiles
        {
            get { return rb_src_files.Checked; }
            set { rb_src_files.Checked = value; }
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            presnterObj.logInToSource();
        }

        private void btn_exec_qry_Click(object sender, EventArgs e) //Exec query
        {
            tv_hierarchy.Nodes.Clear(); // To clear all the nodes every time the query is called
            presnterObj.setProdData();
            if (tv_hierarchy.TopNode != null) // To make the Tree hierarchy nodes' tool tip visible
            {
                tv_hierarchy.ShowNodeToolTips = true;
                tv_hierarchy.ExpandAll();
            }
        }

        private void btn_exp_prodml_Click(object sender, EventArgs e) // Export as PRODML
        {
            presnterObj.browseExportFileDir();
        }

        private void btn_imp_prodml_Click(object sender, EventArgs e) // Import data to database
        {
            presnterObj.updateData();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cutMenuEventHandler(sender, e);
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            copyMenuEventHandler(sender, e);
        }

        private void copyWithHeadersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            copyWithHeadersMenuEventHandler(sender, e);
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pasteMenuEventHandler(sender, e);
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            deleteMenuEventHandler(sender, e);
        }

        private void dg_equip_reliblty_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            dataErrorEventHandler(sender,e);
        }
        
        private void dg_prod_info_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            dataErrorEventHandler(sender, e);
        }

        private void dg_well_test_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            dataErrorEventHandler(sender, e);
        }

        private void rb_src_db_CheckedChanged(object sender, EventArgs e)
        {
            tv_hierarchy.Nodes.Clear(); // Clear the nodes if the source selection has changed
            sourceTypeEventHandler(sender, e);
        }

        private void rb_src_files_CheckedChanged(object sender, EventArgs e)
        {
            tv_hierarchy.Nodes.Clear(); // Clear the nodes if the source selection has changed
            sourceTypeEventHandler(sender, e);
        }

        private void dtp_report_dte_ValueChanged(object sender, EventArgs e)
        {
            presnterObj.clearDataGrids();
        }

        private void cb_data_src_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (presnterObj != null)
            {
                tv_hierarchy.Nodes.Clear(); // Clear the nodes if the asset selection has changed
                presnterObj.selectedAssetChanged();
            }
        }

        private void btn_download_data_Click(object sender, EventArgs e)
        {
            presnterObj.browseDownloadFileDir();
        }

        private void cbx_user_entry_CheckedChanged(object sender, EventArgs e)
        {
            dataGridViewModeEventHandler(sender, e);
            tv_hierarchy.Nodes.Clear();
        }

        private void panel_buttons_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tble_layout_panel_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}