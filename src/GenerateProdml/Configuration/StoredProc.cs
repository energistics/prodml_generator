 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace GenerateProdml.Configuration
{
    class StoredProc : ConfigurationElement
    {
        #region Fields

        private static ConfigurationPropertyCollection _properties;
        private static ConfigurationProperty _propId;
        //private static ConfigurationProperty _propOrder;
        private static ConfigurationProperty _propQueryType;
        private static ConfigurationProperty _propDestName;
        private static ConfigurationProperty _propDescription;

        #endregion

        #region Constructor

        static StoredProc()
        {
            _propId = new ConfigurationProperty(
                "id",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);

            //_propOrder = new ConfigurationProperty(
            //    "order",
            //    typeof(int),
            //    null,
            //    ConfigurationPropertyOptions.IsRequired);

            _propQueryType = new ConfigurationProperty(
                 "querytype",
                 typeof(string),
                 null,
                 ConfigurationPropertyOptions.IsRequired);

            _propDestName = new ConfigurationProperty(
                "destname",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);

            _propDescription = new ConfigurationProperty(
                "description",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);

            _properties = new ConfigurationPropertyCollection();
            _properties.Add(_propId);
            //_properties.Add(_propOrder);
            _properties.Add(_propQueryType);
            _properties.Add(_propDestName);
            _properties.Add(_propDescription);

        }

        #endregion

        #region Properties

        public string Id
        {
            get { return (string)base[_propId]; }
            set { base[_propId] = value; }
        }

        //public int Order
        //{
        //    get { return (int)base[_propOrder]; }
        //    set { base[_propOrder] = value; }
        //}

        public string QueryType
        {
            get { return (string)base[_propQueryType]; }
            set { base[_propQueryType] = value; }
        }

        public string DestName
        {
            get { return (string)base[_propDestName]; }
            set { base[_propDestName] = value; }
        }

        public string Description
        {
            get { return (string)base[_propDescription]; }
            set { base[_propDescription] = value; }
        }

        #endregion

        #region Overrides

        protected override ConfigurationPropertyCollection Properties
        {
            get
            {
                return _properties;
            }
        }

        #endregion
    }
}
