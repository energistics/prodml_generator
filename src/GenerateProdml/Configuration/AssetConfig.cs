 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace GenerateProdml.Configuration
{
    public class AssetConfig : ConfigurationSection
    {
        private AssetConfig() { }

        [ConfigurationProperty("asset", DefaultValue = "")]
        public string AssetName
        {
            get { return this["asset"].ToString(); }
            set { this["asset"] = value; }
        }

        [ConfigurationProperty("platform", DefaultValue = "")]
        public string PlatformName
        {
            get { return this["platform"].ToString(); }
            set { this["platform"] = value; }
        }

        [ConfigurationProperty("reservior", DefaultValue = "")]
        public string ReserviorName
        {
            get { return this["reservior"].ToString(); }
            set { this["reservior"] = value; }
        }

        [ConfigurationProperty("inputparams", DefaultValue = "")]
        public string InputParameters
        {
            get { return this["inputparams"].ToString(); }
            set { this["inputparams"] = value; }
        }
    }
}
