 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace GenerateProdml.Configuration
{
    class GridColumn : ConfigurationElement
    {
        #region Fields

        private static ConfigurationPropertyCollection _properties;
        private static ConfigurationProperty _propShortName;
        private static ConfigurationProperty _propLongName;
        private static ConfigurationProperty _propSourceName;
        private static ConfigurationProperty _propDataType;
        private static ConfigurationProperty _propUom;
        private static ConfigurationProperty _propEnergyUom;
        private static ConfigurationProperty _propElement;
        private static ConfigurationProperty _propReportType;
        
        #endregion

        #region Constructor

        static GridColumn()
        {
            _propShortName = new ConfigurationProperty(
                "shortname",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);

            _propLongName = new ConfigurationProperty(
                "longname",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);

            _propSourceName = new ConfigurationProperty(
                "srcname",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);

            _propDataType = new ConfigurationProperty(
                "datatype",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);

            _propUom = new ConfigurationProperty(
                 "uom",
                 typeof(string),
                 null,
                 ConfigurationPropertyOptions.IsRequired);

            _propEnergyUom = new ConfigurationProperty(
                "energyuom",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);

            _propElement = new ConfigurationProperty(
                "element",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);

            _propReportType = new ConfigurationProperty(
                "reporttype",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);


            _properties = new ConfigurationPropertyCollection();
            _properties.Add(_propShortName);
            _properties.Add(_propLongName);
            _properties.Add(_propSourceName);
            _properties.Add(_propDataType);
            _properties.Add(_propUom);
            _properties.Add(_propEnergyUom);
            _properties.Add(_propElement);
            _properties.Add(_propReportType);
        }

        #endregion

        #region Properties

        public string ShortName
        {
            get { return (string)base[_propShortName]; }
            set { base[_propShortName] = value; }
        }

        public string LongName
        {
            get { return (string)base[_propLongName]; }
            set { base[_propLongName] = value; }
        }

        public string SourceName
        {
            get { return (string)base[_propSourceName]; }
            set { base[_propSourceName] = value; }
        }

        public string DataType
        {
            get { return (string)base[_propDataType]; }
            set { base[_propDataType] = value; }
        }

        public string Uom
        {
            get { return (string)base[_propUom]; }
            set { base[_propUom] = value; }
        }

        public string EnergyUom
        {
            get { return (string)base[_propEnergyUom]; }
            set { base[_propEnergyUom] = value; }
        }

        public string Element
        {
            get { return (string)base[_propElement]; }
            set { base[_propElement] = value; }
        }

        public string ReportType
        {
            get { return (string)base[_propReportType]; }
            set { base[_propReportType] = value; }
        }

        #endregion

        #region Overrides

        protected override ConfigurationPropertyCollection Properties
        {
            get
            {
                return _properties;
            }
        }

        #endregion
    }
}
