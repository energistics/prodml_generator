 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GenerateProdml.Presenter;
using GenerateProdml.View;

namespace GenerateProdml.View
{
    public partial class PvtView : Form, IPvtView
    {
        private PvtPresenter presenterObj;
        public event EventHandler copyMenuEventHandler;
        public event EventHandler copyWithHeadersMenuEventHandler;
        public event EventHandler pasteMenuEventHandler;

        public PvtView()
        {
            InitializeComponent();
        }

        private void PvtView_Load(object sender, EventArgs e)
        {
            presenterObj = new PvtPresenter(this);
        }

        public DataGridView dgvDataSet0
        {
            get { return dg_dataset0; }
            set { dg_dataset0 = value; }
        }

        public DataGridView dgvDataSet1
        {
            get { return dg_dataset1; }
            set { dg_dataset1 = value; }
        }
        public DataGridView dgvDataSet2
        {
            get { return dg_dataset2; }
            set { dg_dataset2 = value; }
        }
        public DataGridView dgvDataSet3
        {
            get { return dg_dataset3; }
            set { dg_dataset3 = value; }
        }
        public DataGridView dgvDataSet4
        {
            get { return dg_dataset4; }
            set { dg_dataset4 = value; }
        }
        public DataGridView dgvDataSet5
        {
            get { return dg_dataset5; }
            set { dg_dataset5 = value; }
        }

        public bool isSet0Visible
        {
            get { return dg_dataset0.Visible; }
            set { dg_dataset0.Visible = value; }
        }

        public bool isTabCtrVisible
        {
            get { return tab_data.Visible; }
            set { tab_data.Visible = value; }
        }

        public string testDescName
        {
            get { return gb_test_typ.Text; }
            set { gb_test_typ.Text = value; }
        }

        public bool isZFselected
        {
            get { return rb_zeroflash.Checked; }
            set { rb_zeroflash.Checked = value; }
        }

        public bool isSATselected
        {
            get { return rb_saturation.Checked; }
            set { rb_saturation.Checked = value; }
        }

        public bool isCCEselected
        {
            get { return rb_cce.Checked; }
            set { rb_cce.Checked = value; }
        }

        public bool isDLselected
        {
            get { return rb_difflab.Checked; }
            set { rb_difflab.Checked = value; }
        }

        public bool isCVDselected
        {
            get { return rb_cvd.Checked; }
            set { rb_cvd.Checked = value; }
        }

        public bool isSWEselected
        {
            get { return rb_swelling.Checked; }
            set { rb_swelling.Checked = value; }
        }

        private void dgvDataSet0_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Exception != null || (e.Context == DataGridViewDataErrorContexts.Commit))
            {
                MessageBox.Show(string.Format("{0} Enter a valid value.", e.Exception.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvDataSet1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Exception != null || (e.Context == DataGridViewDataErrorContexts.Commit))
            {
                MessageBox.Show(string.Format("{0} Enter a valid value.", e.Exception.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvDataSet2_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Exception != null || (e.Context == DataGridViewDataErrorContexts.Commit))
            {
                MessageBox.Show(string.Format("{0} Enter a valid value.", e.Exception.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvDataSet3_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Exception != null || (e.Context == DataGridViewDataErrorContexts.Commit))
            {
                MessageBox.Show(string.Format("{0} Enter a valid value.", e.Exception.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvDataSet4_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Exception != null || (e.Context == DataGridViewDataErrorContexts.Commit))
            {
                MessageBox.Show(string.Format("{0} Enter a valid value.", e.Exception.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvDataSet5_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.Exception != null || (e.Context == DataGridViewDataErrorContexts.Commit))
            {
                MessageBox.Show(string.Format("{0} Enter a valid value.", e.Exception.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rb_zeroflash_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked)
            {
                presenterObj.setData("ZF");
            }
            else
            {
                presenterObj.saveDataOnGrid("ZF");
            }
        }

        private void rb_composition_CheckedChanged(object sender, EventArgs e)
        {
            presenterObj.setVisibility("COMP"); // The visibility of the form is hidden for this case since this form has not yet been created
        }

        private void btn_export_pvt_rpt_Click(object sender, EventArgs e)
        {
            presenterObj.browseExportFileDir();
        }

        private void rb_difflab_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked)
            {
                presenterObj.setData("DL");
            }
            else
            {
                presenterObj.saveDataOnGrid("DL");
            }
        }

        private void rb_cce_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked)
            {
                presenterObj.setData("CCE");
            }
            else
            {
                presenterObj.saveDataOnGrid("CCE");
            }
            
        }

        private void rb_saturation_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked)
            {
                presenterObj.setData("SAT");
            }
            else
            {
                presenterObj.saveDataOnGrid("SAT");
            }
        }

        private void rb_cvd_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked)
            {
                presenterObj.setData("CVD");
            }
            else
            {
                presenterObj.saveDataOnGrid("CVD");
            }
        }

        private void rb_viscosity_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked)
            {
                presenterObj.setData("VIS");
            }
            else
            {
                presenterObj.saveDataOnGrid("VIS");
            }
        }

        private void rb_swelling_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.Checked)
            {
                presenterObj.setData("SWE");
            }
            else
            {
                presenterObj.saveDataOnGrid("SWE");
            }
        }

        private void rb_separator_CheckedChanged(object sender, EventArgs e)
        {
            presenterObj.setVisibility("SEP");
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            copyMenuEventHandler(sender, e);
        }

        private void copyWithHeadersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            copyWithHeadersMenuEventHandler(sender, e);
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pasteMenuEventHandler(sender, e);
        }
    }
}
