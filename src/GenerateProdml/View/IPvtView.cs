 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GenerateProdml.View
{
    interface IPvtView
    {
        #region Event Handlers

        event EventHandler copyMenuEventHandler;
        event EventHandler copyWithHeadersMenuEventHandler;
        event EventHandler pasteMenuEventHandler;

        #endregion

        #region Datagridview maps

        DataGridView dgvDataSet0 { get; set; }
        DataGridView dgvDataSet1 { get; set; }
        DataGridView dgvDataSet2 { get; set; }
        DataGridView dgvDataSet3 { get; set; }
        DataGridView dgvDataSet4 { get; set; }
        DataGridView dgvDataSet5 { get; set; }

        #endregion

        #region Radiobutton maps

        bool isZFselected { get; set; }
        bool isSATselected { get; set; }
        bool isCCEselected { get; set; }
        bool isDLselected { get; set; }
        bool isCVDselected { get; set; }
        bool isSWEselected { get; set; }

        #endregion

        #region Other maps

        bool isSet0Visible { get; set; }
        bool isTabCtrVisible { get; set; }
        string testDescName { get; set; }

        #endregion
    }
}