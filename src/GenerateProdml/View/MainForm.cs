 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GenerateProdml.View
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            //Loading the PRODML form by default
            panel_main.Controls.Clear();
            ProdmlView prodmlForm = new ProdmlView();
            prodmlForm.TopLevel = false;
            panel_main.Controls.Add(prodmlForm);
            prodmlForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            prodmlForm.Dock = DockStyle.Fill;
            prodmlForm.Show();
        }

        private void pRODMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel_main.Controls.Clear();
            ProdmlView prodmlForm = new ProdmlView();
            prodmlForm.TopLevel = false;
            panel_main.Controls.Add(prodmlForm);
            prodmlForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            prodmlForm.Dock = DockStyle.Fill;
            prodmlForm.Show();
        }

        private void pVTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel_main.Controls.Clear();
            PvtView pvtForm = new PvtView();
            pvtForm.TopLevel = false;
            panel_main.Controls.Add(pvtForm);
            pvtForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            pvtForm.Dock = DockStyle.Fill;
            pvtForm.Show();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory,@"..\..\..\Design\PRODML Functional and Design Specification v1.0.pdf")); // Get the Design document file path
        }
    }
}