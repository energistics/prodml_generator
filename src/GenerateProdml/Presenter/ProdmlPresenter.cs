 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data;
using System.Configuration;
using GenerateProdml.Configuration;
using System.Xml;
using System.Xml.Linq;
using GenerateProdml.View;
using GenerateProdml.Model;
using GenerateProdml.Utility;
using Energistics.DataAccess;
using Energistics.DataAccess.PRODML200;
using Energistics.DataAccess.PRODML200.ComponentSchemas;
using Energistics.DataAccess.PRODML200.ReferenceData;
using System.Reflection;
using System.DirectoryServices.AccountManagement;
using System.Xml.Schema;
using System.ComponentModel;
using System.Text.RegularExpressions;


namespace GenerateProdml.Presenter
{
    public class ProdmlPresenter
    {
        #region Variable Initializations

        //string selectedAsset;
        string selectedAssetKey;
        string selectedConnectionString;
        string exprtFileDir = ""; // To preserve the export path selected by user
        string downldFileDir = ""; // To preserve the download file path selected by user
        bool isErrorED = false; // Flag to check if equipment downtime has errors in data
        bool isErrorPI = false; // Flag to check if production information has errors in data
        bool isErrorVWT = false; // Flag to check if validated well tests has errors in data

        public const string ConfigPathString = @"\GenerateProdml.exe.config"; // Changed for this app
        public static string GetBaseConfigPath() { return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + ConfigPathString; } // Changed for this app

        DataTable dtRepEquip = new DataTable(); // To store Reporting Hierarchy equipments information
        DataTable dtRepWell = new DataTable(); // To store Reporting Hierarchy wells information
        ExeConfigurationFileMap configMap    = new ExeConfigurationFileMap();
        Dictionary<string, string> assetList = new Dictionary<string, string>();
        Dictionary<string, List<StoredProcArgument>> storedProcsArgumentsDictionary = new Dictionary<string, List<StoredProcArgument>>();
        StoredProcCollection storedProcCollection = new StoredProcCollection();
        List<string> assetNames = new List<string>();
        IProdmlView  dataView;
        ProdmlModel  dataModel     = new ProdmlModel();
        GridUtils    dataGridUtil  = new GridUtils();
        List<Tuple<string, string, string, string, string, string, string, Tuple<string>>> colMappingsList = new List<Tuple<string, string, string, string, string, string, string, Tuple<string>>>(); // To get all the column mappings from app.config
        List<Tuple<string, string, string, string, string, string, string>> fluidCompCatalogList = new List<Tuple<string, string, string, string, string, string, string>>(); // To get all the Fluid Component Catalog  mappings
        Dictionary<string, Tuple<string, string>> fluidKindDict = new Dictionary<string, Tuple<string, string>>();
        Dictionary<string, string> uuidDict = new Dictionary<string, string>();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        #endregion

        #region Constructors & Intializers

        public ProdmlPresenter(IProdmlView view)
        {
            try
            {
                initializeLog4NetConfiguration();
                log.Info("PRODML file generator application started.");
                dataView = view;
                initializeConfiguration(); //Set the DB/Asset names in the view
                getColumnMappings(); // Get column mappings from App.Config
                getFluidCompCatalogMappings(); // Get fluid component catalog mappings for PI grid
                initDataGrids(); //Sets the columns in the respective data grid view
                initializeEvents(); //Initializes all the events passed by view through the respective Event Handlers
                dataView.isSourceDb = true; // Selects the radio button for relational database source on application startup
                showLogIn(); // To show the Log In panel in case the first loaded asset requires permission for access
            }
            catch(Exception ex)
            {
                MessageBox.Show(string.Format("{0}. Close the application and run again.", ex.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.Error("A problem occurred while initialization. ", ex);
            }
        }

        public void initializeLog4NetConfiguration()
        {
            try
            {
                string baseConfigPath = GetBaseConfigPath();
                log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(baseConfigPath));
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("{0}", ex.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        public void initializeConfiguration() // To populate the asset dropdown
        {
            try
            {
                //Loading configuration file from appdata folder
                log.Info("Initializing configuration file.");
                string baseConfigPath = GetBaseConfigPath();
                configMap.ExeConfigFilename = @baseConfigPath;
                System.Configuration.Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
                ConnectionStringsSection configSection = config.GetSection("connectionStrings") as ConnectionStringsSection;

                //Encrypting App.Config
                if ((!(configSection.ElementInformation.IsLocked)) &&
                (!(configSection.SectionInformation.IsLocked)))
                {
                    if (!configSection.SectionInformation.IsProtected)
                    {
                        configSection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                        config.Save(ConfigurationSaveMode.Full, true);
                    }
                }

                //Get asset list from appsettings
                AppSettingsSection appSettingsSection = config.AppSettings;

                foreach (System.Configuration.KeyValueConfigurationElement asset in appSettingsSection.Settings)
                {
                    assetList.Add(asset.Key.ToString(), asset.Value.ToString());
                }

                assetNames = assetList.Values.Distinct().ToList();
                assetNames.Remove(assetList["Live"]); // Remove the selected connection string from its current position in the dropdown
                assetNames.Insert(0, assetList["Live"]); // Set the selected connection string to the first position in the dropdown
                dataView.srcName = assetNames; // Setting up the DB names.
                //selectedAsset = assetList.Single(s => s.Key == "Live").Value.ToString(); //Commented since no such use required. 
                //selectedAssetKey = assetList.Single(s => s.Value == selectedAsset && s.Key != "Live").Key.ToString();   //Commented since no such use required.
            }
            catch(Exception ex)
            {
                MessageBox.Show(string.Format("{0}",ex.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.Error("A problem occurred while initializing configuration file. ", ex);
            }
        }

        private void initDataGrids()
        {
            log.Info("Initializing data tables.");

            DataTable dtEquipRel = new DataTable();
            DataTable dtProdInfo = new DataTable();
            DataTable dtWellTestInfo = new DataTable();

            ////Loading Column maps from List
            foreach (Tuple<string, string, string, string, string, string, string, Tuple<string>> rowTuple in colMappingsList) //Creating columns in the grids
            {
                string reportType = rowTuple.Rest.Item1;
                if (reportType == "PROD") // Only get columns for Production reporting
                {
                    string[] gridNames = rowTuple.Item7.Split('/'); // Item7 is the Element name. Identifying all the grids where the column(Item1) is to be added.

                    foreach (string name in gridNames)
                    {
                        switch (name)
                        {
                            case "ED":
                                dataView.dgvEquipRelibltyData.Columns.Add(rowTuple.Item1, ((rowTuple.Item5 != "") ? String.Format("{0} ({1})", rowTuple.Item2, rowTuple.Item5) : rowTuple.Item2)); //Mapping column long names to header text in grid
                                dataView.dgvEquipRelibltyData.Columns[rowTuple.Item1].DataPropertyName = rowTuple.Item1;
                                dtEquipRel.Columns.Add(rowTuple.Item1, Type.GetType(string.Format("System.{0}", rowTuple.Item4), true, true)); //For Data Binding
                                if (rowTuple.Item4.ToLower() == "double")
                                {
                                    dataView.dgvEquipRelibltyData.Columns[rowTuple.Item1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight; // Right Aligining the columns having decimal values
                                }
                                else if (rowTuple.Item4.ToLower() == "datetime")
                                {
                                    dataView.dgvEquipRelibltyData.Columns[rowTuple.Item1].DefaultCellStyle.Format = "yyyy-MM-dd hh:mm:ss";
                                }
                                break;

                            case "PI":
                                dataView.dgvProdInfoData.Columns.Add(rowTuple.Item1, ((rowTuple.Item5 != "") ? String.Format("{0}({1})", rowTuple.Item2, rowTuple.Item5) : rowTuple.Item2)); //Mapping column long names to header text in grid
                                dataView.dgvProdInfoData.Columns[rowTuple.Item1].DataPropertyName = rowTuple.Item1;
                                dtProdInfo.Columns.Add(rowTuple.Item1, Type.GetType(string.Format("System.{0}", rowTuple.Item4), true, true)); //For Data Binding
                                if (rowTuple.Item4.ToLower() == "double")
                                {
                                    dataView.dgvProdInfoData.Columns[rowTuple.Item1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight; // Right Aligining the columns having decimal values
                                }
                                else if (rowTuple.Item4.ToLower() == "datetime")
                                {
                                    dataView.dgvProdInfoData.Columns[rowTuple.Item1].DefaultCellStyle.Format = "yyyy-MM-dd hh:mm:ss";
                                }
                                break;

                            case "VWT":
                                dataView.dgvWellTestData.Columns.Add(rowTuple.Item1, ((rowTuple.Item5 != "") ? String.Format("{0} ({1})", rowTuple.Item2, rowTuple.Item5) : rowTuple.Item2)); //Mapping column long names to header text in grid
                                dataView.dgvWellTestData.Columns[rowTuple.Item1].DataPropertyName = rowTuple.Item1;
                                dtWellTestInfo.Columns.Add(rowTuple.Item1, Type.GetType(string.Format("System.{0}", rowTuple.Item4), true, true)); //For Data Binding
                                if (rowTuple.Item4.ToLower() == "double")
                                {
                                    dataView.dgvWellTestData.Columns[rowTuple.Item1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight; // Right Aligining the columns having decimal values
                                }
                                else if (rowTuple.Item4.ToLower() == "datetime")
                                {
                                    dataView.dgvWellTestData.Columns[rowTuple.Item1].DefaultCellStyle.Format = "yyyy-MM-dd hh:mm:ss";
                                }
                                break;
                        }
                    }
                }
            }
            dataView.dgvEquipRelibltyData.DataSource = dtEquipRel; // Used for Data Binding the columns created in the grid
            dataView.dgvProdInfoData.DataSource = dtProdInfo; // Used for Data Binding the columns created in the grid
            dataView.dgvWellTestData.DataSource = dtWellTestInfo; // Used for Data Binding the columns created in the grid
        }

        private void initializeEvents() //initializes all the events
        {
            log.Info("Initializing event handlers.");

            dataView.cutMenuEventHandler += (execRowOperation);
            dataView.copyMenuEventHandler += (execRowOperation);
            dataView.copyWithHeadersMenuEventHandler += (execRowOperation);
            dataView.pasteMenuEventHandler += (execRowOperation);
            dataView.deleteMenuEventHandler += (execRowOperation);
            dataView.sourceTypeEventHandler += (setImportModeConfigs);
            dataView.dataErrorEventHandler +=(displayErrorOrWarningMessage);
            dataView.dataGridViewModeEventHandler += (getUserInputData);
        }

        #endregion

        #region User Input Mode Operations

        public void getUserInputData(object sender, EventArgs e)
        {
            clearDataGrids();

            CheckBox cbxUserInput = (sender as CheckBox);
            if (cbxUserInput.Checked)
            {
                dataView.dgvEquipRelibltyData.AllowUserToAddRows = true;
                dataView.dgvProdInfoData.AllowUserToAddRows = true;
                dataView.dgvWellTestData.AllowUserToAddRows = true;
                dataView.dgvEquipRelibltyData.ReadOnly = false;
                dataView.dgvProdInfoData.ReadOnly = false;
                dataView.dgvWellTestData.ReadOnly = false;
                dataView.isGetDataEnabled = false;
                dataView.isExpHierarchyEntityEnabled = false;
                dataView.isTreeEnabled = false;
                dataView.isImportProdmlEnabled = true;
                dataView.isSourceDbEnabled = false;
                dataView.isSourceFilesEnabled = false;
                dataView.isExportProdmlEnabled = true;
                dataView.isCitationEnabled = true;
                
            }
            else
            {
                dataView.dgvEquipRelibltyData.AllowUserToAddRows = false;
                dataView.dgvProdInfoData.AllowUserToAddRows = false;
                dataView.dgvWellTestData.AllowUserToAddRows = false;
                dataView.dgvEquipRelibltyData.ReadOnly = true;
                dataView.dgvProdInfoData.ReadOnly = true;
                dataView.dgvWellTestData.ReadOnly = true;
                dataView.isGetDataEnabled = true;
                dataView.isExpHierarchyEntityEnabled = true;
                dataView.isTreeEnabled = true;
                dataView.isImportProdmlEnabled = false;
                dataView.isSourceDbEnabled = true;
                dataView.isSourceFilesEnabled = true;
                dataView.isSourceDb = true; // Check the select from source radio button
            }
        }

        #endregion

        #region Browse Export Directory

        public void browseExportFileDir() // To browse for the PRODML Export file Directory.
        {
            FolderBrowserDialog foldrDialogObj = new FolderBrowserDialog();
            foldrDialogObj.Description = "Choose a directory to export.";

            if (exprtFileDir != "" && exprtFileDir != null && exprtFileDir != String.Empty && Directory.Exists(exprtFileDir))
            {
                foldrDialogObj.SelectedPath = exprtFileDir; // Set the last selected directory as current selection
            }

            if (dataView.dgvEquipRelibltyData.Rows.Count < 1 && dataView.dgvProdInfoData.Rows.Count < 1 && dataView.dgvWellTestData.Rows.Count < 1) // This condition will work for Read-Only mode. But for edit mode there is always 1 row available so this condition should be changed in that case.
            {
                MessageBox.Show("No data available to export.", "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (foldrDialogObj.ShowDialog() == DialogResult.OK)
                {
                    exprtFileDir = foldrDialogObj.SelectedPath;
                    try
                    {
                        log.Info("Export commenced.");

                        exportProdml(exprtFileDir);

                        log.Info("Export sucessful.");
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType() == typeof(System.FormatException))
                        {
                            log.Error("A problem occurred while exporting XML(s). ", ex);
                            MessageBox.Show("An error occurred while export due to a null or invalid value in data. The xml(s) will not be generated.", "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            log.Error("A problem occurred while exporting XML(s). ", ex);
                            MessageBox.Show(string.Format("An error occurred while Export. {0}", ex.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }

        #endregion

        #region Browse Import Directory

        public void browseImportFileDir() // To browse for the directory to Import PRODML files
        {
            FolderBrowserDialog foldrDialogImpObj = new FolderBrowserDialog();
            foldrDialogImpObj.Description = "Choose a directory to import files.";

            if (foldrDialogImpObj.ShowDialog() == DialogResult.OK)
            {
                string imprtFileDir = foldrDialogImpObj.SelectedPath;

                try
                {
                    log.Info("Import commenced.");

                    importProdml(imprtFileDir);

                    log.Info("Import successful.");
                }
                catch (Exception ex)
                {
                    log.Error("A problem occurred while importing data from XML(s). ", ex);
                    MessageBox.Show(string.Format("An error occurred while Import. {0}", ex.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        #endregion

        #region Browse Download Directory

        public void browseDownloadFileDir() // To browse for the PRODML Download files Directory.
        {
            FolderBrowserDialog foldrDialogObj = new FolderBrowserDialog();
            foldrDialogObj.Description = "Choose a directory to download files.";

            if (downldFileDir != "" && downldFileDir != null && downldFileDir != String.Empty && Directory.Exists(downldFileDir))
            {
                foldrDialogObj.SelectedPath = downldFileDir; // Set the last selected directory as current selection
            }

            if (dataView.dgvEquipRelibltyData.Rows.Count < 1 && dataView.dgvProdInfoData.Rows.Count < 1 && dataView.dgvWellTestData.Rows.Count < 1) // This condition will work for Read-Only mode. But for edit mode there is always 1 row available so this condition should be changed in that case.
            {
                MessageBox.Show("No data available to download.", "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (foldrDialogObj.ShowDialog() == DialogResult.OK)
                {
                    downldFileDir = foldrDialogObj.SelectedPath;
                    try
                    {
                        log.Info("Download commenced.");

                        downloadDataToFile(downldFileDir);

                        log.Info("Download sucessful.");
                    }
                    catch (Exception ex)
                    {
                        log.Error("A problem occurred while downloading files.", ex);
                        MessageBox.Show(string.Format("An error occurred while downloading data. {0}", ex.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        #endregion

        #region Source Access Related Operations

        public void selectedAssetChanged()
        {
            dataView.isExportHierarchyEntity = false;
            if (dataView.isSourceDb) // This is still under consideration that whether datagrids should clear when user has loaded data from file and changing destination sources from dropdown.
            {
                clearDataGrids();
            }
            dataView.loginUserName = "";
            dataView.loginPassword = "";
            dataView.isLoginMessage = false;
            showLogIn(); // This method is called after the browseImportFileDir method due to an unexpected error if called before the mentioned method
        }

        public bool showLogIn() // Display the LogIn panel in case the asset requires permission to access
        {
            GetConnectionString();
            bool WindowsAuth = false;
            string conectionStringtoValidate = System.Text.RegularExpressions.Regex.Replace(selectedConnectionString.ToLower(), " ", "");
            if (conectionStringtoValidate.Contains("integratedsecurity=true"))
            {
                dataView.isLoginPanel = false;
                enableControls();
                WindowsAuth = false;
            }
            else if (conectionStringtoValidate.Contains("password={1}") && string.IsNullOrEmpty(dataView.loginPassword))
            {
                dataView.isLoginPanel = true;
                disableControls();
                WindowsAuth = true;
            }
            else if ((!(conectionStringtoValidate.Contains("userid={0}"))) && (!(conectionStringtoValidate.Contains("userid=;"))))
            {
                if(conectionStringtoValidate.Contains("userid="))
                {
                    dataView.isLoginPanel = false;
                    enableControls();
                    WindowsAuth = false;
                }
            }

            return WindowsAuth;
        }

        public void logInToSource() // Authorizing user with the input LogIn credentials
        {
            try
            {
                log.Info("Checking user authentication for the selected datasource.");
                GetConnectionString();

                bool AuthUser = false;
                using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(string.Format(selectedConnectionString, dataView.loginUserName, dataView.loginPassword)))
                {
                    // Create the command and set its properties.
                    System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
                    command.Connection = connection;

                    // Open the connection and execute the reader.
                    connection.Open();
                    AuthUser = true;
                }
                
                //bool AuthUser = obj.AuthUser(string.Format(selectedConnectionString, userName, password));
                if (AuthUser == true)
                {
                    dataView.isLoginMessage = false;
                    dataView.isLoginPanel = false;
                    enableControls();
                }
                else
                {
                    dataView.isLoginMessage = true;
                    //dataView.loginUserName = "";
                    //dataView.loginPassword = "";
                }
            }
            catch (Exception ex)
            {
                dataView.isLoginMessage = true;
                log.Error("A problem occurred while connecting to datasource. ", ex);
                // Log Exception
            }
        }

        private void GetConnectionString() // Get the connection string for the selected asset
        {
            string baseConfigPath = GetBaseConfigPath();
            selectedAssetKey = dataView.selectedSrcName;
            configMap.ExeConfigFilename = @baseConfigPath;
            System.Configuration.Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
            ConnectionStringsSection configSection = config.GetSection("connectionStrings") as ConnectionStringsSection;
            selectedConnectionString = configSection.ConnectionStrings[selectedAssetKey].ConnectionString;
            //selectedProviderName = configSection.ConnectionStrings[selectedAssetKey].ProviderName;
        }

        private void enableControls() // Enable the controls in case access is allowed for the selected asset
        {
            dataView.isDgvPanelEnabled = true;
            dataView.isCitationEnabled = (dataView.isSourceFiles) ? false : true;
            dataView.isRprtDteEnabled = true;
        }

        private void disableControls() // Disable the controls in case access required/denied for the selected asset
        {
            dataView.isDgvPanelEnabled = false;
            dataView.isCitationEnabled = false;
            dataView.isRprtDteEnabled = false;
        }

        #endregion

        #region Set Production Data

        public void setProdData() 
        {
            if (dataView.isSourceFiles) // Check if files are selected as the data source
            {
                browseImportFileDir();
            }
            else
            {
                log.Info(string.Format("Getting production data for '{0}'.", dataView.reportDate));
                DataTable dtEquipRel = new DataTable();
                DataTable dtProdInfoData = new DataTable();
                DataTable dtWellTestInfo = new DataTable();
                DataSet dsRepHierData = new DataSet();
                StringBuilder inputVals = new StringBuilder();
                isErrorED = false;
                isErrorPI = false;
                isErrorVWT = false;

                storedProcsArgumentsDictionary = new Dictionary<string, List<StoredProcArgument>>();
                storedProcCollection = new StoredProcCollection();
                selectedAssetKey = dataView.selectedSrcName;

                //Loading configuration file from appdata folder
                string baseConfigPath = GetBaseConfigPath();
                configMap.ExeConfigFilename = @baseConfigPath;
                System.Configuration.Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
                ConnectionStringsSection connectionStringsSection = config.ConnectionStrings;
                selectedConnectionString = connectionStringsSection.ConnectionStrings[selectedAssetKey].ConnectionString;

                //get stored procs list

                string spSec = selectedAssetKey + "/StoredProcConfigurationSection";
                StoredProcConfigurationSection storedProcConfigurationSection = (StoredProcConfigurationSection)config.GetSection(spSec);
                storedProcCollection = storedProcConfigurationSection.StoredProcs;

                string assetSec = selectedAssetKey + "/AssetConfig";
                AssetConfig assetConfigObj = (AssetConfig)config.GetSection(assetSec);

                //Get all the names from the Asset Configurations to create filter for the query
                string assetConfigName = assetConfigObj.AssetName;
                string platformName = assetConfigObj.PlatformName;
                string reserviorName = assetConfigObj.ReserviorName;
                string inputParams = assetConfigObj.InputParameters;

                foreach (StoredProc sp in storedProcCollection)
                {
                    string dataQuery = sp.Description.Replace(@"""", ""); // remove the double quotes escape character '\"'. The """" means it has one double quote in the string
                    string queryId = sp.Id.ToLower();
                    String reportDate = dataView.reportDate;
                    String prodDate = ((Convert.ToDateTime(reportDate)).AddDays(-1)).ToString();

                    if(dataQuery.Contains("@inputParams")) // If query requires input parameters
                    {
                        string[] inputs = inputParams.Split(',');

                        for (int i = 0; i < inputs.Length; i++)
                        {
                            if (i == inputs.Length - 1)
                            {
                                inputVals.Append("'").Append(inputs[i]).Append("'");
                            }
                            else
                            {
                                inputVals.Append("'").Append(inputs[i]).Append("'").Append(",");
                            }
                        }
                        dataQuery = dataQuery.Replace("@inputParams", inputVals.ToString());
                    }

                    switch (queryId)
                    {
                        case "csp_equipmentdowntime_get":
                            dtEquipRel = dataModel.getProdData(selectedConnectionString, dataQuery, prodDate, assetConfigName, platformName, reserviorName, true);
                            break;

                        case "csp_productioninformation_get":
                            dtProdInfoData = dataModel.getProdData(selectedConnectionString, dataQuery, prodDate, assetConfigName, platformName, reserviorName, true);
                            break;

                        case "csp_validatedwelltests_get":
                            dtWellTestInfo = dataModel.getProdData(selectedConnectionString, dataQuery, reportDate, assetConfigName, platformName, reserviorName, true);
                            break;

                        case "csp_reportinghierarchy_equip_get":
                            if (dataView.hierarchyTopNode == null) // Check if the tree is not yet created then only execute this block
                            {
                                dtRepEquip = dataModel.getProdData(selectedConnectionString, dataQuery, reportDate, assetConfigName, platformName, reserviorName, true);
                                dtRepEquip.TableName = "Equipments";
                                dsRepHierData.Tables.Add(dtRepEquip);
                            }
                            break;

                        case "csp_reportinghierarchy_well_get":
                            if (dataView.hierarchyTopNode == null) // Check if the tree is not yet created then only execute this block
                            {
                                dtRepWell = dataModel.getProdData(selectedConnectionString, dataQuery, reportDate, assetConfigName, platformName, reserviorName, true);
                                dtRepWell.TableName = "Wells";
                                dsRepHierData.Tables.Add(dtRepWell);
                            }
                            break;

                        default:
                            break;
                    }
                }
                getUuid(dtEquipRel, "ED");
                getUuid(dtWellTestInfo, "VWT"); // Uuid for well test is generated first because PI data grid has the same wells as WTI, so iterations will be reduced.
                getUuid(dtProdInfoData, "PI");

                dataView.dgvEquipRelibltyData.DataSource = (DataTable)dtEquipRel; // Set Equipment Reliability DataGridView
                dataView.dgvWellTestData.DataSource = (DataTable)dtWellTestInfo; // Set Well Test Information DataGridView
                dataView.dgvProdInfoData.DataSource = (DataTable)getProdInfoDataSource(dtProdInfoData); // Set Production Information DataGridView

                if (dataView.hierarchyTopNode == null) // Check if the tree is not yet created then only execute this block
                {
                    setTreeHierarchy(dsRepHierData); // Set the hierarchy tree
                }
                dsRepHierData.Tables.Clear(); // Removing the tables to free the dataset
                validateDataGrids(); // Validate the data grids by checing for NULL values only.
            }
        }

        private DataTable getProdInfoDataSource(DataTable dtDbData) // To get the data source for the Production Information data grid
        {
            DataTable dtFluidComp = new DataTable(); // Create the datatable to load Fluid Component Catalog values from the configuration

            dtFluidComp.Columns.Add("ProdNameConfig", typeof(string));
            dtFluidComp.Columns.Add("Uom", typeof(string));
            dtFluidComp.Columns.Add("RepEntityVols", typeof(string));

            foreach (Tuple<string, string, string, string, string, string, string> tuple in fluidCompCatalogList) // Loading all configuration values to the Fluid Component Catlog Data Table
            {
                DataRow dr = dtFluidComp.NewRow();
                dr["ProdNameConfig"] = tuple.Item1;    // Gets the Product Fluid Catalog
                dr["Uom"] = tuple.Item2;               // Gets the Unit of Measure
                dr["RepEntityVols"] = tuple.Item4;     // Gets the Reporting Entity Volumes
                dtFluidComp.Rows.Add(dr);
            }

            //// Exclude Injection data with null values. In case injection is null, the queries will return a negative number, this check is incorporated by dev team and not a part of business.
            //var rowsWithNullInj = (from row in dtDbData.AsEnumerable()
            //                       where row.Field<decimal>("ProdVolume") < 0
            //                       select row).ToList();

            //rowsWithNullInj.ForEach(r => dtDbData.Rows.Remove(r)); //Removing rows with Null injection data


            //Join the configurations for Fluid Component Catalog with the Data Table retrieved from DB
            DataTable dtProdInfo = dtDbData.Clone();

            var dtConfigColumns = dtFluidComp.Columns.OfType<DataColumn>().Select(dataCol =>
                new DataColumn(dataCol.ColumnName, dataCol.DataType, dataCol.Expression, dataCol.ColumnMapping));

            dtProdInfo.Columns.AddRange(dtConfigColumns.ToArray()); // Get Config columns

            var rowData =
                from dbRow in dtDbData.AsEnumerable()
                join configRow in dtFluidComp.AsEnumerable()
                    on dbRow.Field<string>("ProdName") equals configRow.Field<string>("ProdNameConfig")
                select dbRow.ItemArray.Concat(configRow.ItemArray).ToArray(); // Concatenating the configuration values with the data values based upon the 'ProdName' key

            foreach (object[] values in rowData) // Set up the data table for the Production Information data source
            {
                dtProdInfo.Rows.Add(values);
            }
            dtProdInfo.Columns.Remove("ProdNameConfig"); // Remove the duplicate column from the configuration
            dtProdInfo.DefaultView.Sort = "FacilityName DESC,RepEntityVols DESC"; // Sorting the datatable

            return dtProdInfo;
        }

        #endregion

        #region Export PRODML

        public void exportProdml(string fileDir)
        {
            string repDate = (Convert.ToDateTime(dataView.reportDate)).Date.ToString("MM-dd-yyyy");

            #region Export ED
            
            //Export EquipmentReliability PRODML file
            DataTable dtEquipRelExport = (DataTable)dataView.dgvEquipRelibltyData.DataSource;

            if (dtEquipRelExport.Rows.Count > 0) // Check if there are any rows available for export
            {
                if (!isErrorED) // Check if there are no validation errors for the ED data
                {
                    string prodMLtyp = "ProductionOperationShutdown";
                    string gridViewName = dataView.equipDowntimeLabel;
                    string citationDesc = (dataView.citationDesc != "" && dataView.citationDesc != string.Empty) ? dataView.citationDesc + "-" : "";
                    ProductionOperation prodOperObj = new ProductionOperation();

                    //Citation
                    prodOperObj.Citation = getCitation(gridViewName, string.Format("{0}{1}", citationDesc, gridViewName));

                    //Aliases
                    //List<ObjectAlias> objAliasList = new List<ObjectAlias>();
                    //objAliasList.Add(getAliases(dataView.aliasesAuthority, string.Format("{0}-{1}", dataView.aliasesDesc, fileName)));
                    //prodOperObj.Aliases = objAliasList;

                    //Issue Date
                    prodOperObj.IssueDate = DateTime.Now; // Fix this

                    //populating the Production Operation Object
                    ProductionOperationInstallationReport prodOperInstRepObj = new ProductionOperationInstallationReport();
                    ProductionOperationActivity prodOperActivtyObj = new ProductionOperationActivity();
                    List<ProductionOperationShutdown> prodOperShutdwnListObj = new List<ProductionOperationShutdown>();

                    foreach (DataRow dr in dtEquipRelExport.Rows)
                    {
                        ProductionOperationShutdown prodOperShutdwnObj = new ProductionOperationShutdown();
                        FacilityIdentifierStruct faciltyIdentStrucObj = new FacilityIdentifierStruct();
                        List<string> installObj = new List<string>();

                        foreach (Tuple<string, string, string, string, string, string, string, Tuple<string>> value in colMappingsList)
                        {
                            if (value.Rest.Item1 == "PROD")
                            {
                                string colName = value.Item1;
                                string uom = value.Item6;
                                string gridName = value.Item7;

                                if ((dtEquipRelExport.Columns.Contains(colName)) && (gridName == "ED" || gridName.StartsWith("ED/") || gridName.EndsWith("/ED") || gridName.Contains("/ED/"))) // Check if the column exists in the export datatable for only ED
                                {
                                    //uom = getEnergisticsUomName(uom); // Commented since the new Energistics Dll uses units as string

                                    switch (colName)
                                    {
                                        case "Equipment": //Setting Equipment
                                            installObj.Add(dr[colName].ToString());
                                            break;

                                        case "HoursDown": //Setting HoursDown
                                            prodOperShutdwnObj.VolumetricDownTime = new TimeMeasure(double.Parse(dr[colName].ToString()), (TimeUom)((typeof(TimeUom).GetField(uom)).GetValue(null)));
                                            break;

                                        case "Reason": //Setting Reason
                                            prodOperShutdwnObj.Description = dr[colName].ToString();
                                            break;

                                        case "StartDate": //Setting Equipment Reporting Start Date
                                            if (dr[colName] != DBNull.Value)
                                            {
                                                prodOperShutdwnObj.DateTimeStart = Convert.ToDateTime(dr[colName]);
                                            }
                                            break;

                                        case "EndDate": //Setting Equipment Reporting End Date
                                            if (dr[colName] != DBNull.Value)
                                            {
                                                prodOperShutdwnObj.DateTimeEnd = Convert.ToDateTime(dr[colName]);
                                            }
                                            break;

                                        case "Uuid": //Setting Global Unique Identifiers for each equipment
                                            prodOperShutdwnObj.Uid = dr[colName].ToString();
                                            break;
                                    }
                                }
                            }
                        }

                        faciltyIdentStrucObj.Text = installObj;
                        prodOperShutdwnObj.Installation = faciltyIdentStrucObj;
                        prodOperShutdwnListObj.Add(prodOperShutdwnObj);
                    }

                    //Setting up ProductionActivity and InstallationReport
                    prodOperActivtyObj.Shutdown = prodOperShutdwnListObj;
                    prodOperActivtyObj.Shutdown.ToArray();
                    prodOperInstRepObj.ProductionActivity = prodOperActivtyObj;
                    List<ProductionOperationInstallationReport> prodOperInstRepList = new List<ProductionOperationInstallationReport>();
                    prodOperInstRepObj.Uid = "Production Operation Shutdown Report";
                    prodOperInstRepList.Add(prodOperInstRepObj);
                    prodOperObj.InstallationReport = prodOperInstRepList;
                    prodOperObj.SchemaVersion = "2.0";
                    prodOperObj.Uuid = "00000000-0000-0000-0000-000000000000";

                    EnergisticsFile.WriteFile(String.Format(@"{0}\{1}_{2}_{3}.xml", fileDir.Trim(), prodMLtyp, dataView.selectedSrcName, repDate), prodOperObj, true); //Export PRODML
                    log.Info("Operation - downtime XML generated successfully.");
                }
            }

            #endregion

            #region Export PI

            //Export Production Information PRODML file
            DataTable dtProdInfoExport = (DataTable)dataView.dgvProdInfoData.DataSource;

            if (dtProdInfoExport.Rows.Count > 0) // Check if there are any rows available for export
            {
                if (!isErrorPI) // Check if there are no validation errors for the PI data
                {
                    string Uuid = "";
                    string facilityType = "";
                    bool execOnce = false;
                    string prodMLtyp = "ProductionVolume";
                    string gridViewName = dataView.prodLabel;
                    string citationDesc = (dataView.citationDesc != "" && dataView.citationDesc != string.Empty) ? dataView.citationDesc + "-" : "";
                    AssetProductionVolumes assetProdVolsObj = new AssetProductionVolumes();
                    //List<ObjectAlias> aliasList = new List<ObjectAlias>();
                    //aliasList.Add(getAliases("CVX", "Daily"));
                    //assetProdVolsObj.Aliases = aliasList;
                    assetProdVolsObj.Citation = getCitation(gridViewName, string.Format("{0}{1}", citationDesc, gridViewName));

                    //Standard Condition
                    TemperaturePressure tempPresObj = new TemperaturePressure();
                    tempPresObj.Temperature = new ThermodynamicTemperatureMeasure { Value = 60, Uom = ThermodynamicTemperatureUom.degF };
                    tempPresObj.Pressure = new PressureMeasure { Value = 14.7, Uom = LegacyPressureUom.psig.ToString() };
                    assetProdVolsObj.StandardConditions = tempPresObj;

                    //FluidComponentCatalog
                    DataTable dtDistinctFluidCompCatalog = dtProdInfoExport.DefaultView.ToTable(true, "ProdName"); // Getting distinct name of Fluid Component Catalog

                    FluidComponentCatalog fluidCompCatalogObj = new FluidComponentCatalog();
                    List<StockTankOil> stockTankList = new List<StockTankOil>();
                    List<NaturalGas> naturalGasList = new List<NaturalGas>();
                    List<FormationWater> formationWaterList = new List<FormationWater>();
                    List<PseudoComponentEnumEnumExtension> pseudoCompExtList = new List<PseudoComponentEnumEnumExtension>();
                    List<PseudoFluidComponent> list = new List<PseudoFluidComponent>();

                    foreach (DataRow drProd in dtDistinctFluidCompCatalog.Rows)
                    {
                        foreach (Tuple<string, string, string, string, string, string, string> tuple in fluidCompCatalogList)
                        {
                            string prodName = tuple.Item1;
                            string fluidCatalogType = tuple.Item7;

                            if (drProd[0].ToString() == prodName)
                            {
                                switch (fluidCatalogType)
                                {
                                    case "StockTankOil":
                                        StockTankOil stockTankOilObj = new StockTankOil();
                                        stockTankOilObj.Uid = prodName;
                                        stockTankList.Add(stockTankOilObj);
                                        fluidCompCatalogObj.StockTankOil = stockTankList;
                                        break;

                                    case "NaturalGas":
                                        NaturalGas naturalGasObj = new NaturalGas();
                                        naturalGasObj.Uid = prodName;
                                        naturalGasList.Add(naturalGasObj);
                                        fluidCompCatalogObj.NaturalGas = naturalGasList;
                                        break;

                                    case "FormationWater":
                                        FormationWater formationWaterObj = new FormationWater();
                                        formationWaterObj.Uid = prodName;
                                        formationWaterList.Add(formationWaterObj);
                                        fluidCompCatalogObj.FormationWater = formationWaterList;
                                        break;
                                }
                                break;
                            }
                        }
                    }
                    assetProdVolsObj.FluidComponentCatalog = fluidCompCatalogObj;

                    //StartDate
                    assetProdVolsObj.StartDate = Convert.ToDateTime(dataView.dgvProdInfoData.Rows[0].Cells["StartDate"].Value);

                    //EndDate
                    assetProdVolsObj.EndDate = Convert.ToDateTime(dataView.dgvProdInfoData.Rows[0].Cells["EndDate"].Value);// DateTime.Now; // DB HIt

                    //Nominal Period
                    assetProdVolsObj.NominalPeriod = new ReportingDurationKindEnumExtension { EnumValueStr = "day" };

                    DataTable dtDistinctFieldNames = dtProdInfoExport.DefaultView.ToTable(true, "FacilityName"); // Getting distinct name of entities
                    List<ReportingEntityVolumes> reprtngEntityVolsList = new List<ReportingEntityVolumes>(); // Creating Reporting Entity Volumes list

                    foreach (DataRow dr in dtDistinctFieldNames.Rows) //For each distinct FacilityName
                    {
                        DataRow[] drDispData = dtProdInfoExport.Select(string.Format("FacilityName = '{0}' AND RepEntityVols = 'disposition'", dr[0].ToString()));
                        DataRow[] drProdData = dtProdInfoExport.Select(string.Format("FacilityName = '{0}' AND RepEntityVols = 'production'", dr[0].ToString()));
                        DataRow[] drInjectData = dtProdInfoExport.Select(string.Format("FacilityName = '{0}' AND RepEntityVols = 'injection'", dr[0].ToString()));

                        ReportingEntityVolumes reportingEntityVolsObj = new ReportingEntityVolumes(); // Set up reporting entity volume List

                        if (drDispData.Length > 0) // Disposition
                        {
                            List<AbstractDisposition> abstractDispositionList = new List<AbstractDisposition>(); // Disposition List (currently showing everything in its first element)
                            List<AbstractProductQuantity> abstractDispQuantityList = new List<AbstractProductQuantity>(); //List to contain Disposition Quantity elements

                            ProductDisposition prodDispositionObj = new ProductDisposition(); //Disposition Node
                            prodDispositionObj.QuantityMethod = new QuantityMethodEnumExtension { EnumValueStr = (drDispData[0]["FacilityType"].ToString() == "equipment") ? "measured" : "allocated" };

                            if (drDispData[0]["FacilityType"].ToString() == "field") // If it is a field then categorize different dispositions at field level, e.g. fuel/flare/buyback
                            {
                                DataTable dtDispData = dtProdInfoExport.Clone();

                                foreach (DataRow row in drDispData) // Get all the field dispositions in a datatable
                                {
                                    dtDispData.ImportRow(row);
                                }

                                //[Start] This code is added to generically get all the Disposition Kinds
                                dtDispData.Columns.Add("DispKind", typeof(string));

                                foreach (DataRow dispRow in dtDispData.Rows)
                                {
                                    dispRow["DispKind"] = fluidKindDict[dispRow["ProdName"].ToString()].Item1;
                                }

                                DataTable dtDistinctDispNames = dtDispData.DefaultView.ToTable(true, "DispKind");
                                //[End]

                                //foreach (string dispTyp in fieldDispTypes) // previous disp demo code
                                foreach (DataRow dispName in dtDistinctDispNames.Rows) // new disp code
                                {
                                    ProductDisposition prodFieldDispositionObj = new ProductDisposition(); //Disposition Node
                                    prodFieldDispositionObj.QuantityMethod = new QuantityMethodEnumExtension { EnumValueStr = "measured" };
                                    List<AbstractProductQuantity> abstractFieldDispQuantityList = new List<AbstractProductQuantity>(); //List to contain Disposition Quantity elements
                                    DataRow[] drFieldDispData = dtDispData.Select(string.Format("DispKind = '{0}'", dispName[0].ToString())); // new disp code

                                    foreach (DataRow dataRow in drFieldDispData) // Creates Nodes for each type of disposition. fuel, flare, buyback.
                                    {
                                        if (!execOnce) // To set all these values in the first iteration only
                                        {
                                            prodFieldDispositionObj.Uid = fluidKindDict[dataRow["ProdName"].ToString()].Item1; //hide
                                            prodFieldDispositionObj.Kind = new DispositionKindEnumExtension { EnumValueStr = fluidKindDict[dataRow["ProdName"].ToString()].Item1 };//new DispositionKindEnumExtension { EnumValueStr = dataRow["DispositionKind"].ToString() };
                                            Uuid = dataRow["Uuid"].ToString();
                                            facilityType = dataRow["FacilityType"].ToString();
                                        }

                                        ProductFluid prodFluidObj = new ProductFluid();
                                        prodFluidObj.Uid = dataRow["ProdName"].ToString();
                                        prodFluidObj.ProductFluidReference = dataRow["ProdName"].ToString();
                                        //prodFluidObj.Volume = new VolumeValue { Volume = new VolumeMeasureExt { Value = Convert.ToDouble(dataRow["ProdVolume"].ToString()), Uom = ((typeof(LegacyVolumeUom).GetField(getEnergisticsUomName(getProdInfoUom(dataRow["ProdName"].ToString())))).GetValue(null)).ToString() } };
                                        prodFluidObj.Volume = new VolumeValue { Volume = new VolumeMeasureExt { Value = Convert.ToDouble(dataRow["ProdVolume"].ToString()), Uom = getProdInfoUom(dataRow["ProdName"].ToString(), true) } };
                                        prodFluidObj.ProductFluidKind = new ProductFluidKindEnumExtension { EnumValueStr = fluidKindDict[dataRow["ProdName"].ToString()].Item2 };//new ProductFluidKindEnumExtension { EnumValueStr = ProductFluidKind.gasdry.ToString() };

                                        abstractFieldDispQuantityList.Add(prodFluidObj); //Add element to Disposition Quantity List.
                                        execOnce = true;
                                    }
                                    prodFieldDispositionObj.DispositionQuantity = abstractFieldDispQuantityList; //Add Disposition Quantity List to object.
                                    abstractDispositionList.Add(prodFieldDispositionObj); // Adding to Disposition List
                                    execOnce = false;
                                }
                                reportingEntityVolsObj.Disposition = abstractDispositionList;
                            }
                            else
                            {
                                foreach (DataRow dataRow in drDispData)
                                {
                                    if (!execOnce) // To set all these values in the first iteration only
                                    {
                                        prodDispositionObj.Uid = fluidKindDict[dataRow["ProdName"].ToString()].Item1; //hide
                                        //prodDispositionObj.Kind = new DispositionKindEnumExtension { EnumValueStr = dataRow["DispositionKind"].ToString() };
                                        //prodDispositionObj.Uid = prodObjectsValues[dataRow["ProdName"].ToString()].Values;
                                        //string[] i = prodObjectsValues[""].Values.ToArray<string>();
                                        prodDispositionObj.Kind = new DispositionKindEnumExtension { EnumValueStr = fluidKindDict[dataRow["ProdName"].ToString()].Item1 };//new DispositionKindEnumExtension { EnumValueStr = dataRow["DispositionKind"].ToString() };
                                        Uuid = dataRow["Uuid"].ToString();
                                        facilityType = dataRow["FacilityType"].ToString();
                                    }

                                    ProductFluid prodFluidObj = new ProductFluid();
                                    prodFluidObj.Uid = dataRow["ProdName"].ToString();
                                    prodFluidObj.ProductFluidReference = dataRow["ProdName"].ToString();
                                    //prodFluidObj.Volume = new VolumeValue { Volume = new VolumeMeasureExt { Value = Convert.ToDouble(dataRow["ProdVolume"].ToString()), Uom = ((typeof(LegacyVolumeUom).GetField(getEnergisticsUomName(getProdInfoUom(dataRow["ProdName"].ToString())))).GetValue(null)).ToString() } };
                                    prodFluidObj.Volume = new VolumeValue { Volume = new VolumeMeasureExt { Value = Convert.ToDouble(dataRow["ProdVolume"].ToString()), Uom = getProdInfoUom(dataRow["ProdName"].ToString(), true) } };
                                    prodFluidObj.ProductFluidKind = new ProductFluidKindEnumExtension { EnumValueStr = fluidKindDict[dataRow["ProdName"].ToString()].Item2 };//new ProductFluidKindEnumExtension { EnumValueStr = ProductFluidKind.gasdry.ToString() };

                                    abstractDispQuantityList.Add(prodFluidObj); //Add element to Disposition Quantity List.
                                    execOnce = true;
                                }

                                prodDispositionObj.DispositionQuantity = abstractDispQuantityList; //Add Disposition Quantity List to object.
                                abstractDispositionList.Add(prodDispositionObj); // Adding to Disposition List
                                reportingEntityVolsObj.Disposition = abstractDispositionList;
                            }
                        }

                        if (drProdData.Length > 0) // Production
                        {
                            List<Production> prodList = new List<Production>();
                            List<AbstractProductQuantity> abstractProdQuantityList = new List<AbstractProductQuantity>();
                            Production prodObj = new Production();
                            prodObj.QuantityMethod = new QuantityMethodEnumExtension { EnumValueStr = (drProdData[0]["FacilityType"].ToString() == "field" || drProdData[0]["FacilityType"].ToString() == "equipment") ? "measured" : "allocated" };

                            foreach (DataRow dataRow in drProdData)
                            {
                                if (!execOnce)
                                {
                                    Uuid = dataRow["Uuid"].ToString();
                                    facilityType = dataRow["FacilityType"].ToString();
                                    execOnce = true;
                                }

                                ProductFluid productFluidObj = new ProductFluid();
                                productFluidObj.Uid = dataRow["ProdName"].ToString();//"Oil";
                                productFluidObj.ProductFluidReference = dataRow["ProdName"].ToString();
                                //productFluidObj.Volume = new VolumeValue { Volume = new VolumeMeasureExt { Value = Convert.ToDouble(dataRow["ProdVolume"].ToString()), Uom = ((typeof(LegacyVolumeUom).GetField(getEnergisticsUomName(getProdInfoUom(dataRow["ProdName"].ToString())))).GetValue(null)).ToString() } };
                                productFluidObj.Volume = new VolumeValue { Volume = new VolumeMeasureExt { Value = Convert.ToDouble(dataRow["ProdVolume"].ToString()), Uom = getProdInfoUom(dataRow["ProdName"].ToString(), true) } };
                                productFluidObj.ProductFluidKind = new ProductFluidKindEnumExtension { EnumValueStr = fluidKindDict[dataRow["ProdName"].ToString()].Item2 }; //ProductFluidKind.oilgross.ToString()

                                abstractProdQuantityList.Add(productFluidObj);
                            }

                            prodObj.ProductionQuantity = abstractProdQuantityList;
                            prodList.Add(prodObj);
                            reportingEntityVolsObj.Production = prodList;
                        }

                        if (drInjectData.Length > 0) // Injection
                        {
                            List<Injection> injectList = new List<Injection>();
                            List<AbstractProductQuantity> abstractInjectQuantityList = new List<AbstractProductQuantity>();
                            Injection injectObj = new Injection();
                            injectObj.QuantityMethod = new QuantityMethodEnumExtension { EnumValueStr = (drInjectData[0]["FacilityType"].ToString() == "field") ? "measured" : "allocated" };

                            foreach (DataRow dataRow in drInjectData)
                            {
                                if (!execOnce)
                                {
                                    Uuid = dataRow["Uuid"].ToString();
                                    facilityType = dataRow["FacilityType"].ToString();
                                    execOnce = true;
                                }

                                ProductFluid injProductFluidObj = new ProductFluid();
                                injProductFluidObj.Uid = dataRow["ProdName"].ToString();//"gir or wir";
                                injProductFluidObj.ProductFluidReference = dataRow["ProdName"].ToString();
                                injProductFluidObj.Volume = new VolumeValue { Volume = new VolumeMeasureExt { Value = Convert.ToDouble(dataRow["ProdVolume"].ToString()), Uom = getProdInfoUom(dataRow["ProdName"].ToString(), true) } };
                                injProductFluidObj.ProductFluidKind = new ProductFluidKindEnumExtension { EnumValueStr = fluidKindDict[dataRow["ProdName"].ToString()].Item2 };

                                abstractInjectQuantityList.Add(injProductFluidObj);
                            }

                            injectObj.InjectionQuantity = abstractInjectQuantityList;
                            injectList.Add(injectObj);
                            reportingEntityVolsObj.Injection = injectList;
                        }

                        DataObjectReference reprtngEntityRefObj = new DataObjectReference(); // Well Reference
                        reprtngEntityRefObj.ContentType = facilityType; // Assumed to show the facility type e.g. well, equipment, field etc.
                        reprtngEntityRefObj.Title = dr[0].ToString();
                        reprtngEntityRefObj.Uuid = Uuid; // In case there will be Disposition only, then this value will come from Disposition block and vice versa for Production or Injection.
                        reportingEntityVolsObj.ReportingEntityReference = reprtngEntityRefObj;

                        reprtngEntityVolsList.Add(reportingEntityVolsObj);
                        execOnce = false;
                    }
                    assetProdVolsObj.ReportingEntityVolumes = reprtngEntityVolsList;
                    assetProdVolsObj.SchemaVersion = "2.0";
                    assetProdVolsObj.Uuid = "00000000-0000-0000-0000-000000000000";

                    string filePath = string.Format(@"{0}\{1}_{2}_{3}.xml", fileDir.Trim(), prodMLtyp, dataView.selectedSrcName, repDate);
                    EnergisticsFile.WriteFile(filePath, assetProdVolsObj, true);
                    log.Info("Daily allocated volumes XML generated successfully.");
                }
            }

            #endregion

            #region Export VWT

            //Export WellTestInformation PRODML file
            DataTable dtWellTestInfoExport = (DataTable)dataView.dgvWellTestData.DataSource;

            if (dtWellTestInfoExport.Rows.Count > 0) // Check if there are any rows available for export
            {
                if (!isErrorVWT) // Check if there are no validation errors for the VWT data
                {
                    foreach (DataRow dr in dtWellTestInfoExport.Rows)
                    {
                        string prodMLtyp = "WellTest";
                        string gridViewName = dataView.validWellTestLabel;
                        string citationDesc = (dataView.citationDesc != "" && dataView.citationDesc != string.Empty) ? dataView.citationDesc + "-" : "";
                        WellTest welltestObj = new WellTest();
                        WellTestProductionTestData wellTestProdDataObj = dataModel.getWellTestProdTestDataObject();

                        //Citation
                        welltestObj.Citation = getCitation(gridViewName, string.Format("{0}{1}", citationDesc, gridViewName));

                        //ValidationState
                        welltestObj.ValidationState = ValidationState.validated; //This report is only for Validated well tests

                        //WellReference
                        welltestObj.WellReference = new DataObjectReference(); //WellReference initialization
                        welltestObj.WellReference.ContentType = "well"; // What does this column show?

                        foreach (Tuple<string, string, string, string, string, string, string, Tuple<string>> value in colMappingsList)
                        {
                            if (value.Rest.Item1 == "PROD")
                            {
                                string colName = value.Item1;
                                //string uom = value.Item5;
                                string uom = value.Item6;
                                string gridName = value.Item7;

                                if ((dtWellTestInfoExport.Columns.Contains(colName)) && (gridName == "VWT" || gridName.StartsWith("VWT/") || gridName.EndsWith("/VWT") || gridName.Contains("/VWT/"))) // Check if the column exists in the export datatable for only VWT
                                {
                                    // This is commented since Energistics Dll now accepts units as string//uom = getEnergisticsUomName(uom); // Get the correct Units of Measure name defined in the Energistics dll

                                    switch (colName)
                                    {
                                        case "Well":
                                            welltestObj.WellReference.Title = dr[colName].ToString();
                                            break;

                                        case "TestDate":
                                            welltestObj.TestDate = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd hh:mm:ss}", dr[colName]));
                                            break;

                                        case "Uuid":
                                            welltestObj.WellReference.Uuid = dr[colName].ToString();
                                            break;

                                        case "DHPT":
                                            wellTestProdDataObj.BottomholeData.BottomholePres.Value = double.Parse(dr[colName].ToString());
                                            wellTestProdDataObj.BottomholeData.BottomholePres.Uom = uom; //((typeof(LegacyPressureUom).GetField(uom)).GetValue(null)).ToString(); // This is commented since Energistics Dll now accepts units as string
                                            break;

                                        case "BS&W%":
                                            wellTestProdDataObj.ProductionTestResults.BasicSedimentAndWater.Value = (dr[colName].ToString() == "NULL" || dr[colName].ToString() == "") ? double.NaN : double.Parse(dr[colName].ToString());
                                            wellTestProdDataObj.ProductionTestResults.BasicSedimentAndWater.Uom = uom; // ((typeof(VolumePerVolumeUom).GetField(uom)).GetValue(null)).ToString(); // Since this is a '%' symbol, this does not require to be in LegacyUom
                                            break;

                                        case "WPR":
                                            wellTestProdDataObj.ProductionTestResults.WaterVolume.Volume.Value = double.Parse(dr[colName].ToString());
                                            wellTestProdDataObj.ProductionTestResults.WaterVolume.Volume.Uom = uom; //((typeof(LegacyVolumeUom).GetField(uom)).GetValue(null)).ToString();
                                            break;

                                        case "OPR":
                                            wellTestProdDataObj.ProductionTestResults.OilVolume.Volume.Value = double.Parse(dr[colName].ToString());
                                            wellTestProdDataObj.ProductionTestResults.OilVolume.Volume.Uom = uom; //((typeof(LegacyVolumeUom).GetField(uom)).GetValue(null)).ToString();
                                            break;

                                        case "GPR":
                                            wellTestProdDataObj.ProductionTestResults.GasVolume.Volume.Value = double.Parse(dr[colName].ToString());
                                            wellTestProdDataObj.ProductionTestResults.GasVolume.Volume.Uom = uom; //((typeof(LegacyVolumeUom).GetField(uom)).GetValue(null)).ToString();
                                            break;

                                        case "UCTT":
                                            wellTestProdDataObj.WellheadData.Temperature.Value = double.Parse(dr[colName].ToString());
                                            wellTestProdDataObj.WellheadData.Temperature.Uom = (ThermodynamicTemperatureUom)((typeof(ThermodynamicTemperatureUom).GetField(uom)).GetValue(null));
                                            break;

                                        case "UCPT":
                                            wellTestProdDataObj.WellheadData.FlowingPressure = new GaugePressure { GaugePressure1 = new PressureMeasureExt(double.Parse(dr[colName].ToString()), uom) };
                                            break;

                                        case "SITP":
                                            wellTestProdDataObj.WellheadData.ShutInPressure = new GaugePressure { GaugePressure1 = new PressureMeasureExt(double.Parse(dr[colName].ToString()), uom) };
                                            break;

                                        case "PCV":
                                            wellTestProdDataObj.WellheadData.ChokeOrificeSize.Value = double.Parse(dr[colName].ToString());
                                            if (uom == "in")
                                            {
                                                wellTestProdDataObj.WellheadData.ChokeOrificeSize.Uom = LengthUom.@in; // The length for this one is hard-coded for demo
                                            }
                                            else
                                            {
                                                wellTestProdDataObj.WellheadData.ChokeOrificeSize.Uom = (LengthUom)((typeof(LengthUom).GetField(uom)).GetValue(null));
                                            }
                                            break;
                                    }
                                }
                            }
                        }

                        //Assigning wellTestProdDataObj to WellTestObj
                        welltestObj.WellTestData = wellTestProdDataObj;
                        welltestObj.WellTestData.Uid = string.Format("WellTest Data for {0}", welltestObj.WellReference.Title);
                        welltestObj.SchemaVersion = "2.0";
                        welltestObj.Uuid = "00000000-0000-0000-0000-000000000000";
                        // End of WellTest Object Initialization

                        string filePath = string.Format(@"{0}\{1}_{2}_{3}_{4}.xml", fileDir.Trim(), prodMLtyp, dataView.selectedSrcName, welltestObj.WellReference.Title.Replace(" ", ""), repDate);
                        EnergisticsFile.WriteFile(filePath, welltestObj, true);
                    }
                    log.Info("Validated well test XML(s) generated successfully.");
                }
            }

            #endregion

            #region Export RH

            if (dataView.isExportHierarchyEntity) // Check if Reporting Hierarchy is chosen to export or not
            {
                //Adding Reporting Hierarchy data tables to the dataset to use for export of XML
                DataSet dsRepHierDataSet = new DataSet();

                if(dtRepEquip.Rows.Count > 0) // Check if there are any equipments available for this field
                {
                    dsRepHierDataSet.Tables.Add(dtRepEquip);
                }
                if (dtRepEquip.Rows.Count > 0) // Check if there are any wells available for this field
                {
                    dsRepHierDataSet.Tables.Add(dtRepWell);
                }

                if (dsRepHierDataSet.Tables.Count > 0) // Execute the Reporting Hierarchy section only if there is data available
                {
                    //Initializing Reporting Hierarchy objects
                    ReportingHierarchy repHierarchyObj = new ReportingHierarchy();
                    string prodMLtyp = "Reporting Hierarchy";
                    string citationDesc = (dataView.citationDesc != "" && dataView.citationDesc != string.Empty) ? dataView.citationDesc + "-" : "";
                    Random rand = new Random(); // To create a random number in case Uuid is not already generated for a facility while data retrieval in UI

                    //Citation
                    repHierarchyObj.Citation = getCitation(prodMLtyp, string.Format("{0}{1}", citationDesc, prodMLtyp));

                    ReportingHierarchyNode repHierarchyParentNode = new ReportingHierarchyNode();
                    List<ReportingHierarchyNode> repHierarchyParentNodeList = new List<ReportingHierarchyNode>();
                    List<ReportingHierarchyNode> repHierarchyChildNodeList = new List<ReportingHierarchyNode>();

                    repHierarchyParentNode.Id = "field";
                    repHierarchyParentNode.Name = dataView.selectedSrcName; // Select the field name from the dropdown

                    foreach (DataTable dtRepHierData in dsRepHierDataSet.Tables) // For each facility type e.g. equipments, wells
                    {
                        foreach (DataRow row in dtRepHierData.Rows)
                        {
                            //Setting up Well Reference
                            DataObjectReference dataObjRef = new DataObjectReference();
                            ReportingHierarchyNode repHierarchyChildNode = new ReportingHierarchyNode();
                            repHierarchyChildNode.Name = row["Facility"].ToString();
                            dataObjRef.Title = row["Facility"].ToString();

                            //Getting validity period to mark current status of facility
                            DateTime validTo = Convert.ToDateTime(row["ValidTo"]);
                            dataObjRef.ContentType = (dtRepHierData.TableName == "Wells") ? "well" : "equipment"; // (validTo >= DateTime.Now) ? "active" : "non active"; // Record the facility status in the content type. Marking it as active or non active depending upon the check on the validity period.

                            if (dtRepHierData.TableName == "Equipments") // If equipments data table
                            {
                                repHierarchyChildNode.Id = "equipment";
                                if (uuidDict != null && uuidDict.ContainsKey(row["Facility"].ToString()))
                                {
                                    dataObjRef.Uuid = uuidDict[row["Facility"].ToString()];
                                }
                                else
                                {
                                    uuidDict.Add(row["Facility"].ToString(), string.Format("00000000-0000-0000-0000-000{0}", rand.Next(658946723, 999999999)));
                                    dataObjRef.Uuid = uuidDict[row["Facility"].ToString()];// If Uuid is not available in dictionary then create a new Uuid
                                }
                            }
                            else // If wells data table. As of now only equipments and wells are the identified facility types.
                            {
                                repHierarchyChildNode.Id = "well";
                                if (uuidDict != null && uuidDict.ContainsKey(row["Facility"].ToString()))
                                {
                                    dataObjRef.Uuid = uuidDict[row["Facility"].ToString()];
                                }
                                else
                                {
                                    uuidDict.Add(row["Facility"].ToString(), string.Format("00000000-0000-0000-00{0}-{1}", row["Id"].ToString().Substring(0, 2), row["Id"].ToString().Substring(2, row["Id"].ToString().Length - 2))); // Creating Uuid from Api14 which is in the "Id" column
                                    dataObjRef.Uuid = uuidDict[row["Facility"].ToString()];// If Uuid is not available in dictionary then create a new Uuid
                                }
                            }
                            repHierarchyChildNode.ReportingEnitityReference = dataObjRef;
                            repHierarchyChildNodeList.Add(repHierarchyChildNode);
                        }
                    }
                    repHierarchyParentNode.ChildNode = repHierarchyChildNodeList;
                    repHierarchyParentNodeList.Add(repHierarchyParentNode);
                    repHierarchyObj.ReportingNode = repHierarchyParentNodeList;

                    //Report Info
                    repHierarchyObj.Uuid = "00000000-0000-0000-0000-000000000000";
                    repHierarchyObj.SchemaVersion = "v2.0";

                    string filePath = string.Format(@"{0}\{1}_{2}_{3}.xml", fileDir.Trim(), prodMLtyp.Replace(" ", ""), dataView.selectedSrcName, repDate);
                    EnergisticsFile.WriteFile(filePath, repHierarchyObj, true);
                    dsRepHierDataSet.Tables.Clear(); // Removing the reporting hierarchy tables from the data set so that they can be used in other methods
                    log.Info("Reporting hierarchy XML generated successfully.");
                }
            }

            #endregion

            #region Export RE

            if (dataView.isExportHierarchyEntity) // Check if Reporting Entity is chosen to export
            {
                Random rnd = new Random();
                DataSet dsRepEntityData = new DataSet();

                if(dtRepEquip.Rows.Count > 0) // Check if data rows available for equipment
                {
                    dsRepEntityData.Tables.Add(dtRepEquip);
                }

                if (dtRepWell.Rows.Count > 0) // Check if data rows available for wells
                {
                    dsRepEntityData.Tables.Add(dtRepWell);
                }

                if (dsRepEntityData.Tables.Count > 0) // Execute only if there is data available
                {
                    foreach (DataTable dtRepEntity in dsRepEntityData.Tables) // For each type of facility
                    {
                        foreach (DataRow drow in dtRepEntity.Rows)
                        {
                            ReportingEntity repEntity = new ReportingEntity(); // Set the reporting entity object
                            DateTime validTo = Convert.ToDateTime(drow["ValidTo"]); // Get the validityTo date of the equipment

                            //Citation
                            string citationDesc = (dataView.citationDesc != "" && dataView.citationDesc != string.Empty) ? dataView.citationDesc + "-" : "";
                            repEntity.Citation = getCitation("Reporting Entity", string.Format("{0}{1}", citationDesc, "Reporting Entity"));

                            /* Commented due to further discussion on exporting Reporting Facility from a different schema
                            if (dtRepEntity.TableName == "Equipments") // In case facility is an equipment
                            {
                                //Field Reference
                                DataObjectReference dataRefEntityObj = new DataObjectReference();
                                dataRefEntityObj.ContentType = "equipment"; // (validTo >= DateTime.Now) ? "active" : "non active"; // Record the facility status in the content type. Marking it as active or non active depending upon the check on the validity period.
                                dataRefEntityObj.Title = drow["Facility"].ToString();
                                if (uuidDict != null && uuidDict.ContainsKey(drow["Facility"].ToString()))
                                {
                                    dataRefEntityObj.Uuid = uuidDict[drow["Facility"].ToString()];
                                }
                                else
                                {
                                    uuidDict.Add(drow["Facility"].ToString(), string.Format("00000000-0000-0000-0000-000{0}", rnd.Next(699998564, 999999999)));// If Uuid is not available in dictionary then create a new Uuid
                                    dataRefEntityObj.Uuid = uuidDict[drow["Facility"].ToString()];
                                }
                                repEntity.TargetFacilityReference = dataRefEntityObj;

                                //Reporting Entity Kind
                                repEntity.Kind = ReportingEntityKind.facility;

                                //Set Equipment Alias
                                List<ObjectAlias> aliasList = new List<ObjectAlias>();
                                aliasList.Add(getAliases("Equipment", "Equipment Code", drow["Id"].ToString()));
                                repEntity.Alias = aliasList;

                                //Report Info
                                repEntity.Uuid = "00000000-0000-0000-0000-000000000000";
                                repEntity.SchemaVersion = "v2.0";

                                string filePath = string.Format(@"{0}\ReportingEntity_{1}_Facility-{2}_{3}.xml", fileDir.Trim(), dataView.selectedSrcName, repEntity.TargetFacilityReference.Title, repDate);
                                EnergisticsFile.WriteFile(filePath, repEntity, true);
                            }
                            else */
                            if (dtRepEntity.TableName == "Wells") // In case facility is a well
                            {
                                foreach (DataRow row in dtRepEntity.Rows)
                                {
                                    //Citation
                                    repEntity.Citation = getCitation("Reporting Entity", string.Format("{0}{1}", citationDesc, "Reporting Entity"));

                                    //Field Reference
                                    DataObjectReference dataRefEntityObj = new DataObjectReference();
                                    dataRefEntityObj.ContentType = "well";
                                    dataRefEntityObj.Title = row["Facility"].ToString();
                                    dataRefEntityObj.Uuid = uuidDict[row["Facility"].ToString()]; // Assuming Uuid dict will have all the wells since it has already been created for reporting hierarchy
                                    repEntity.TargetFacilityReference = dataRefEntityObj;

                                    //Reporting Entity Kind
                                    repEntity.Kind = ReportingEntityKind.well;

                                    //Set Well Names
                                    List<ObjectAlias> aliasList = new List<ObjectAlias>();
                                    aliasList.Add(getAliases("Api", "Api12", row["Api12"].ToString()));
                                    aliasList.Add(getAliases("Api", "Api14", row["Id"].ToString()));
                                    aliasList.Add(getAliases("Api", "Api15", row["Api15"].ToString()));
                                    repEntity.Alias = aliasList;

                                    //Report Info
                                    repEntity.Uuid = "00000000-0000-0000-0000-000000000000";
                                    repEntity.SchemaVersion = "v2.0";

                                    string filePath = string.Format(@"{0}\ReportingEntity_{1}_Well-{2}_{3}.xml", fileDir.Trim(), dataView.selectedSrcName, repEntity.TargetFacilityReference.Title, repDate);
                                    EnergisticsFile.WriteFile(filePath, repEntity, true);
                                }
                            }
                        }
                    }
                    dsRepEntityData.Tables.Clear(); // Removing tables from the data set so that they can be used in other processings
                    log.Info("Reporting entity XML(s) generated successfully.");
                }
            }

            #endregion

            #region Export Result Messages

            if (isErrorED || isErrorPI || isErrorVWT) // Check if there is any validation errors detected
            {
                StringBuilder validErrRepsName = new StringBuilder();
                if (isErrorED)
                {
                    validErrRepsName.Append("- Equipment Downtime \n");
                    log.Warn("Equipment Downtime XML not generated due to validation error.");
                }
                if (isErrorPI)
                {
                    validErrRepsName.Append("- Production \n");
                    log.Warn("Production XML not generated due to validation error.");
                }
                if (isErrorVWT)
                {
                    validErrRepsName.Append("- Validated Well Tests");
                    log.Warn("Validated Well Tests XML not generated due to validation error.");
                }
                MessageBox.Show(string.Format("The following report(s) will not be generated due to validation errors. \n {0}",validErrRepsName), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            if (MessageBox.Show("Export successful, Do you want to open the directory containing files?", "PRODML File Generator", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) // To prompt that Export operation has been completed.
            {
                System.Diagnostics.Process.Start(fileDir);
            }

            #endregion
        }

        #endregion

        #region Import PRODML

        public void importProdml(string fileDir)
        {
            bool isDataImported = false; // Checks if the import was executed for any file or no
            string reprtDte = (Convert.ToDateTime(dataView.reportDate)).Date.ToString("MM-dd-yyyy"); // Gets the report date and sets it in a format which can be appended to the generated xml(s) file name

            var xmlFilePaths = Directory.EnumerateFiles(fileDir, string.Format("*{0}.xml", reprtDte), SearchOption.TopDirectoryOnly); // Search for all the xml(s) for the mentioned report date

            foreach (string filePath in xmlFilePaths)
            {

                #region Import ED

                if (Path.GetFileName(filePath).ToLower().StartsWith("productionoperationshutdown") && (Regex.Replace(Path.GetFileName(filePath), @"\s+", "").ToLower()).Contains(Regex.Replace(dataView.selectedSrcName, @"\s+", "").ToLower())) // The first condition checks for the type of prodml and the second condition compares the asset name with the one selected in the dropdown to make sure the prodml(s) having the correct asset data is selected
                {
                    DataTable dtEquipRelImport = ((DataTable)(dataView.dgvEquipRelibltyData.DataSource)).Clone();
                    ProductionOperation xmlProdOperObj = EnergisticsFile.ReadFile<ProductionOperation>(filePath);

                    foreach (ProductionOperationInstallationReport prodOperInstRepObj in xmlProdOperObj.InstallationReport) //Installation Report List
                    {
                        foreach (ProductionOperationShutdown prodOperShutdownObj in prodOperInstRepObj.ProductionActivity.Shutdown) //Shutdown List
                        {
                            DataRow rows = dtEquipRelImport.NewRow();

                            rows["Equipment"] = prodOperShutdownObj.Installation.Text.First().ToString(); // Since Installation / Equipment is mapped as a List<string> in the object
                            rows["HoursDown"] = prodOperShutdownObj.VolumetricDownTime.Value;
                            rows["Reason"] = prodOperShutdownObj.Description;
                            if (prodOperShutdownObj.DateTimeStart != null)
                            {
                                rows["StartDate"] = prodOperShutdownObj.DateTimeStart;
                            }
                            if (prodOperShutdownObj.DateTimeEnd != null)
                            {
                                rows["EndDate"] = prodOperShutdownObj.DateTimeEnd;
                            }
                            rows["Uuid"] = prodOperShutdownObj.Uid;

                            dtEquipRelImport.Rows.Add(rows);
                        }
                    }
                    //string xmlCitDesc = xmlProdOperObj.Citation.Description;
                    //dataView.citationDesc = (xmlCitDesc.Contains("-")) ? xmlCitDesc.Substring(0, xmlCitDesc.IndexOf("-")) : "";
                    dataView.dgvEquipRelibltyData.DataSource = dtEquipRelImport;
                    isDataImported = true;

                    log.Info(string.Format("Data imported successfully from file '{0}'.",Path.GetFileName(filePath)));
                }

                #endregion

                #region Import PI

                else if (Path.GetFileName(filePath).ToLower().StartsWith("productionvolume") && (Regex.Replace(Path.GetFileName(filePath), @"\s+", "").ToLower()).Contains(Regex.Replace(dataView.selectedSrcName, @"\s+", "").ToLower())) // The first condition checks for the type of prodml and the second condition compares the asset name with the one selected in the dropdown to make sure the prodml(s) having the correct asset data is selected
                {
                    DataTable dtProdInfoImport = ((DataTable)(dataView.dgvProdInfoData.DataSource)).Clone();
                    AssetProductionVolumes xmlAssetProdVolsObj = EnergisticsFile.ReadFile<AssetProductionVolumes>(filePath);
                    List<ReportingEntityVolumes> xmlRepEntityVolsList = xmlAssetProdVolsObj.ReportingEntityVolumes;

                    foreach (ReportingEntityVolumes repEntityVolsObj in xmlRepEntityVolsList)
                    {
                        if (repEntityVolsObj.Disposition.Count > 0) // disposition
                        {
                            foreach (ProductDisposition xmlProdDispObj in repEntityVolsObj.Disposition)
                            {
                                foreach (ProductFluid xmlDispFluidObj in xmlProdDispObj.DispositionQuantity)
                                {
                                    DataRow dispRows = dtProdInfoImport.NewRow();

                                    dispRows["FacilityName"] = repEntityVolsObj.ReportingEntityReference.Title;
                                    dispRows["FacilityType"] = repEntityVolsObj.ReportingEntityReference.ContentType;
                                    dispRows["ProdName"] = xmlDispFluidObj.ProductFluidReference;
                                    dispRows["ProdVolume"] = xmlDispFluidObj.Volume.Volume.Value;
                                    dispRows["Uom"] = getProdInfoUom(xmlDispFluidObj.ProductFluidReference, false);
                                    dispRows["RepEntityVols"] = "disposition";
                                    dispRows["StartDate"] = xmlAssetProdVolsObj.StartDate;
                                    dispRows["EndDate"] = xmlAssetProdVolsObj.EndDate;
                                    dispRows["Uuid"] = repEntityVolsObj.ReportingEntityReference.Uuid;

                                    dtProdInfoImport.Rows.Add(dispRows);
                                }
                            }
                        }

                        if (repEntityVolsObj.Production.Count > 0) // production
                        {
                            foreach (Production xmlProdObj in repEntityVolsObj.Production)
                            {
                                foreach (ProductFluid xmlProdFluidObj in xmlProdObj.ProductionQuantity)
                                {
                                    DataRow prodRows = dtProdInfoImport.NewRow();

                                    prodRows["FacilityName"] = repEntityVolsObj.ReportingEntityReference.Title;
                                    prodRows["FacilityType"] = repEntityVolsObj.ReportingEntityReference.ContentType;
                                    prodRows["ProdName"] = xmlProdFluidObj.ProductFluidReference;
                                    prodRows["ProdVolume"] = xmlProdFluidObj.Volume.Volume.Value;
                                    prodRows["Uom"] = getProdInfoUom(xmlProdFluidObj.ProductFluidReference, false);
                                    prodRows["RepEntityVols"] = "production";
                                    prodRows["StartDate"] = xmlAssetProdVolsObj.StartDate;
                                    prodRows["EndDate"] = xmlAssetProdVolsObj.EndDate;
                                    prodRows["Uuid"] = repEntityVolsObj.ReportingEntityReference.Uuid;

                                    dtProdInfoImport.Rows.Add(prodRows);
                                }
                            }
                        }

                        if (repEntityVolsObj.Injection.Count > 0) // injection
                        {
                            foreach (Injection xmlInjectObj in repEntityVolsObj.Injection)
                            {
                                foreach (ProductFluid xmlInjectFluidObj in xmlInjectObj.InjectionQuantity)
                                {
                                    DataRow injectRows = dtProdInfoImport.NewRow();

                                    injectRows["FacilityName"] = repEntityVolsObj.ReportingEntityReference.Title;
                                    injectRows["FacilityType"] = repEntityVolsObj.ReportingEntityReference.ContentType;
                                    injectRows["ProdName"] = xmlInjectFluidObj.ProductFluidReference;
                                    injectRows["ProdVolume"] = xmlInjectFluidObj.Volume.Volume.Value;
                                    injectRows["Uom"] = getProdInfoUom(xmlInjectFluidObj.ProductFluidReference, false);
                                    injectRows["RepEntityVols"] = "injection";
                                    injectRows["StartDate"] = xmlAssetProdVolsObj.StartDate;
                                    injectRows["EndDate"] = xmlAssetProdVolsObj.EndDate;
                                    injectRows["Uuid"] = repEntityVolsObj.ReportingEntityReference.Uuid;

                                    dtProdInfoImport.Rows.Add(injectRows);
                                }
                            }
                        }
                    }
                    //string xmlCitDesc = xmlAssetProdVolsObj.Citation.Description;
                    //dataView.citationDesc = (xmlCitDesc.Contains("-")) ? xmlCitDesc.Substring(0, xmlCitDesc.IndexOf("-")) : "";
                    dataView.dgvProdInfoData.DataSource = dtProdInfoImport;
                    isDataImported = true;

                    log.Info(string.Format("Data imported successfully from file '{0}'.",Path.GetFileName(filePath)));
                }

                #endregion

                #region Import VWT

                else if (Path.GetFileName(filePath).ToLower().StartsWith("welltest") && (Regex.Replace(Path.GetFileName(filePath), @"\s+", "").ToLower()).Contains(Regex.Replace(dataView.selectedSrcName, @"\s+", "").ToLower())) // The first condition checks for the type of prodml and the second condition compares the asset name with the one selected in the dropdown to make sure the prodml(s) having the correct asset data is selected
                {
                    //DataTable dtWellTestInfoImport = ((DataTable)(dataView.dgvWellTestData.DataSource)).Clone();
                    DataTable dtWellTestInfoImport = (DataTable)(dataView.dgvWellTestData.DataSource); // The direct source is used because everytime well test import executes for a single file, the system should retain the import of the previous well test file on the datagrid and likewise it keeps on going till all well tests are imported on the same datagridview
                    WellTest xmlWellTestObj = EnergisticsFile.ReadFile<WellTest>(filePath);
                    WellTestProductionTestData xmlWellTestProdTestDataObj = (WellTestProductionTestData)xmlWellTestObj.WellTestData;
                    WellTestProductionTestResults xmlWellTestProdTestResObj = xmlWellTestProdTestDataObj.ProductionTestResults;
                    WellTestWellheadData xmlWellTestWellHeadDataObj = xmlWellTestProdTestDataObj.WellheadData;
                    GaugePressure xmlFlowingGaugePressure = (GaugePressure)(xmlWellTestWellHeadDataObj.FlowingPressure);
                    GaugePressure xmlShutinGaugePressure = (GaugePressure)(xmlWellTestWellHeadDataObj.FlowingPressure);

                    DataRow row = dtWellTestInfoImport.NewRow();

                    row["Well"] = xmlWellTestObj.WellReference.Title;
                    row["Uuid"] = xmlWellTestObj.WellReference.Uuid;
                    row["TestDate"] = xmlWellTestObj.TestDate;
                    //row["OPR"] = xmlWellTestProdTestResObj.OilRate.FluidRate.Value;
                    //row["WPR"] = xmlWellTestProdTestResObj.WaterRate.FluidRate.Value;
                    //row["GPR"] = xmlWellTestProdTestResObj.GasRate.FluidRate.Value;
                    row["OPR"] = xmlWellTestProdTestResObj.OilVolume.Volume.Value;
                    row["WPR"] = xmlWellTestProdTestResObj.WaterVolume.Volume.Value;
                    row["GPR"] = xmlWellTestProdTestResObj.GasVolume.Volume.Value;
                    row["BS&W%"] = xmlWellTestProdTestResObj.BasicSedimentAndWater.Value;
                    row["DHPT"] = xmlWellTestProdTestDataObj.BottomholeData.BottomholePres.Value;
                    row["UCTT"] = xmlWellTestProdTestDataObj.WellheadData.Temperature.Value;
                    row["PCV"] = xmlWellTestProdTestDataObj.WellheadData.ChokeOrificeSize.Value;
                    row["UCPT"] = xmlFlowingGaugePressure.GaugePressure1.Value;
                    row["SITP"] = xmlShutinGaugePressure.GaugePressure1.Value;

                    dtWellTestInfoImport.Rows.Add(row);
                    //string xmlCitDesc = xmlWellTestObj.Citation.Description;
                    //dataView.citationDesc = (xmlCitDesc.Contains("-")) ? xmlCitDesc.Substring(0, xmlCitDesc.IndexOf("-")) : "";
                    dataView.dgvWellTestData.DataSource = dtWellTestInfoImport;
                    isDataImported = true;

                    log.Info(string.Format("Data imported successfully from file '{0}'.", Path.GetFileName(filePath)));
                }

                #endregion

                #region Import RH

                else if (Path.GetFileName(filePath).ToLower().StartsWith("reportinghierarchy") && (Regex.Replace(Path.GetFileName(filePath), @"\s+", "").ToLower()).Contains(Regex.Replace(dataView.selectedSrcName, @"\s+", "").ToLower())) // The first condition checks for the type of prodml and the second condition compares the asset name with the one selected in the dropdown to make sure the prodml(s) having the correct asset data is selected
                {

                    ReportingHierarchy repHierImpObj = EnergisticsFile.ReadFile<ReportingHierarchy>(filePath);
                    ReportingHierarchyNode repHierImpNodeobj = repHierImpObj.ReportingNode.First<ReportingHierarchyNode>();

                    //Create the top node of the tree which will be the name of the asset selected by user
                    TreeNode topNode = new TreeNode();
                    topNode.Name = dataView.selectedSrcName;
                    topNode.Text = dataView.selectedSrcName;

                    topNode.Nodes.Add("0", "Equipments");
                    topNode.Nodes.Add("1", "Wells");
                    
                    foreach(ReportingHierarchyNode repNode in repHierImpNodeobj.ChildNode)
                    {
                        //Creating nodes for each facility type which will go as child node under the top node
                        TreeNode node = new TreeNode();
                        DataObjectReference dataObjRef = repNode.ReportingEnitityReference;
                        node.Name = dataObjRef.Title;
                        node.Text = dataObjRef.Title;
                        //node.ToolTipText = string.Format("Id: {0}", dataObjRef.Uuid); // Set the tool tip text for this node

                        if(repNode.Id == "well")
                        {
                            topNode.Nodes["1"].Nodes.Add(node);
                        }
                        else
                        {
                            topNode.Nodes["0"].Nodes.Add(node);
                        }
                    }
                    topNode.Text = string.Format("{0}({1})", topNode.Text, topNode.GetNodeCount(false)); // Set the node name to include the node count
                    topNode.Nodes[0].Text = string.Format("{0}({1})", topNode.Nodes[0].Text, topNode.Nodes[0].GetNodeCount(false)); // Set the node name to include the node count
                    topNode.Nodes[1].Text = string.Format("{0}({1})", topNode.Nodes[1].Text, topNode.Nodes[1].GetNodeCount(false)); // Set the node name to include the node count
                    dataView.hierarchyTopNode = topNode;
                }

                #endregion

            }
            if (!isDataImported)
            {
                clearDataGrids();
                MessageBox.Show("The chosen directory did not have any XML(s) for the selected asset and the mentioned report date or the XML(s) present at this directory are not valid. Please select the correct asset and report date or choose a different directory and try again.", "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion

        #region Upload Data

        public void updateData()
        {
            if (dataView.dgvEquipRelibltyData.Rows.Count < 1 && dataView.dgvProdInfoData.Rows.Count < 1 && dataView.dgvWellTestData.Rows.Count < 1) // This condition will work for Read-Only mode. But for edit mode there is always 1 row available so this condition should be changed in that case.
            {
                MessageBox.Show("No data available to upload.", "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (MessageBox.Show(string.Format("Are you sure you want to upload data to {0} ?",dataView.selectedSrcName), "PRODML File Generator", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) // Ask user to check whether he really wants to upload data.
            {
                return;
            }

            try
            {
                log.Info("Uploading data.");
                DataTable equipRelData = ((DataTable)dataView.dgvEquipRelibltyData.DataSource).Copy();
                DataTable prodInfoData = ((DataTable)dataView.dgvProdInfoData.DataSource).Copy();
                DataTable wellTestData = ((DataTable)dataView.dgvWellTestData.DataSource).Copy();
                string selectedSource = dataView.selectedSrcName;

                //// Creating production date column
                //DataColumn prodDate = new DataColumn(); 
                //prodDate.ColumnName = "ProductionDate";
                //prodDate.DataType = typeof(DateTime);
                //prodDate.DefaultValue = (Convert.ToDateTime(dataView.reportDate)).AddDays(-1);

                //// Creating asset column to store the update the name of the asset with the record
                //DataColumn assetNme = new DataColumn(); // Creating production date column
                //assetNme.ColumnName = "AssetName";
                //assetNme.DataType = typeof(string);
                //assetNme.DefaultValue = dataView.selectedSrcName;

                //Loading configuration file from appdata folder
                StoredProcCollection stordProcColl = new StoredProcCollection();
                string baseConfigPath = GetBaseConfigPath();
                ExeConfigurationFileMap confMap = new ExeConfigurationFileMap();
                confMap.ExeConfigFilename = @baseConfigPath;
                System.Configuration.Configuration config = ConfigurationManager.OpenMappedExeConfiguration(confMap, ConfigurationUserLevel.None);
                ConnectionStringsSection connectionStringsSection = config.ConnectionStrings;
                string conStr = connectionStringsSection.ConnectionStrings[selectedSource].ConnectionString;

                //get stored procs list

                string spSec = selectedSource + "/StoredProcConfigurationSection";
                StoredProcConfigurationSection storedProcConfigurationSection = (StoredProcConfigurationSection)config.GetSection(spSec);
                stordProcColl = storedProcConfigurationSection.StoredProcs;

                foreach (StoredProc sp in stordProcColl)
                {
                    // Creating production date column
                    DataColumn prodDate = new DataColumn();
                    prodDate.ColumnName = "ProductionDate";
                    prodDate.DataType = typeof(DateTime);
                    prodDate.DefaultValue = (Convert.ToDateTime(dataView.reportDate)).AddDays(-1);

                    // Creating asset column to store the update the name of the asset with the record
                    DataColumn assetNme = new DataColumn(); // Creating production date column
                    assetNme.ColumnName = "AssetName";
                    assetNme.DataType = typeof(string);
                    assetNme.DefaultValue = assetList["Live"]; //dataView.selectedSrcName;
                    
                    string dataQuery = sp.Description.Replace(@"""", ""); // remove the double quotes escape character '\"'. The """" means it has one double quote in the string
                    string queryId = sp.Id.ToLower();

                    switch (queryId)
                    {
                        case "csp_equipmentdowntime_get":
                            foreach (DataColumn col in equipRelData.Columns)
                            {
                                foreach (Tuple<string, string, string, string, string, string, string, Tuple<string>> rowTuple in colMappingsList)
                                {
                                    if (rowTuple.Rest.Item1 == "PROD")
                                    {
                                        if (col.ColumnName == rowTuple.Item1)
                                        {
                                            if (rowTuple.Item3 != "")
                                            {
                                                equipRelData.Columns[col.ColumnName].ColumnName = rowTuple.Item3; // Changing column names in datatable to original column names in database
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                            equipRelData.Columns.AddRange(new DataColumn[] { prodDate, assetNme }); // Adding the created data columns to the datatable.
                            dataModel.WriteToDatabase(conStr, sp.DestName, equipRelData);
                            break;

                        case "csp_productioninformation_get":
                            foreach (DataColumn col in prodInfoData.Columns)
                            {
                                foreach (Tuple<string, string, string, string, string, string, string, Tuple<string>> rowTuple in colMappingsList)
                                {
                                    if (rowTuple.Rest.Item1 == "PROD")
                                    {
                                        if (col.ColumnName == rowTuple.Item1)
                                        {
                                            if (rowTuple.Item3 != "")
                                            {
                                                prodInfoData.Columns[col.ColumnName].ColumnName = rowTuple.Item3; // Changing column names in datatable to original column names in database
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                            prodInfoData.Columns.AddRange(new DataColumn[] { prodDate, assetNme }); // Adding the created data columns to the datatable.
                            dataModel.WriteToDatabase(conStr, sp.DestName, prodInfoData);
                            break;

                        case "csp_validatedwelltests_get":
                            foreach (DataColumn col in wellTestData.Columns)
                            {
                                foreach (Tuple<string, string, string, string, string, string, string, Tuple<string>> rowTuple in colMappingsList)
                                {
                                    if (rowTuple.Rest.Item1 == "PROD")
                                    {
                                        if (col.ColumnName == rowTuple.Item1)
                                        {
                                            if (rowTuple.Item3 != "")
                                            {
                                                wellTestData.Columns[col.ColumnName].ColumnName = rowTuple.Item3; // Changing column names in datatable to original column names in database
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                            wellTestData.Columns.AddRange(new DataColumn[] { assetNme }); // Adding the created data column to the datatable.
                            dataModel.WriteToDatabase(conStr, sp.DestName, wellTestData);
                            break;
                    }
                }
                log.Info("Data uploaded successfully.");
                MessageBox.Show("Data uploaded successfully.", "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                log.Error("A problem occurred while uploading data to database.", ex);
                MessageBox.Show(string.Format("An error occurred while uploding data to database. {0}", ex.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Download Data

        public void downloadDataToFile(string fileDir)
        {
            DataSet dsDataViews = new DataSet();
            DataTable dtEquipRel = mapOrigColNames((dataView.dgvEquipRelibltyData.DataSource as DataTable).Copy());
            dtEquipRel.TableName = "OperationShutdown_data";
            DataTable dtProdInfo = mapOrigColNames((dataView.dgvProdInfoData.DataSource as DataTable).Copy());
            dtProdInfo.TableName = "DailyProductionVolumes_data";
            DataTable dtWellTest = mapOrigColNames((dataView.dgvWellTestData.DataSource as DataTable).Copy());
            dtWellTest.TableName = "ValidatedWellTest_data";

            dsDataViews.Tables.Add(dtEquipRel);
            dsDataViews.Tables.Add(dtProdInfo);
            dsDataViews.Tables.Add(dtWellTest);

            foreach (DataTable dtDwnldData in dsDataViews.Tables)
            {
                // Creating column to add selected asset name in the download file
                DataColumn selecAssetNme = new DataColumn();
                selecAssetNme.DataType = typeof(string);
                selecAssetNme.ColumnName = "AssetName";
                selecAssetNme.DefaultValue = assetList["Live"];
                dtDwnldData.Columns.Add(selecAssetNme);

                string filePath = string.Format("{0}\\{1}_{2}.csv", fileDir, dtDwnldData.TableName, (Convert.ToDateTime(dataView.reportDate)).Date.ToString("MM-dd-yyyy"));
                StreamWriter streamWriter = new StreamWriter(filePath, false);

                for (int i = 0; i < dtDwnldData.Columns.Count; i++)
                {
                    if (i == dtDwnldData.Columns.Count - 1) // check if last column // This is a new condition since column selectAssetNme is added now
                    {
                        streamWriter.Write(dtDwnldData.Columns[i].ColumnName);
                    }
                    else
                    {
                        streamWriter.Write(dtDwnldData.Columns[i].ColumnName + ",");
                    }
                }
                streamWriter.WriteLine();

                foreach (DataRow row in dtDwnldData.Rows)
                {
                    object[] array = row.ItemArray;
                    for (int j = 0; j < array.Length; j++)
                    {
                        if (j == dtDwnldData.Columns.Count - 1) // Check if last column
                        {
                            streamWriter.Write(array[j].ToString());
                        }
                        else
                        {
                            streamWriter.Write(array[j].ToString() + ",");
                        }
                    }
                    streamWriter.WriteLine();
                }
                streamWriter.Close();
            }

            if (MessageBox.Show("Download successful, Do you want to open the directory containing files?", "PRODML File Generator", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) // To prompt that Export operation has been completed.
            {
                System.Diagnostics.Process.Start(fileDir);
            }
        }

        public DataTable mapOrigColNames(DataTable dtData) // To map the original column names from the configuration list
        {
            foreach (DataColumn dataCol in dtData.Columns)
            {
                foreach (Tuple<string, string, string, string, string, string, string, Tuple<string>> value in colMappingsList)
                {
                    if (value.Rest.Item1 == "PROD") // Get the 8th item from the mapping list which is the reporting type
                    {
                        if (dataCol.ColumnName == value.Item1) // Get the source names for the corresponding shortnames
                        {
                            string uom = (value.Item5 != "") ? string.Format("{0}", value.Item5) : ""; // Getting the unit of measure for the column
                            string colName = (uom != "") ? string.Format("{0} ({1})", value.Item2, uom) : value.Item2; // Checking condition if uom is not empty
                            dtData.Columns[dataCol.ColumnName].ColumnName = colName; // Mapping shortnames to longnames alongwith units
                            break;
                        }
                    }
                }
            }
            return dtData;
        }

        #endregion

        #region Set Tree Hierarchy

        private void setTreeHierarchy(DataSet dsRepHierData)
        {
            //Get the number of days configured by user to highlight the equipment/well name for
            configMap.ExeConfigFilename = GetBaseConfigPath();
            System.Configuration.Configuration appConfig = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
            string userConfigSec = "UserConfig";
            UserConfig userConfigObj = (UserConfig)appConfig.GetSection(userConfigSec);
            int nodeHighlightDays = userConfigObj.AlertDays;

            //Create the top node of the tree which will be the name of the asset selected by user
            TreeNode topNode = new TreeNode();
            topNode.Name = dataView.selectedSrcName;
            topNode.Text = dataView.selectedSrcName;
            topNode.GetNodeCount(false);

            foreach (DataTable dtRepData in dsRepHierData.Tables)
            {
                //Creating nodes for each facility type which will go as child node under the top node
                TreeNode node = new TreeNode();
                node.Text = dtRepData.TableName;
                node.Name = dtRepData.TableName;
                foreach (DataRow dr in dtRepData.Rows)
                {
                    //Creating child nodes for each node defining a facility type
                    TreeNode childNode = new TreeNode();
                    childNode.Name = dr["Facility"].ToString();
                    childNode.Text = dr["Facility"].ToString();

                    // Get facility validity dates
                    DateTime validFrom = Convert.ToDateTime(dr["ValidFrom"]);
                    DateTime validTo = Convert.ToDateTime(dr["ValidTo"]);
                    DateTime rptDate = Convert.ToDateTime(dataView.reportDate);

                    if (validFrom <= rptDate)
                    {
                        if(validTo >= rptDate)
                        {
                            if(rptDate.Subtract(validFrom).Days <= nodeHighlightDays)
                            {
                                childNode.ForeColor = System.Drawing.Color.Green; // It is a new facility
                            }
                        }
                        else
                        {
                            childNode.ForeColor = System.Drawing.Color.Red; // Facility has been decomissioned
                        }
                    }
                    else
                    {
                        childNode.ForeColor = System.Drawing.Color.Red; // Facility was not active at this report date
                    }
                    childNode.ToolTipText = string.Format("Id: {0}\nValidityPeriod: ({1}-{2})", dr["Id"], dr["ValidFrom"], dr["ValidTo"]); // Set the tool tip text for this node
                    node.Nodes.Add(childNode); // Add the child nodes to node representing the particular facility type
                }
                topNode.Nodes.Add(node); // Adding the nodes representing facility types to the top node
            }
           // topNode.Text = string.Format("{0}({1})",topNode.Text,topNode.GetNodeCount(false)); // Set the node name to include the node count

            if (dsRepHierData.Tables["Equipments"].Rows.Count > 0) // Check whether there are nodes available for Equipments node.
            {
                topNode.Nodes["Equipments"].Text = string.Format("{0}({1})", topNode.Nodes["Equipments"].Text, topNode.Nodes["Equipments"].GetNodeCount(false)); // Set the node name to include the node count
            }

            if (dsRepHierData.Tables["Wells"].Rows.Count > 0) // Check whether there are nodes available for Wells node.
            {
                topNode.Nodes["Wells"].Text = string.Format("{0}({1})", topNode.Nodes["Wells"].Text, topNode.Nodes["Wells"].GetNodeCount(false)); // Set the node name to include the node count
            }
            dataView.hierarchyTopNode = topNode; // Assign the top node
        }

        #endregion

        #region Get Energistics Uom Name

        public string getEnergisticsUomName(string name)
        {
           if(name.Contains('/'))
           {
               name = name.Remove(name.IndexOf('/'), 1);
           }

           if (System.Text.RegularExpressions.Regex.IsMatch(name, @"^\d")) // Check if the units of measure starts with a number, then concatenate "Item." with it to use reflection to set its Uom
           {
               name = string.Format("Item{0}", name); 
           }
           
           if(name == "%")
           {
               name = "Item";
           }

           //if(name == "psig")
           //{
           //    name = "psi";
           //}

           return name;
        }

        public string getProdInfoUom(string prodName, bool isEnergyUom) // The isEnergyUom if set to true gives Energistics units, otherwise returns standard units.
        {
            string uom = "";
            foreach (Tuple<string, string, string, string, string, string, string> tuple in fluidCompCatalogList)
            {
                if (tuple.Item1 == prodName)
                {
                    if (isEnergyUom)
                    {
                        uom = tuple.Item3;
                    }
                    else
                    {
                        uom = tuple.Item2;
                    }
                    break;
                }
            }
            return uom;
        }

        #endregion

        #region Data Grid View Operations

        public void validateDataGrids()
        {
            foreach (DataGridViewRow dgvRow in dataView.dgvEquipRelibltyData.Rows)
            {
                foreach (DataGridViewColumn dgvCol in dataView.dgvEquipRelibltyData.Columns)
                {
                    if (dgvCol.Name != "StartDate" && dgvCol.Name != "EndDate") // Ignore Nulls in Start Date and End Date
                    {
                        if (String.IsNullOrEmpty(dgvRow.Cells[dgvCol.Name].FormattedValue.ToString()))
                        {
                            dataView.dgvEquipRelibltyData.Rows[dgvRow.Index].ErrorText = string.Format("Data is missing for {0}.", dgvCol.HeaderText);
                            isErrorED = true;
                        }
                    }
                }
            }

            foreach (DataGridViewRow dgvRow in dataView.dgvProdInfoData.Rows)
            {
                foreach (DataGridViewColumn dgvCol in dataView.dgvProdInfoData.Columns)
                {
                    if (String.IsNullOrEmpty(dgvRow.Cells[dgvCol.Name].FormattedValue.ToString()))
                    {
                        dataView.dgvProdInfoData.Rows[dgvRow.Index].ErrorText = string.Format("Data is missing for {0}.", dgvCol.HeaderText);
                        isErrorPI = true;
                    }
                }
            }

            foreach (DataGridViewRow dgvRow in dataView.dgvWellTestData.Rows)
            {
                foreach (DataGridViewColumn dgvCol in dataView.dgvWellTestData.Columns)
                {
                    if (String.IsNullOrEmpty(dgvRow.Cells[dgvCol.Name].FormattedValue.ToString()))
                    {
                        dataView.dgvWellTestData.Rows[dgvRow.Index].ErrorText = string.Format("Data is missing for {0}.", dgvCol.HeaderText);
                        isErrorVWT = true;
                    }
                }
            }
        }

        public void execRowOperation(object sender, EventArgs e) // Contains the implementation of the Copy/Copy w Headers/Cut/Paste & Delete functionality for the Context Menu Strip
        {
            try
            {
                ToolStripItem toolStripMenuItem = (sender as ToolStripItem); //Initialize the ToolStripItem object based on sender
                if (toolStripMenuItem != null)
                {
                    ContextMenuStrip contextMenuStripOwner = (toolStripMenuItem.Owner as ContextMenuStrip);
                    if (contextMenuStripOwner != null)
                    {
                        DataGridView selectedDataGrid = contextMenuStripOwner.SourceControl as DataGridView; // Gets the DataGridView control which contains this ContextMenuStrip
                        if (selectedDataGrid != null)
                        {
                            if (selectedDataGrid.SelectedCells.Count > 0)
                            {
                                switch (toolStripMenuItem.Name)
                                {
                                    case "cutToolStripMenuItem":
                                        dataGridUtil.cutToClipboard(selectedDataGrid);
                                        break;

                                    case "copyToolStripMenuItem":
                                        dataGridUtil.copyToClipboard(selectedDataGrid, false);
                                        break;

                                    case "copyWithHeadersToolStripMenuItem":
                                        dataGridUtil.copyToClipboard(selectedDataGrid, true);
                                        break;

                                    case "pasteToolStripMenuItem":
                                        dataGridUtil.pasteToClipboard(selectedDataGrid);
                                        break;

                                    case "deleteToolStripMenuItem":

                                        switch (selectedDataGrid.Name)
                                        {
                                            case "dg_equip_reliblty":
                                                dataView.dgvEquipRelibltyData.DataSource = dataGridUtil.deleteFromClipboard(selectedDataGrid);
                                                break;

                                            case "dg_prod_info":
                                                dataView.dgvProdInfoData.DataSource = dataGridUtil.deleteFromClipboard(selectedDataGrid);
                                                break;

                                            case "dg_well_test":
                                                dataView.dgvWellTestData.DataSource = dataGridUtil.deleteFromClipboard(selectedDataGrid);
                                                break;

                                            default:
                                                break;
                                        }
                                        break;

                                    default:
                                        break;
                                }
                                // Can assign the contextMenu here in case there is no data in the grid. And can also have an else statement where no data is available
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(string.Format("{0}", ex.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void displayErrorOrWarningMessage(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(string.Format("{0} Enter a valid value.",e.Exception.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public DataTable getUuid(DataTable dtData, string type)
        {
            Random number = new Random(); // To generate a random number to concatenate in the Uuid
            dtData.Columns.Add("Uuid", typeof(string));

            switch (type)
            {
                case "ED":
                    foreach (DataRow row in dtData.Rows)
                    {
                        string key = row["Equipment"].ToString();

                        if (uuidDict != null && uuidDict.ContainsKey(key))
                        {
                            row["Uuid"] = uuidDict[key];
                        }
                        else if(row["FacTyp"].ToString() == "equipment")
                        {
                            //uuidDict.Add(key, Guid.NewGuid().ToString("D"));
                            //uuidDict.Add(key, string.Format("00000000-0000-0000-0000-0000{0}", number.Next(10000000, 99999999)));
                            uuidDict.Add(key, string.Format("00000000-0000-0000-0000-000{0}", number.Next(600000000, 999999999)));
                            row["Uuid"] = uuidDict[key];
                        }
                        else if (row["FacTyp"].ToString() == "well")
                        {
                            string api = row["FacilityCode"].ToString();

                            uuidDict.Add(key, string.Format("00000000-0000-0000-00{0}-{1}", api.Substring(0, 2), api.Substring(2, api.Length - 2)));
                            row["Uuid"] = uuidDict[key];
                        }
                    }
                    if(dtData.Columns.Contains("FacTyp"))
                    {
                        dtData.Columns.Remove("FacTyp");
                    }
                    if (dtData.Columns.Contains("FacilityCode"))
                    {
                        dtData.Columns.Remove("FacilityCode");
                    }
                    break;
                case "PI":
                    foreach (DataRow row in dtData.Rows)
                    {
                        string key = row["FacilityName"].ToString(); // Assuming Well Name would not have 'SM' appended to it and will just be the well number

                        if (uuidDict != null && uuidDict.ContainsKey(key)) //Assuming that Generator names in ER and PI grid would be the same
                        {
                            row["Uuid"] = uuidDict[key];
                        }
                        else
                        {
                            //uuidDict.Add(key, Guid.NewGuid().ToString("D")); // Assumption is that All the Wells reported in Well Test data grid will be also available in Production data grid.
                            //uuidDict.Add(key, string.Format("00000000-0000-0000-0000-0000{0}", number.Next(10000000, 99999999)));
                            uuidDict.Add(key, string.Format("00000000-0000-0000-0000-000{0}", number.Next(100000000, 499999999)));
                            row["Uuid"] = uuidDict[key];
                        }
                    }
                    break;
                case "VWT":
                    foreach (DataRow row in dtData.Rows)
                    {
                        string key = row["Well"].ToString();// Now WellName is replaced with Well Number which does not contain a long name. (row["Well"].ToString().Substring(row["Well"].ToString().Length - 6, 6)).Replace("-", ""); //Extracting Well Number out of the Well name

                        if (uuidDict != null && uuidDict.ContainsKey(key))
                        {
                            row["Uuid"] = uuidDict[key];
                        }
                        else
                        {
                            string api = row["Api14"].ToString();

                            uuidDict.Add(key, string.Format("00000000-0000-0000-00{0}-{1}", api.Substring(0, 2), api.Substring(2, api.Length - 2)));
                            row["Uuid"] = uuidDict[key];
                        }
                    }
                    dtData.Columns.Remove("Api14");
                    break;
            }
            return dtData;
        }

        public void setImportModeConfigs(object sender, EventArgs e)
        {
            if (dataView.isSourceFiles) // Import mode on
            {
                dataView.isImportProdmlEnabled = true;
                dataView.isExportProdmlEnabled = false;
                dataView.isCitationEnabled = false;
                dataView.isExportHierarchyEntity = false;
                dataView.isExpHierarchyEntityEnabled = false;
                //dataView.isTreeEnabled = false;
                dataView.citationDesc = "";
                clearDataGrids();
            }
            else // Import mode off
            {
                dataView.isImportProdmlEnabled = false;
                dataView.isExportProdmlEnabled = true;
                dataView.isCitationEnabled = true;
                dataView.isGetDataEnabled = true;
                dataView.isExpHierarchyEntityEnabled = true;
                dataView.isTreeEnabled = true;
                dataView.citationDesc = "";
                clearDataGrids();
            }
        }

        public void clearDataGrids()
        {
            ((DataTable)dataView.dgvEquipRelibltyData.DataSource).Clear();
            ((DataTable)dataView.dgvProdInfoData.DataSource).Clear();
            ((DataTable)dataView.dgvWellTestData.DataSource).Clear();
        }

        #endregion

        #region Configuration Maps

        private void getColumnMappings() // Gets the column mappings for all the data grids from the configuration file.
        {
            GridColumnCollection gridColCollection = new GridColumnCollection();

            //Loading configuration file from appdata folder
            string baseConfigPath = GetBaseConfigPath();
            configMap.ExeConfigFilename = @baseConfigPath;
            System.Configuration.Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);

            //Get Grid Columns Collection
            string colSec = "GridConfigurations/GridColumnConfigurationSection";
            GridColumnConfigurationSection gridConfigSection = (GridColumnConfigurationSection)config.GetSection(colSec);
            gridColCollection = gridConfigSection.GridColumns;

            // Can have an active column check here so the foreach loop would traverse only selected column.
            foreach (GridColumn col in gridColCollection) //Creating columns in the grids
            {
                if (col.ReportType == "PROD")
                {
                    Tuple<string, string, string, string, string, string, string, Tuple<string>> tuple = Tuple.Create(col.ShortName, col.LongName, col.SourceName, col.DataType, col.Uom, col.EnergyUom, col.Element, col.ReportType);    //new Tuple<string, string, string, string, string, string, string, Tuple<string>>(col.ShortName, col.LongName, col.SourceName, col.DataType, col.Uom, col.EnergyUom, col.Element, col.ReportType);
                    colMappingsList.Add(tuple);
                }
            }
        }

        private void getFluidCompCatalogMappings() // Gets the Fluid Component Catalog mappings for Production Information data grid.
        {
            FluidCompCatalogCollection fluidCompCollection = new FluidCompCatalogCollection();

            //Loading configuration file from appdata folder
            string baseConfigPath = GetBaseConfigPath();
            configMap.ExeConfigFilename = @baseConfigPath;
            System.Configuration.Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);

            //Get Grid Columns Collection
            string compCatalogSec = "FluidCompCatalogConfigs/FluidCompCatalogConfigSection";
            FluidCompCatalogConfigSection fluidCompConfigSection = (FluidCompCatalogConfigSection)config.GetSection(compCatalogSec);

            fluidCompCollection = fluidCompConfigSection.FluidCompCatalogs;

            // Can have an active column check here so the foreach loop would traverse only selected column.
            foreach (FluidCompCatalog catalog in fluidCompCollection) //Creating catalog mappings for PI grid
            {
                Tuple<string, string, string, string, string, string, string> catalogTuple = new Tuple<string, string, string, string, string, string, string>(catalog.Uid, catalog.Uom, catalog.EnergisticsUom, catalog.RepEntityVols, catalog.DispositionKind, catalog.ProdFluidKind, catalog.XmlElement);
                fluidCompCatalogList.Add(catalogTuple);
                fluidKindDict.Add(catalog.Uid, new Tuple<string,string>(catalog.DispositionKind,catalog.ProdFluidKind)); // Populating the dictionary with the parameters of the configuration not shown in the data grid.
            }
        }

        #endregion

        #region Export Operation Methods

        private Citation getCitation(string title, string description)
        {
            Citation citationObj = new Citation();
            citationObj.Title = title;
            citationObj.Originator = UserPrincipal.Current.DisplayName;
            citationObj.Creation = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd hh:mm:ss}", DateTime.Now));
            //citationObj.Creation = Convert.ToDateTime(string.Format("{0:yyyy-MM-dd hh:mm:ss zzz}", DateTime.Now));//DateTime.Parse("2017-02-03T09:30:47Z"); // Correct the format
            citationObj.Format = "PRODML File Generator";//Form.ActiveForm.Text; // Can change in case of MDI forms
            citationObj.Description = string.Format("{0}_{1}",description,(Convert.ToDateTime(dataView.reportDate)).Date.ToString("yyyy-MM-dd"));

            return citationObj;
        }

        private ObjectAlias getAliases(string authority, string description, string identifier) // There is no detail about any of the Alias objects in the Ref guide
        {
            ObjectAlias aliasesObj = new ObjectAlias();

            aliasesObj.Authority = authority;
            aliasesObj.Identifier = (identifier != null && identifier != string.Empty) ? identifier : Guid.NewGuid().ToString();
            aliasesObj.Description = description;

            return aliasesObj;

        }

        #endregion
    }
}