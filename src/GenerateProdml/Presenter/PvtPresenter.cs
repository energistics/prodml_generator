 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data;
using System.Configuration;
using GenerateProdml.Configuration;
using System.Xml;
using System.Xml.Linq;
using GenerateProdml.View;
using GenerateProdml.Model;
using GenerateProdml.Utility;
using Energistics.DataAccess;
using Energistics.DataAccess.PRODML200;
using Energistics.DataAccess.PRODML200.ComponentSchemas;
using Energistics.DataAccess.PRODML200.ReferenceData;
using System.Reflection;
using System.DirectoryServices.AccountManagement;
using System.Xml.Schema;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace GenerateProdml.Presenter
{
    class PvtPresenter
    {
        #region Variable Initialization

        IPvtView dataView;
        DataSet dsZeroFlash = new DataSet();
        DataSet dsDiffLab = new DataSet();
        DataSet dsCCE = new DataSet();
        DataSet dsCVD = new DataSet();
        DataSet dsSaturation = new DataSet();
        DataSet dsSwelling = new DataSet();
        DataSet dsViscosity = new DataSet();
        GridUtils dataGridUtil = new GridUtils();
        List<Tuple<string, string, string, string, string, string, string, Tuple<string>>> colMappingsList = new List<Tuple<string, string, string, string, string, string, string, Tuple<string>>>(); // To get all the column mappings from app.config
        public const string ConfigPathString = "GenerateProdml.exe.config"; // Changed for this app
        public static string GetBaseConfigPath() { return AppDomain.CurrentDomain.BaseDirectory + ConfigPathString; } // Changed for this app
        ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
        string exprtFileDir = "";
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region Initialize Presenter
 
        public PvtPresenter(IPvtView view)
        {
            try
            {
                dataView = view;
                getColumnMappings();
                initializeEvents();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Load Event Handlers

        private void initializeEvents() //initializes all the events
        {
            log.Info("Initializing event handlers for PVT forms.");

            dataView.copyMenuEventHandler += (execRowOperation);
            dataView.copyWithHeadersMenuEventHandler += (execRowOperation);
            dataView.pasteMenuEventHandler += (execRowOperation);
        }

        #endregion

        #region Browse Export Directory

        public void browseExportFileDir() // To browse for the PRODML Export file Directory.
        {
            if(saveFirstExportData() == 1) // Save data from the current data grid and in case if there is no data available then prompt user
            {
                MessageBox.Show("No data available to export.", "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            bool isDataAvailable = false;
            FolderBrowserDialog foldrDialogObj = new FolderBrowserDialog();
            foldrDialogObj.Description = "Choose a directory to export.";

            if (exprtFileDir != "" && exprtFileDir != null && exprtFileDir != String.Empty && Directory.Exists(exprtFileDir))
            {
                foldrDialogObj.SelectedPath = exprtFileDir; // Set the last selected directory as current selection
            }

            foreach(DataTable dtData in dsZeroFlash.Tables)
            {
                if(dtData.Rows.Count > 0)
                {
                    isDataAvailable = true;
                    break;
                }
            }

            foreach (DataTable dtData in dsSaturation.Tables)
            {
                if(isDataAvailable)
                {
                    break;
                }
                if (dtData.Rows.Count > 0)
                {
                    isDataAvailable = true;
                    break;
                }
            }

            foreach (DataTable dtData in dsCCE.Tables)
            {
                if (isDataAvailable)
                {
                    break;
                }
                if (dtData.Rows.Count > 0)
                {
                    isDataAvailable = true;
                    break;
                }
            }

            foreach (DataTable dtData in dsDiffLab.Tables)
            {
                if (isDataAvailable)
                {
                    break;
                }
                if (dtData.Rows.Count > 0)
                {
                    isDataAvailable = true;
                    break;
                }
            }

            foreach (DataTable dtData in dsCVD.Tables)
            {
                if (isDataAvailable)
                {
                    break;
                }
                if (dtData.Rows.Count > 0)
                {
                    isDataAvailable = true;
                    break;
                }
            }

            foreach (DataTable dtData in dsSwelling.Tables)
            {
                if (isDataAvailable)
                {
                    break;
                }
                if (dtData.Rows.Count > 0)
                {
                    isDataAvailable = true;
                    break;
                }
            }

            if (!isDataAvailable)
            {
                MessageBox.Show("No data available to export.", "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (foldrDialogObj.ShowDialog() == DialogResult.OK)
                {
                    exprtFileDir = foldrDialogObj.SelectedPath;
                    try
                    {
                        log.Info("Export commenced.");

                        exportPVT(exprtFileDir);

                        log.Info("Export sucessful.");
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType() == typeof(System.FormatException))
                        {
                            log.Error("A problem occurred while exporting XML(s). ", ex);
                            MessageBox.Show("An error occurred while export due to a null or invalid value in data. The xml(s) will not be generated.", "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            log.Error("A problem occurred while exporting XML(s). ", ex);
                            MessageBox.Show(string.Format("An error occurred while Export. {0}", ex.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }

        #endregion

        #region Select Test Datagridview

        public void setVisibility(string testTyp)
        {
            if (testTyp == "ZF" || testTyp == "SAT")
            {
                dataView.isSet0Visible = true;
                dataView.isTabCtrVisible = false;
            }
            else if(testTyp == "COMP" || testTyp == "SEP")
            {
                dataView.isSet0Visible = false;
                dataView.isTabCtrVisible = false;
            }
            else
            {
                dataView.isSet0Visible = false;
                dataView.isTabCtrVisible = true;
            }
            setTestName(testTyp);
        }

        private void setTestName(string testTyp)
        {
            switch(testTyp)
            {
                case "SAT":
                    dataView.testDescName = "Saturation Points";
                    break;

                case "ZF":
                    dataView.testDescName = "Zero - Flash";
                    break;

                case "CCE":
                    dataView.testDescName = "Constant Composition Expansion";
                    break;

                case "DL":
                    dataView.testDescName = "Differential Liberation";
                    break;

                case "CVD":
                    dataView.testDescName = "Constant Volume Depletion";
                    break;

                case "VIS":
                    dataView.testDescName = "Live Oil Viscosity";
                    break;

                case "SWE":
                    dataView.testDescName = "Swelling Test";
                    break;

                default:
                    dataView.testDescName = "";
                    break;
            }
        }

        #endregion

        #region Get Energistics Uom

        public string getEnergisticsUomName(string name)
        {
            if (name.Contains('/'))
            {
                name = name.Remove(name.IndexOf('/'), 1);
            }

            if (System.Text.RegularExpressions.Regex.IsMatch(name, @"^\d")) // Check if the units of measure starts with a number, then concatenate "Item." with it to use reflection to set its Uom
            {
                name = string.Format("Item{0}", name);
            }

            if (name == "%")
            {
                name = "Item";
            }

            //if(name == "psig")
            //{
            //    name = "psi";
            //}

            return name;
        }

        #endregion

        #region Set Data

        public void setData(string testTyp)
        {
            DataTable dtDataSource = new DataTable();
            bool isTabs = false;
            
            if(testTyp == "ZF" || testTyp == "SAT")
            {
                dataView.dgvDataSet0.DataSource = null;
                dataView.dgvDataSet0.Rows.Clear();
                dataView.dgvDataSet0.Columns.Clear();
            }
            else
            {
                dataView.dgvDataSet1.DataSource = null;
                dataView.dgvDataSet2.DataSource = null;
                dataView.dgvDataSet3.DataSource = null;
                dataView.dgvDataSet4.DataSource = null;
                dataView.dgvDataSet5.DataSource = null;
                dataView.dgvDataSet1.Rows.Clear();
                dataView.dgvDataSet1.Columns.Clear();
                dataView.dgvDataSet2.Rows.Clear();
                dataView.dgvDataSet2.Columns.Clear();
                dataView.dgvDataSet3.Rows.Clear();
                dataView.dgvDataSet3.Columns.Clear();
                dataView.dgvDataSet4.Rows.Clear();
                dataView.dgvDataSet4.Columns.Clear();
                dataView.dgvDataSet5.Rows.Clear();
                dataView.dgvDataSet5.Columns.Clear();
            }

            foreach (Tuple<string, string, string, string, string, string, string, Tuple<string>> rowTuple in colMappingsList) //Creating columns in the grids
            {
                string[] testNames = rowTuple.Item7.Split('/'); // Item7 is the Element name. Identifying all the grids where the column(Item1) is to be added.

                foreach (string name in testNames)
                {
                    if (name == testTyp && rowTuple.Rest.Item1 == "PVT")
                    {
                        if (name == "ZF" || name == "SAT")
                        {
                            dataView.dgvDataSet0.Columns.Add(rowTuple.Item1, ((rowTuple.Item5 != "") ? String.Format("{0} ({1})", rowTuple.Item2, rowTuple.Item5) : rowTuple.Item2)); //Mapping column long names to header text in grid
                            dataView.dgvDataSet0.Columns[rowTuple.Item1].DataPropertyName = rowTuple.Item1;
                            dtDataSource.Columns.Add(rowTuple.Item1, Type.GetType(string.Format("System.{0}", rowTuple.Item4), true, true)); //For Data Binding
                            if (rowTuple.Item4.ToLower() == "double")
                            {
                                dataView.dgvDataSet0.Columns[rowTuple.Item1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight; // Right Aligining the columns having decimal values
                            }
                        }
                        else
                        {
                            dataView.dgvDataSet1.Columns.Add(rowTuple.Item1, ((rowTuple.Item5 != "") ? String.Format("{0} ({1})", rowTuple.Item2, rowTuple.Item5) : rowTuple.Item2)); //Mapping column long names to header text in grid
                            dataView.dgvDataSet1.Columns[rowTuple.Item1].DataPropertyName = rowTuple.Item1;
                            dataView.dgvDataSet2.Columns.Add(rowTuple.Item1, ((rowTuple.Item5 != "") ? String.Format("{0} ({1})", rowTuple.Item2, rowTuple.Item5) : rowTuple.Item2)); //Mapping column long names to header text in grid
                            dataView.dgvDataSet2.Columns[rowTuple.Item1].DataPropertyName = rowTuple.Item1;
                            dataView.dgvDataSet3.Columns.Add(rowTuple.Item1, ((rowTuple.Item5 != "") ? String.Format("{0} ({1})", rowTuple.Item2, rowTuple.Item5) : rowTuple.Item2)); //Mapping column long names to header text in grid
                            dataView.dgvDataSet3.Columns[rowTuple.Item1].DataPropertyName = rowTuple.Item1;
                            dataView.dgvDataSet4.Columns.Add(rowTuple.Item1, ((rowTuple.Item5 != "") ? String.Format("{0} ({1})", rowTuple.Item2, rowTuple.Item5) : rowTuple.Item2)); //Mapping column long names to header text in grid
                            dataView.dgvDataSet4.Columns[rowTuple.Item1].DataPropertyName = rowTuple.Item1;
                            dataView.dgvDataSet5.Columns.Add(rowTuple.Item1, ((rowTuple.Item5 != "") ? String.Format("{0} ({1})", rowTuple.Item2, rowTuple.Item5) : rowTuple.Item2)); //Mapping column long names to header text in grid
                            dataView.dgvDataSet5.Columns[rowTuple.Item1].DataPropertyName = rowTuple.Item1;
                            dtDataSource.Columns.Add(rowTuple.Item1, Type.GetType(string.Format("System.{0}", rowTuple.Item4), true, true)); //For Data Binding
                            if (rowTuple.Item4.ToLower() == "double")
                            {
                                dataView.dgvDataSet1.Columns[rowTuple.Item1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight; // Right Aligining the columns having decimal values
                                dataView.dgvDataSet2.Columns[rowTuple.Item1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight; // Right Aligining the columns having decimal values
                                dataView.dgvDataSet3.Columns[rowTuple.Item1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight; // Right Aligining the columns having decimal values
                                dataView.dgvDataSet4.Columns[rowTuple.Item1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight; // Right Aligining the columns having decimal values
                                dataView.dgvDataSet5.Columns[rowTuple.Item1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight; // Right Aligining the columns having decimal values
                            }
                            isTabs = true; // To prompt if there is a datagrid with tabs being populated 
                        }
                    }
                }
                if(isTabs)
                {
                    dataView.dgvDataSet1.DataSource = dtDataSource.Copy();
                    dataView.dgvDataSet2.DataSource = dtDataSource.Copy();
                    dataView.dgvDataSet3.DataSource = dtDataSource.Copy();
                    dataView.dgvDataSet4.DataSource = dtDataSource.Copy();
                    dataView.dgvDataSet5.DataSource = dtDataSource.Copy();
                    isTabs = false;
                }
                else
                {
                    dataView.dgvDataSet0.DataSource = dtDataSource.Copy();
                }
            }
            setVisibility(testTyp);
            loadDataToGrid(testTyp);
        }

        public void loadDataToGrid(string testTyp)
        {
            switch(testTyp)
            {
                case "ZF":
                    if (dsZeroFlash.Tables.Count > 0)
                    {
                        for (int i = 0; i < dsZeroFlash.Tables.Count; i++)
                        {
                            if (dsZeroFlash.Tables[i].Rows.Count > 0)
                            {
                                dataView.dgvDataSet0.DataSource = dsZeroFlash.Tables[i];
                            }
                        }
                    }
                    break;

                case "SAT":
                    if (dsSaturation.Tables.Count > 0)
                    {
                        for (int i = 0; i < dsSaturation.Tables.Count; i++)
                        {
                            if (dsSaturation.Tables[i].Rows.Count > 0)
                            {
                                dataView.dgvDataSet0.DataSource = dsSaturation.Tables[i];
                            }
                        }
                    }
                    break;

                case "CCE":
                    if (dsCCE.Tables.Count > 0)
                    {
                        for (int i = 0; i < dsCCE.Tables.Count; i++)
                        {
                            if (dsCCE.Tables[i].Rows.Count > 0)
                            {
                                switch (i)
                                {
                                    case 0:
                                        dataView.dgvDataSet1.DataSource = dsCCE.Tables[i];
                                        break;

                                    case 1:
                                        dataView.dgvDataSet2.DataSource = dsCCE.Tables[i];
                                        break;

                                    case 2:
                                        dataView.dgvDataSet3.DataSource = dsCCE.Tables[i];
                                        break;

                                    case 3:
                                        dataView.dgvDataSet4.DataSource = dsCCE.Tables[i];
                                        break;

                                    case 4:
                                        dataView.dgvDataSet5.DataSource = dsCCE.Tables[i];
                                        break;
                                }
                            }
                        }
                    }
                    break;

                case "DL":
                    if (dsDiffLab.Tables.Count > 0)
                    {
                        for (int i = 0; i < dsDiffLab.Tables.Count; i++)
                        {
                            if (dsDiffLab.Tables[i].Rows.Count > 0)
                            {
                                switch (i)
                                {
                                    case 0:
                                        dataView.dgvDataSet1.DataSource = dsDiffLab.Tables[i];
                                        break;

                                    case 1:
                                        dataView.dgvDataSet2.DataSource = dsDiffLab.Tables[i];
                                        break;

                                    case 2:
                                        dataView.dgvDataSet3.DataSource = dsDiffLab.Tables[i];
                                        break;

                                    case 3:
                                        dataView.dgvDataSet4.DataSource = dsDiffLab.Tables[i];
                                        break;

                                    case 4:
                                        dataView.dgvDataSet5.DataSource = dsDiffLab.Tables[i];
                                        break;
                                }
                            }
                        }
                    }
                    break;

                case "CVD":
                    if (dsCVD.Tables.Count > 0)
                    {
                        for (int i = 0; i < dsCVD.Tables.Count; i++)
                        {
                            if (dsCVD.Tables[i].Rows.Count > 0)
                            {
                                switch (i)
                                {
                                    case 0:
                                        dataView.dgvDataSet1.DataSource = dsCVD.Tables[i];
                                        break;

                                    case 1:
                                        dataView.dgvDataSet2.DataSource = dsCVD.Tables[i];
                                        break;

                                    case 2:
                                        dataView.dgvDataSet3.DataSource = dsCVD.Tables[i];
                                        break;

                                    case 3:
                                        dataView.dgvDataSet4.DataSource = dsCVD.Tables[i];
                                        break;

                                    case 4:
                                        dataView.dgvDataSet5.DataSource = dsCVD.Tables[i];
                                        break;
                                }
                            }
                        }
                    }
                    break;

                case "VIS":
                    if (dsViscosity.Tables.Count > 0)
                    {
                        for (int i = 0; i < dsViscosity.Tables.Count; i++)
                        {
                            if (dsViscosity.Tables[i].Rows.Count > 0)
                            {
                                switch (i)
                                {
                                    case 0:
                                        dataView.dgvDataSet1.DataSource = dsViscosity.Tables[i];
                                        break;

                                    case 1:
                                        dataView.dgvDataSet2.DataSource = dsViscosity.Tables[i];
                                        break;

                                    case 2:
                                        dataView.dgvDataSet3.DataSource = dsViscosity.Tables[i];
                                        break;

                                    case 3:
                                        dataView.dgvDataSet4.DataSource = dsViscosity.Tables[i];
                                        break;

                                    case 4:
                                        dataView.dgvDataSet5.DataSource = dsViscosity.Tables[i];
                                        break;
                                }
                            }
                        }
                    }
                    break;

                case "SWE":
                    if (dsSwelling.Tables.Count > 0)
                    {
                        for (int i = 0; i < dsSwelling.Tables.Count; i++)
                        {
                            if (dsSwelling.Tables[i].Rows.Count > 0)
                            {
                                switch (i)
                                {
                                    case 0:
                                        dataView.dgvDataSet1.DataSource = dsSwelling.Tables[i];
                                        break;

                                    case 1:
                                        dataView.dgvDataSet2.DataSource = dsSwelling.Tables[i];
                                        break;

                                    case 2:
                                        dataView.dgvDataSet3.DataSource = dsSwelling.Tables[i];
                                        break;

                                    case 3:
                                        dataView.dgvDataSet4.DataSource = dsSwelling.Tables[i];
                                        break;

                                    case 4:
                                        dataView.dgvDataSet5.DataSource = dsSwelling.Tables[i];
                                        break;
                                }
                            }
                        }
                    }
                    break;
            }
        }

        public void clearDataGrids(bool isTabs)
        {
            if(isTabs)
            {
                dataView.dgvDataSet1.DataSource = null;
                dataView.dgvDataSet2.DataSource = null;
                dataView.dgvDataSet3.DataSource = null;
                dataView.dgvDataSet4.DataSource = null;
                dataView.dgvDataSet5.DataSource = null;
            }
            else
            {
                dataView.dgvDataSet0.DataSource = null;
            }
        }

        #endregion

        #region Save data on Grid

        private int saveFirstExportData() // Used to save data on the grid if user is on a current screen and presses export
        {
            if(dataView.isZFselected)
            {
                saveDataOnGrid("ZF");
                return 0;
            }
            else if(dataView.isSATselected)
            {
                saveDataOnGrid("SAT");
                return 0;
            }
            else if(dataView.isCCEselected)
            {
                saveDataOnGrid("CCE");
                return 0;
            }
            else if(dataView.isDLselected)
            {
                saveDataOnGrid("DL");
                return 0;
            }
            else if(dataView.isCVDselected)
            {
                saveDataOnGrid("CVD");
                return 0;
            }
            else if(dataView.isSWEselected)
            {
                saveDataOnGrid("SWE");
                return 0;
            }
            else
            {
                return 1;
            }
        }

        public void saveDataOnGrid(string testTyp)
        {
            switch (testTyp)
            {
                case "ZF":
                    dsZeroFlash.Tables.Clear();
                    dsZeroFlash.Tables.Add(((DataTable)dataView.dgvDataSet0.DataSource).Copy());
                    break;

                case "SAT":
                    dsSaturation.Tables.Clear();
                    dsSaturation.Tables.Add(((DataTable)dataView.dgvDataSet0.DataSource).Copy());
                    break;

                case "CCE":
                    dsCCE.Tables.Clear();
                    dsCCE.Tables.Add(((DataTable)dataView.dgvDataSet1.DataSource).Copy());
                    dsCCE.Tables.Add(((DataTable)dataView.dgvDataSet2.DataSource).Copy());
                    dsCCE.Tables.Add(((DataTable)dataView.dgvDataSet3.DataSource).Copy());
                    dsCCE.Tables.Add(((DataTable)dataView.dgvDataSet4.DataSource).Copy());
                    dsCCE.Tables.Add(((DataTable)dataView.dgvDataSet5.DataSource).Copy());
                    break;

                case "DL":
                    dsDiffLab.Tables.Clear();
                    dsDiffLab.Tables.Add(((DataTable)dataView.dgvDataSet1.DataSource).Copy());
                    dsDiffLab.Tables.Add(((DataTable)dataView.dgvDataSet2.DataSource).Copy());
                    dsDiffLab.Tables.Add(((DataTable)dataView.dgvDataSet3.DataSource).Copy());
                    dsDiffLab.Tables.Add(((DataTable)dataView.dgvDataSet4.DataSource).Copy());
                    dsDiffLab.Tables.Add(((DataTable)dataView.dgvDataSet5.DataSource).Copy());
                    break;

                case "CVD":
                    dsCVD.Tables.Clear();
                    dsCVD.Tables.Add(((DataTable)dataView.dgvDataSet1.DataSource).Copy());
                    dsCVD.Tables.Add(((DataTable)dataView.dgvDataSet2.DataSource).Copy());
                    dsCVD.Tables.Add(((DataTable)dataView.dgvDataSet3.DataSource).Copy());
                    dsCVD.Tables.Add(((DataTable)dataView.dgvDataSet4.DataSource).Copy());
                    dsCVD.Tables.Add(((DataTable)dataView.dgvDataSet5.DataSource).Copy());
                    break;

                case "VIS":
                    dsViscosity.Tables.Clear();
                    dsViscosity.Tables.Add(((DataTable)dataView.dgvDataSet1.DataSource).Copy());
                    dsViscosity.Tables.Add(((DataTable)dataView.dgvDataSet2.DataSource).Copy());
                    dsViscosity.Tables.Add(((DataTable)dataView.dgvDataSet3.DataSource).Copy());
                    dsViscosity.Tables.Add(((DataTable)dataView.dgvDataSet4.DataSource).Copy());
                    dsViscosity.Tables.Add(((DataTable)dataView.dgvDataSet5.DataSource).Copy());
                    break;

                case "SWE":
                    dsSwelling.Tables.Clear();
                    dsSwelling.Tables.Add(((DataTable)dataView.dgvDataSet1.DataSource).Copy());
                    dsSwelling.Tables.Add(((DataTable)dataView.dgvDataSet2.DataSource).Copy());
                    dsSwelling.Tables.Add(((DataTable)dataView.dgvDataSet3.DataSource).Copy());
                    dsSwelling.Tables.Add(((DataTable)dataView.dgvDataSet4.DataSource).Copy());
                    dsSwelling.Tables.Add(((DataTable)dataView.dgvDataSet5.DataSource).Copy());
                    break;
            }
        }

        #endregion

        #region Export PVT

        public void exportPVT(string fileDir)
        {
            FluidAnalysis fluidAnalysisObj = new FluidAnalysis();
            HydrocarbonAnalysis hydroCarbonAnalysisObj = new HydrocarbonAnalysis();
            List<DifferentialLiberationTest> diffLibTestList = new List<DifferentialLiberationTest>(); //Differential Liberation Tests List
            List<ConstantCompositionExpansionTest> constCompExpTestList = new List<ConstantCompositionExpansionTest>();
            List<ConstantVolumeDepletionTest> constVolDepTestList = new List<ConstantVolumeDepletionTest>();
            //There is no place holder for Viscosity Test in HydrocarbonAnalysis Object or either FluidAnalysis Object
            List<SwellingTest> swellingTestList = new List<SwellingTest>();
            List<AtmosphericFlashTestAndCompositionalAnalysis> zeroFlashTestList = new List<AtmosphericFlashTestAndCompositionalAnalysis>();
            List<SaturationTest> satTestList = new List<SaturationTest>();

            #region Export DiffLib

            if (dsDiffLab.Tables.Count > 0) // Check if Differential Liberation data is input by user
            {
                //List<FluidDifferentialLiberationTestStep> diffLibTestStepList = new List<FluidDifferentialLiberationTestStep>();
                int testNumber = 1;
                int stepNumber = 1;

                foreach(DataTable dtDLdata in dsDiffLab.Tables)
                {
                    List<FluidDifferentialLiberationTestStep> diffLibTestStepList = new List<FluidDifferentialLiberationTestStep>();
                    stepNumber = 1;
                    DifferentialLiberationTest diffLibTestObj = new DifferentialLiberationTest();
                    diffLibTestObj.Uid = string.Format("DiffLibTest{0}", testNumber);
                    diffLibTestObj.TestNumber = testNumber;
                    diffLibTestObj.TestTemperature = new ThermodynamicTemperatureMeasure { Uom = ThermodynamicTemperatureUom.degF, Value = 220 };
                    diffLibTestObj.SaturationPressure = new SaturationPressure { Kind = SaturationPointKind.bubblepoint, Uom = "psi", Value = 2620 };
                    diffLibTestObj.SeparatorConditions = new SeparatorConditions { SeparatorTestReference = "Sep1" };
                    diffLibTestObj.CorrectionMethod = "McCain";

                    foreach(DataRow drDLdata in dtDLdata.Rows)
                    {
                        FluidDifferentialLiberationTestStep fluidDiffLibTestObj = new FluidDifferentialLiberationTestStep();
                        fluidDiffLibTestObj.Uid = string.Format("step{0}", stepNumber);
                        fluidDiffLibTestObj.StepNumber = stepNumber;

                        foreach (Tuple<string, string, string, string, string, string, string, Tuple<string>> value in colMappingsList)
                        {
                            if (value.Rest.Item1 == "PVT")
                            {
                                string colName = value.Item1;
                                string uom = value.Item6;
                                string testTyp = value.Item7;

                                if ((dtDLdata.Columns.Contains(colName)) && (testTyp == "DL" || testTyp.StartsWith("DL/") || testTyp.EndsWith("/DL") || testTyp.Contains("/DL/")))
                                {
                                    switch (colName)
                                    {
                                        case "Temperature":
                                            fluidDiffLibTestObj.StepTemperature = new ThermodynamicTemperatureMeasure { Value = (drDLdata[colName].ToString() == "") ? double.NaN : double.Parse(drDLdata[colName].ToString()), Uom = (ThermodynamicTemperatureUom)((typeof(ThermodynamicTemperatureUom).GetField(uom)).GetValue(null)) };
                                            break;

                                        case "Pressure":
                                            fluidDiffLibTestObj.StepPressure = new PressureMeasure { Value = (drDLdata[colName].ToString() == "") ? double.NaN : double.Parse(drDLdata[colName].ToString()), Uom = uom };
                                            break;

                                        case "OilFVF":
                                            fluidDiffLibTestObj.OilFormationVolumeFactor = new VolumePerVolumeMeasure { Value = (drDLdata[colName].ToString() == "") ? double.NaN : double.Parse(drDLdata[colName].ToString()), Uom = uom };
                                            break;

                                        case "SolGOR":
                                            fluidDiffLibTestObj.SolutionGORMeasured = new VolumePerVolumeMeasure { Value = (drDLdata[colName].ToString() == "") ? double.NaN : double.Parse(drDLdata[colName].ToString()), Uom = uom };
                                            break;

                                        case "LiquidDensity":
                                            fluidDiffLibTestObj.OilDensity = new MassPerVolumeMeasure { Value = (drDLdata[colName].ToString() == "") ? double.NaN : double.Parse(drDLdata[colName].ToString()), Uom = uom };
                                            break;

                                        case "GasFVF":
                                            fluidDiffLibTestObj.GasFormationVolumeFactor = new VolumePerVolumeMeasure { Value = (drDLdata[colName].ToString() == "") ? double.NaN : double.Parse(drDLdata[colName].ToString()), Uom = uom };
                                            break;

                                        case "GasZ":
                                            fluidDiffLibTestObj.GasZFactor = (drDLdata[colName].ToString() == "") ? double.NaN : double.Parse(drDLdata[colName].ToString());
                                            break;

                                        case "GasGravity":
                                            fluidDiffLibTestObj.GasGravity = (drDLdata[colName].ToString() == "") ? double.NaN : double.Parse(drDLdata[colName].ToString());
                                            break;
                                    }
                                }
                            }
                        }

                        if(double.IsNaN(fluidDiffLibTestObj.GasFormationVolumeFactor.Value)) // If no gas present
                        {
                            if(!(double.IsNaN(fluidDiffLibTestObj.OilFormationVolumeFactor.Value))) // If only oil present
                            {
                                fluidDiffLibTestObj.PhasesPresent = PhasePresent.oil;
                            }
                        }
                        else // gas is present
                        {
                            if(double.IsNaN(fluidDiffLibTestObj.OilFormationVolumeFactor.Value)) // If no oil present
                            {
                                fluidDiffLibTestObj.PhasesPresent = PhasePresent.gas;
                            }
                            else
                            {
                                fluidDiffLibTestObj.PhasesPresent = PhasePresent.oilandgas;
                            }
                        }
                        
                        diffLibTestStepList.Add(fluidDiffLibTestObj);
                        stepNumber++;
                    }
                    diffLibTestObj.DLTestStep = diffLibTestStepList;
                    diffLibTestList.Add(diffLibTestObj);
                    testNumber++;
                }
            }

            #endregion           

            #region Export CCE

            if (dsCCE.Tables.Count > 0) // Check if Differential Liberation data is input by user
            {
                int testNumber = 1;
                int stepNumber = 1;

                foreach(DataTable dtCCEdata in dsCCE.Tables)
                {
                    stepNumber = 1;
                    List<ConstantCompositionExpansionTestStep> constCompTestStepList = new List<ConstantCompositionExpansionTestStep>();
                    ConstantCompositionExpansionTest constCompExpTestObj = new ConstantCompositionExpansionTest();
                    constCompExpTestObj.TestNumber = testNumber;
                    constCompExpTestObj.Uid = string.Format("CCETest{0}", testNumber);
                    constCompExpTestObj.TestTemperature = new ThermodynamicTemperatureMeasure { Uom = ThermodynamicTemperatureUom.degF, Value = 220 };
                    constCompExpTestObj.SaturationPressure = new SaturationPressure { Value = 2620, Uom = "psi", Kind = SaturationPointKind.bubblepoint };
                    FluidVolumeReference fluidVolRefObj = new FluidVolumeReference();
                    fluidVolRefObj.Uid = "MSAT";
                    fluidVolRefObj.Kind = VolumeReferenceKind.saturationmeasured;
                    fluidVolRefObj.ReferenceVolume = new VolumeMeasure { Value = 10, Uom = "mL" };
                    List<FluidVolumeReference> fluidVolRefList = new List<FluidVolumeReference>();
                    fluidVolRefList.Add(fluidVolRefObj);
                    constCompExpTestObj.RelativeVolumeReference = fluidVolRefList;

                    //constCompExpTestObj.ConstantCompositionExpansionTestStep

                    foreach (DataRow drCCEdata in dtCCEdata.Rows)
                    {
                        ConstantCompositionExpansionTestStep constCompExpTestStepObj = new ConstantCompositionExpansionTestStep();
                        constCompExpTestStepObj.Uid = string.Format("step{0}", stepNumber);
                        constCompExpTestStepObj.StepNumber = stepNumber;

                        foreach (Tuple<string, string, string, string, string, string, string, Tuple<string>> value in colMappingsList)
                        {
                            if (value.Rest.Item1 == "PVT")
                            {
                                string colName = value.Item1;
                                string uom = value.Item6;
                                string testTyp = value.Item7;

                                if ((dtCCEdata.Columns.Contains(colName)) && (testTyp == "CCE" || testTyp.StartsWith("CCE/") || testTyp.EndsWith("/CCE") || testTyp.Contains("/CCE/")))
                                {
                                    switch (colName)
                                    {
                                         //Temperature place holder missing in Energistics

                                        case "Pressure":
                                            constCompExpTestStepObj.StepPressure = new PressureMeasure { Value = (drCCEdata[colName].ToString() == "") ? double.NaN : double.Parse(drCCEdata[colName].ToString()), Uom = uom };
                                            break;

                                        case "RelativeVolume":
                                            constCompExpTestStepObj.RelativeVolumeRatio = new RelativeVolumeRatio { Value = (drCCEdata[colName].ToString() == "") ? double.NaN : double.Parse(drCCEdata[colName].ToString()), Uom = uom, FluidVolumeReference = "MSAT" };
                                            break;

                                        // Liq.Vol (%Vsat) place holder missing or unknown in Energistics

                                        case "LiquidDensity":
                                            constCompExpTestStepObj.OilDensity = new MassPerVolumeMeasure { Value = (drCCEdata[colName].ToString() == "") ? double.NaN : double.Parse(drCCEdata[colName].ToString()), Uom = uom };
                                            break;

                                        case "YFunction":
                                            constCompExpTestStepObj.YFunction = (drCCEdata[colName].ToString() == "") ? double.NaN : double.Parse(drCCEdata[colName].ToString());
                                            break;

                                        case "Compress":
                                            constCompExpTestStepObj.OilCompressibility = new OilCompressibility { Value = (drCCEdata[colName].ToString() == "") ? double.NaN : double.Parse(drCCEdata[colName].ToString()), Uom = (ReciprocalPressureUom)(typeof(ReciprocalPressureUom).GetField(getEnergisticsUomName(uom)).GetValue(null)), Kind = "average" };
                                            break;

                                        case "GasZ":
                                            constCompExpTestStepObj.GasZFactor = (drCCEdata[colName].ToString() == "") ? double.NaN : double.Parse(drCCEdata[colName].ToString());
                                            break;

                                        case "GasDensity":
                                            constCompExpTestStepObj.GasDensity = new MassPerVolumeMeasure { Value = (drCCEdata[colName].ToString() == "") ? double.NaN : double.Parse(drCCEdata[colName].ToString()), Uom = "g/cm3" };
                                            break;
                                    }
                                }
                            }
                        }
                        constCompTestStepList.Add(constCompExpTestStepObj);
                        stepNumber++;                    
                    }
                    constCompExpTestObj.ConstantCompositionExpansionTestStep = constCompTestStepList;
                    constCompExpTestList.Add(constCompExpTestObj);
                    testNumber++;
                }
            }

            #endregion

            #region Export CVD

            if (dsCVD.Tables.Count > 0) // Check if CVD data is input by user
            {
                int testNumber = 1;
                int stepNumber = 1;

                foreach (DataTable dtCVDdata in dsCVD.Tables)
                {
                    List<FluidCvdTestStep> cvdTestStepList = new List<FluidCvdTestStep>();
                    stepNumber = 1;
                    ConstantVolumeDepletionTest conVolDepTestObj = new ConstantVolumeDepletionTest();
                    conVolDepTestObj.Uid = string.Format("CVDTest{0}", testNumber);
                    conVolDepTestObj.TestNumber = testNumber;
                    conVolDepTestObj.TestTemperature = new ThermodynamicTemperatureMeasure { Uom = ThermodynamicTemperatureUom.degF, Value = 220 }; // Added to fulfill required field functionality
                    conVolDepTestObj.SatuationPressure = new SaturationPressure { Kind = SaturationPointKind.bubblepoint, Uom = "psi", Value = 2620 };

                    foreach (DataRow drCVDdata in dtCVDdata.Rows)
                    {
                        FluidCvdTestStep fluidCvdTestStepObj = new FluidCvdTestStep();
                        fluidCvdTestStepObj.Uid = string.Format("step{0}", stepNumber);
                        fluidCvdTestStepObj.StepNumber = stepNumber;

                        foreach (Tuple<string, string, string, string, string, string, string, Tuple<string>> value in colMappingsList)
                        {
                            if (value.Rest.Item1 == "PVT")
                            {
                                string colName = value.Item1;
                                string uom = value.Item6;
                                string testTyp = value.Item7;

                                if ((dtCVDdata.Columns.Contains(colName)) && (testTyp == "CVD" || testTyp.StartsWith("CVD/") || testTyp.EndsWith("/CVD") || testTyp.Contains("/CVD/")))
                                {
                                    switch (colName)
                                    {
                                        //Temperature place holder missing in Energistics
 
                                        case "Pressure":
                                            fluidCvdTestStepObj.StepPressure = new PressureMeasure{ Value = (drCVDdata[colName].ToString() == "") ? double.NaN : double.Parse(drCVDdata[colName].ToString()), Uom = uom };
                                            break;

                                       //MolePercentProduced place holder missing in Energistics
                                        
                                       //LiquidVolume(%Vsat) place holder missing in Energistics

                                        case "ZFactor":
                                            fluidCvdTestStepObj.GasZFactor = (drCVDdata[colName].ToString() == "") ? double.NaN : double.Parse(drCVDdata[colName].ToString());
                                            break;

                                        case "ZFactor2":
                                            fluidCvdTestStepObj.Phase2ZFactor = (drCVDdata[colName].ToString() == "") ? double.NaN : double.Parse(drCVDdata[colName].ToString());
                                            break;

                                        case "GasGravity":
                                            fluidCvdTestStepObj.GasGravity = (drCVDdata[colName].ToString() == "") ? double.NaN : double.Parse(drCVDdata[colName].ToString());
                                            break;
                                    }
                                }
                            }
                        }
                        cvdTestStepList.Add(fluidCvdTestStepObj);
                        stepNumber++;
                    }
                    conVolDepTestObj.CvdTestStep = cvdTestStepList;
                    constVolDepTestList.Add(conVolDepTestObj);
                    testNumber++;
                }
            }

            #endregion

            #region Export Viscosity

           //There is no place holder for viscosity Test in Energistics

            #endregion

            #region Export Swelling

            if (dsSwelling.Tables.Count > 0) // Check if CVD data is input by user
            {
                int testNumber = 1;
                int stepNumber = 1;

                foreach (DataTable dtSwellingDdata in dsSwelling.Tables)
                {
                    List<SwellingTestStep> swellingTestStepList = new List<SwellingTestStep>();
                    stepNumber = 1;
                    SwellingTest swellingTestObj = new SwellingTest();
                    swellingTestObj.Uid = string.Format("SwellingTest{0}", testNumber);
                    swellingTestObj.TestNumber = testNumber;
                    swellingTestObj.TestTemperature = new ThermodynamicTemperatureMeasure { Uom = ThermodynamicTemperatureUom.degF, Value = 220 }; // Added to fulfill required field functionality

                    foreach (DataRow drSwellingdata in dtSwellingDdata.Rows)
                    {
                        SwellingTestStep swellingTestStepObj = new SwellingTestStep();
                        swellingTestStepObj.Uid = string.Format("step{0}", stepNumber);
                        swellingTestStepObj.StepNumber = stepNumber;

                        foreach (Tuple<string, string, string, string, string, string, string, Tuple<string>> value in colMappingsList)
                        {
                            if (value.Rest.Item1 == "PVT")
                            {
                                string colName = value.Item1;
                                string uom = value.Item6;
                                string testTyp = value.Item7;

                                if ((dtSwellingDdata.Columns.Contains(colName)) && (testTyp == "SWE" || testTyp.StartsWith("SWE/") || testTyp.EndsWith("/SWE") || testTyp.Contains("/SWE/")))
                                {
                                    switch (colName)
                                    {
                                        //Temperature place holder missing in Energistics

                                        //InjectionMoleRatio place holder missing in Energistics

                                        case "GOR":
                                            swellingTestStepObj.Gor = new VolumePerVolumeMeasure { Value = (drSwellingdata[colName].ToString() == "") ? double.NaN : double.Parse(drSwellingdata[colName].ToString()), Uom = uom };
                                            break;

                                        case "PSat":
                                            swellingTestStepObj.SaturationPressure = new SaturationPressure{ Value = (drSwellingdata[colName].ToString() == "") ? double.NaN : double.Parse(drSwellingdata[colName].ToString()), Uom = uom, Kind = SaturationPointKind.bubblepoint };
                                            break;

                                        case "SwellingFactor":
                                            swellingTestStepObj.SwellingFactor = new VolumePerVolumeMeasure { Value = (drSwellingdata[colName].ToString() == "") ? double.NaN : double.Parse(drSwellingdata[colName].ToString()), Uom = uom };
                                            break;

                                        case "DensityPsat":
                                            swellingTestStepObj.DensityAtSaturationPoint = new MassPerVolumeMeasure { Value = (drSwellingdata[colName].ToString() == "") ? double.NaN : double.Parse(drSwellingdata[colName].ToString()), Uom = uom };
                                            break;

                                        //ViscosityPsat place holder missing in Energistics

                                        //DensitySTO place holder missing in Energistics

                                        //LiveMW place holder missing in Energistics

                                        //STOMW place holder missing in Energistics

                                        //FlashGasGravity place holder missing in Energistics
                                    }
                                }
                            }
                        }
                        swellingTestStepList.Add(swellingTestStepObj);
                        stepNumber++;
                    }
                    swellingTestObj.SwellingTestStep = swellingTestStepList;
                    swellingTestList.Add(swellingTestObj);
                    testNumber++;
                }
            }

            #endregion

            #region Export ZeroFlash

            if (dsZeroFlash.Tables.Count > 0) // Check if Zero Flash data is input by user
            {
                int testNumber = 1;

                foreach(DataTable dtZFdata in dsZeroFlash.Tables)
                {
                    foreach(DataRow drZFdata in dtZFdata.Rows)
                    {
                        AtmosphericFlashTestAndCompositionalAnalysis atFlTestCompAnlObj = new AtmosphericFlashTestAndCompositionalAnalysis();
                        atFlTestCompAnlObj.Uid = string.Format("AtmFlashTest{0}", testNumber);
                        atFlTestCompAnlObj.TestNumber = testNumber;
                        foreach (Tuple<string, string, string, string, string, string, string, Tuple<string>> value in colMappingsList)
                        {
                            if (value.Rest.Item1 == "PVT")
                            {
                                string colName = value.Item1;
                                string uom = value.Item6;
                                string testTyp = value.Item7;

                                if ((dtZFdata.Columns.Contains(colName)) && (testTyp == "ZF" || testTyp.StartsWith("ZF/") || testTyp.EndsWith("/ZF") || testTyp.Contains("/ZF/")))
                                {
                                    switch (colName)
                                    {
                                        case "Temperature":
                                            atFlTestCompAnlObj.FlashToTemperature = new ThermodynamicTemperatureMeasure { Value = (drZFdata[colName].ToString() == "") ? double.NaN : double.Parse(drZFdata[colName].ToString()), Uom = (ThermodynamicTemperatureUom)((typeof(ThermodynamicTemperatureUom).GetField(uom)).GetValue(null)) };
                                            break;

                                        case "Pressure":
                                            atFlTestCompAnlObj.FlashToPressure = new AbsolutePressure { AbsolutePressure1 = new PressureMeasureExt { Value = (drZFdata[colName].ToString() == "") ? double.NaN : double.Parse(drZFdata[colName].ToString()), Uom = uom } };
                                            break;

                                        case "AmbTemp":
                                            atFlTestCompAnlObj.AtmosphericTemperature = new ThermodynamicTemperatureMeasure { Value = (drZFdata[colName].ToString() == "") ? double.NaN : double.Parse(drZFdata[colName].ToString()), Uom = (ThermodynamicTemperatureUom)((typeof(ThermodynamicTemperatureUom).GetField(uom)).GetValue(null)) };
                                            break;

                                        case "AmbPressure":
                                            atFlTestCompAnlObj.AtmosphericPressure = new PressureMeasure { Value = (drZFdata[colName].ToString() == "") ? double.NaN : double.Parse(drZFdata[colName].ToString()), Uom = uom };
                                            break;

                                        case "GOR":
                                            atFlTestCompAnlObj.FlashGOR = new VolumePerVolumeMeasure { Value = (drZFdata[colName].ToString() == "") ? double.NaN : double.Parse(drZFdata[colName].ToString()), Uom = uom };
                                            break;

                                        case "FVF":
                                            atFlTestCompAnlObj.OilFormationVolumeFactor = new VolumePerVolumeMeasure { Value = (drZFdata[colName].ToString() == "") ? double.NaN : double.Parse(drZFdata[colName].ToString()), Uom = uom };
                                            break;

                                        //Gas Gravity placeholder missing in energistics.

                                        case "LiquidDensity":
                                            atFlTestCompAnlObj.DensityAtSamplePressureandTemperature = new MassPerVolumeMeasure { Value = (drZFdata[colName].ToString() == "") ? double.NaN : double.Parse(drZFdata[colName].ToString()), Uom = uom };
                                            break;

                                        case "LiquidMW":
                                            atFlTestCompAnlObj.AvgMolecularWeight = new MolecularWeightMeasure { Value = (drZFdata[colName].ToString() == "") ? double.NaN : double.Parse(drZFdata[colName].ToString()), Uom = MolecularWeightUom.gmol };
                                            break;

                                        //LiqUOP. K Factor placeholder missing in energistics
                                    }
                                }
                            }
                        }
                        zeroFlashTestList.Add(atFlTestCompAnlObj);
                        testNumber++;
                    }
                }
            }

            #endregion

            #region Export Saturation

            if (dsSaturation.Tables.Count > 0) // Check if Saturation data is input by user
            {
                int testNumber = 1;

                foreach (DataTable dtSATdata in dsSaturation.Tables)
                {
                    foreach (DataRow drSATdata in dtSATdata.Rows)
                    {
                        SaturationTest satTestObj = new SaturationTest();
                        satTestObj.Uid = string.Format("SaturationTest{0}", testNumber);
                        satTestObj.TestNumber = testNumber;
                        satTestObj.Remark = string.Format("measurement no.{0}", testNumber);
                        foreach (Tuple<string, string, string, string, string, string, string, Tuple<string>> value in colMappingsList)
                        {
                            if (value.Rest.Item1 == "PVT")
                            {
                                string colName = value.Item1;
                                string uom = value.Item6;
                                string testTyp = value.Item7;

                                if ((dtSATdata.Columns.Contains(colName)) && (testTyp == "SAT" || testTyp.StartsWith("SAT/") || testTyp.EndsWith("/SAT") || testTyp.Contains("/SAT/")))
                                {
                                    switch (colName)
                                    {
                                        case "Temperature":
                                            satTestObj.TestTemperature = new ThermodynamicTemperatureMeasure { Value = (drSATdata[colName].ToString() == "") ? double.NaN : double.Parse(drSATdata[colName].ToString()), Uom = (ThermodynamicTemperatureUom)((typeof(ThermodynamicTemperatureUom).GetField(uom)).GetValue(null)) };
                                            break;

                                        case "Pressure":
                                            satTestObj.SaturationPressure = new SaturationPressure { Value = (drSATdata[colName].ToString() == "") ? double.NaN : double.Parse(drSATdata[colName].ToString()), Uom = uom, Kind = SaturationPointKind.bubblepoint };
                                            break;
                                    }
                                }
                            }
                        }
                        satTestList.Add(satTestObj);
                        testNumber++;
                    }
                }
            }

            #endregion

            #region Execute Export All

            hydroCarbonAnalysisObj.AtmosphericFlashTestAndCompositionalAnalysis = zeroFlashTestList;
            hydroCarbonAnalysisObj.SaturationTest = satTestList;
            hydroCarbonAnalysisObj.ConstantCompositionExpansionTest = constCompExpTestList;
            hydroCarbonAnalysisObj.DifferentialLiberationTest = diffLibTestList;
            hydroCarbonAnalysisObj.ConstantVolumeDepletionTest = constVolDepTestList;
            hydroCarbonAnalysisObj.SwellingTest = swellingTestList;
            hydroCarbonAnalysisObj.SchemaVersion = "2.0";
            hydroCarbonAnalysisObj.Uuid = "00000000-0000-0000-0000-000000000000";

            fluidAnalysisObj = hydroCarbonAnalysisObj;
            EnergisticsFile.WriteFile(String.Format(@"{0}\{1}_{2}.xml", fileDir.Trim(), "FluidAnalysis", DateTime.Now.Date.ToString("MM-dd-yyyy")), fluidAnalysisObj, true); //Export PRODML

            if (MessageBox.Show("Export successful, Do you want to open the directory containing files?", "PRODML File Generator", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) // To prompt that Export operation has been completed.
            {
                System.Diagnostics.Process.Start(fileDir);
            }

            #endregion

        }

        #endregion

        #region Get Column Mappings

        private void getColumnMappings() // Gets the column mappings for all the data grids from the configuration file.
        {
            GridColumnCollection gridColCollection = new GridColumnCollection();

            //Loading configuration file from appdata folder
            string baseConfigPath = GetBaseConfigPath();
            configMap.ExeConfigFilename = @baseConfigPath;
            System.Configuration.Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);

            //Get Grid Columns Collection
            string colSec = "GridConfigurations/GridColumnConfigurationSection";
            GridColumnConfigurationSection gridConfigSection = (GridColumnConfigurationSection)config.GetSection(colSec);
            gridColCollection = gridConfigSection.GridColumns;

            // Can have an active column check here so the foreach loop would traverse only selected column.
            foreach (GridColumn col in gridColCollection) //Creating columns in the grids
            {
                if (col.ReportType == "PVT")
                {
                    Tuple<string, string, string, string, string, string, string, Tuple<string>> tuple = Tuple.Create(col.ShortName, col.LongName, col.SourceName, col.DataType, col.Uom, col.EnergyUom, col.Element, col.ReportType);
                    colMappingsList.Add(tuple);
                }
            }
        }

        #endregion

        #region Datagridview Operations

        public void execRowOperation(object sender, EventArgs e) // Contains the implementation of the Copy/Copy w Headers/Cut/Paste & Delete functionality for the Context Menu Strip
        {
            try
            {
                ToolStripItem toolStripMenuItem = (sender as ToolStripItem); //Initialize the ToolStripItem object based on sender
                if (toolStripMenuItem != null)
                {
                    ContextMenuStrip contextMenuStripOwner = (toolStripMenuItem.Owner as ContextMenuStrip);
                    if (contextMenuStripOwner != null)
                    {
                        DataGridView selectedDataGrid = contextMenuStripOwner.SourceControl as DataGridView; // Gets the DataGridView control which contains this ContextMenuStrip
                        if (selectedDataGrid != null)
                        {
                            if (selectedDataGrid.SelectedCells.Count > 0)
                            {
                                switch (toolStripMenuItem.Name)
                                {
                                    case "copyToolStripMenuItem":
                                        dataGridUtil.copyToClipboard(selectedDataGrid, false);
                                        break;

                                    case "copyWithHeadersToolStripMenuItem":
                                        dataGridUtil.copyToClipboard(selectedDataGrid, true);
                                        break;

                                    case "pasteToolStripMenuItem":
                                        dataGridUtil.pasteToClipboard(selectedDataGrid);
                                        break;

                                    default:
                                        break;
                                }
                                // Can assign the contextMenu here in case there is no data in the grid. And can also have an else statement where no data is available
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("{0}", ex.Message), "PRODML File Generator", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion
    }
}
