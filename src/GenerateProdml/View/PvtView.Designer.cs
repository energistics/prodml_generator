 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

namespace GenerateProdml.View
{
    partial class PvtView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.gb_sel_test = new System.Windows.Forms.GroupBox();
            this.btn_export_pvt_rpt = new System.Windows.Forms.Button();
            this.rb_swelling = new System.Windows.Forms.RadioButton();
            this.rb_viscosity = new System.Windows.Forms.RadioButton();
            this.rb_composition = new System.Windows.Forms.RadioButton();
            this.rb_cvd = new System.Windows.Forms.RadioButton();
            this.rb_separator = new System.Windows.Forms.RadioButton();
            this.rb_difflab = new System.Windows.Forms.RadioButton();
            this.rb_cce = new System.Windows.Forms.RadioButton();
            this.rb_zeroflash = new System.Windows.Forms.RadioButton();
            this.rb_saturation = new System.Windows.Forms.RadioButton();
            this.gb_test_typ = new System.Windows.Forms.GroupBox();
            this.tab_data = new System.Windows.Forms.TabControl();
            this.tab_pg1 = new System.Windows.Forms.TabPage();
            this.dg_dataset1 = new System.Windows.Forms.DataGridView();
            this.tab_pg2 = new System.Windows.Forms.TabPage();
            this.dg_dataset2 = new System.Windows.Forms.DataGridView();
            this.tab_pg3 = new System.Windows.Forms.TabPage();
            this.dg_dataset3 = new System.Windows.Forms.DataGridView();
            this.tab_pg4 = new System.Windows.Forms.TabPage();
            this.dg_dataset4 = new System.Windows.Forms.DataGridView();
            this.tab_pg5 = new System.Windows.Forms.TabPage();
            this.dg_dataset5 = new System.Windows.Forms.DataGridView();
            this.dg_dataset0 = new System.Windows.Forms.DataGridView();
            this.contxt_mnu_strip_grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyWithHeadersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.gb_sel_test.SuspendLayout();
            this.gb_test_typ.SuspendLayout();
            this.tab_data.SuspendLayout();
            this.tab_pg1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_dataset1)).BeginInit();
            this.tab_pg2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_dataset2)).BeginInit();
            this.tab_pg3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_dataset3)).BeginInit();
            this.tab_pg4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_dataset4)).BeginInit();
            this.tab_pg5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_dataset5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_dataset0)).BeginInit();
            this.contxt_mnu_strip_grid.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gb_sel_test);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gb_test_typ);
            this.splitContainer1.Size = new System.Drawing.Size(1186, 762);
            this.splitContainer1.SplitterDistance = 171;
            this.splitContainer1.TabIndex = 0;
            // 
            // gb_sel_test
            // 
            this.gb_sel_test.Controls.Add(this.btn_export_pvt_rpt);
            this.gb_sel_test.Controls.Add(this.rb_swelling);
            this.gb_sel_test.Controls.Add(this.rb_viscosity);
            this.gb_sel_test.Controls.Add(this.rb_composition);
            this.gb_sel_test.Controls.Add(this.rb_cvd);
            this.gb_sel_test.Controls.Add(this.rb_separator);
            this.gb_sel_test.Controls.Add(this.rb_difflab);
            this.gb_sel_test.Controls.Add(this.rb_cce);
            this.gb_sel_test.Controls.Add(this.rb_zeroflash);
            this.gb_sel_test.Controls.Add(this.rb_saturation);
            this.gb_sel_test.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb_sel_test.Location = new System.Drawing.Point(0, 0);
            this.gb_sel_test.Name = "gb_sel_test";
            this.gb_sel_test.Size = new System.Drawing.Size(171, 762);
            this.gb_sel_test.TabIndex = 8;
            this.gb_sel_test.TabStop = false;
            this.gb_sel_test.Text = "Test Types";
            // 
            // btn_export_pvt_rpt
            // 
            this.btn_export_pvt_rpt.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_export_pvt_rpt.Location = new System.Drawing.Point(3, 725);
            this.btn_export_pvt_rpt.Name = "btn_export_pvt_rpt";
            this.btn_export_pvt_rpt.Size = new System.Drawing.Size(165, 34);
            this.btn_export_pvt_rpt.TabIndex = 17;
            this.btn_export_pvt_rpt.Text = "Create Report(PRODML)";
            this.btn_export_pvt_rpt.UseVisualStyleBackColor = true;
            this.btn_export_pvt_rpt.Click += new System.EventHandler(this.btn_export_pvt_rpt_Click);
            // 
            // rb_swelling
            // 
            this.rb_swelling.AutoSize = true;
            this.rb_swelling.Location = new System.Drawing.Point(38, 646);
            this.rb_swelling.Name = "rb_swelling";
            this.rb_swelling.Size = new System.Drawing.Size(64, 17);
            this.rb_swelling.TabIndex = 16;
            this.rb_swelling.TabStop = true;
            this.rb_swelling.Text = "Swelling";
            this.rb_swelling.UseVisualStyleBackColor = true;
            this.rb_swelling.CheckedChanged += new System.EventHandler(this.rb_swelling_CheckedChanged);
            // 
            // rb_viscosity
            // 
            this.rb_viscosity.AutoSize = true;
            this.rb_viscosity.Location = new System.Drawing.Point(38, 570);
            this.rb_viscosity.Name = "rb_viscosity";
            this.rb_viscosity.Size = new System.Drawing.Size(66, 17);
            this.rb_viscosity.TabIndex = 15;
            this.rb_viscosity.TabStop = true;
            this.rb_viscosity.Text = "Viscosity";
            this.rb_viscosity.UseVisualStyleBackColor = true;
            this.rb_viscosity.CheckedChanged += new System.EventHandler(this.rb_viscosity_CheckedChanged);
            // 
            // rb_composition
            // 
            this.rb_composition.AutoSize = true;
            this.rb_composition.Location = new System.Drawing.Point(38, 114);
            this.rb_composition.Name = "rb_composition";
            this.rb_composition.Size = new System.Drawing.Size(82, 17);
            this.rb_composition.TabIndex = 14;
            this.rb_composition.TabStop = true;
            this.rb_composition.Text = "Composition";
            this.rb_composition.UseVisualStyleBackColor = true;
            this.rb_composition.CheckedChanged += new System.EventHandler(this.rb_composition_CheckedChanged);
            // 
            // rb_cvd
            // 
            this.rb_cvd.AutoSize = true;
            this.rb_cvd.Location = new System.Drawing.Point(38, 418);
            this.rb_cvd.Name = "rb_cvd";
            this.rb_cvd.Size = new System.Drawing.Size(47, 17);
            this.rb_cvd.TabIndex = 13;
            this.rb_cvd.TabStop = true;
            this.rb_cvd.Text = "CVD";
            this.rb_cvd.UseVisualStyleBackColor = true;
            this.rb_cvd.CheckedChanged += new System.EventHandler(this.rb_cvd_CheckedChanged);
            // 
            // rb_separator
            // 
            this.rb_separator.AutoSize = true;
            this.rb_separator.Location = new System.Drawing.Point(38, 494);
            this.rb_separator.Name = "rb_separator";
            this.rb_separator.Size = new System.Drawing.Size(71, 17);
            this.rb_separator.TabIndex = 12;
            this.rb_separator.TabStop = true;
            this.rb_separator.Text = "Separator";
            this.rb_separator.UseVisualStyleBackColor = true;
            this.rb_separator.CheckedChanged += new System.EventHandler(this.rb_separator_CheckedChanged);
            // 
            // rb_difflab
            // 
            this.rb_difflab.AutoSize = true;
            this.rb_difflab.Location = new System.Drawing.Point(38, 342);
            this.rb_difflab.Name = "rb_difflab";
            this.rb_difflab.Size = new System.Drawing.Size(39, 17);
            this.rb_difflab.TabIndex = 11;
            this.rb_difflab.TabStop = true;
            this.rb_difflab.Text = "DL";
            this.rb_difflab.UseVisualStyleBackColor = true;
            this.rb_difflab.CheckedChanged += new System.EventHandler(this.rb_difflab_CheckedChanged);
            // 
            // rb_cce
            // 
            this.rb_cce.AutoSize = true;
            this.rb_cce.Location = new System.Drawing.Point(38, 266);
            this.rb_cce.Name = "rb_cce";
            this.rb_cce.Size = new System.Drawing.Size(46, 17);
            this.rb_cce.TabIndex = 10;
            this.rb_cce.TabStop = true;
            this.rb_cce.Text = "CCE";
            this.rb_cce.UseVisualStyleBackColor = true;
            this.rb_cce.CheckedChanged += new System.EventHandler(this.rb_cce_CheckedChanged);
            // 
            // rb_zeroflash
            // 
            this.rb_zeroflash.AutoSize = true;
            this.rb_zeroflash.Location = new System.Drawing.Point(38, 38);
            this.rb_zeroflash.Name = "rb_zeroflash";
            this.rb_zeroflash.Size = new System.Drawing.Size(75, 17);
            this.rb_zeroflash.TabIndex = 9;
            this.rb_zeroflash.TabStop = true;
            this.rb_zeroflash.Text = "Zero Flash";
            this.rb_zeroflash.UseVisualStyleBackColor = true;
            this.rb_zeroflash.CheckedChanged += new System.EventHandler(this.rb_zeroflash_CheckedChanged);
            // 
            // rb_saturation
            // 
            this.rb_saturation.AutoSize = true;
            this.rb_saturation.Location = new System.Drawing.Point(38, 190);
            this.rb_saturation.Name = "rb_saturation";
            this.rb_saturation.Size = new System.Drawing.Size(73, 17);
            this.rb_saturation.TabIndex = 8;
            this.rb_saturation.TabStop = true;
            this.rb_saturation.Text = "Saturation";
            this.rb_saturation.UseVisualStyleBackColor = true;
            this.rb_saturation.CheckedChanged += new System.EventHandler(this.rb_saturation_CheckedChanged);
            // 
            // gb_test_typ
            // 
            this.gb_test_typ.Controls.Add(this.dg_dataset0);
            this.gb_test_typ.Controls.Add(this.tab_data);
            this.gb_test_typ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb_test_typ.Location = new System.Drawing.Point(0, 0);
            this.gb_test_typ.Name = "gb_test_typ";
            this.gb_test_typ.Size = new System.Drawing.Size(1011, 762);
            this.gb_test_typ.TabIndex = 0;
            this.gb_test_typ.TabStop = false;
            // 
            // tab_data
            // 
            this.tab_data.Controls.Add(this.tab_pg1);
            this.tab_data.Controls.Add(this.tab_pg2);
            this.tab_data.Controls.Add(this.tab_pg3);
            this.tab_data.Controls.Add(this.tab_pg4);
            this.tab_data.Controls.Add(this.tab_pg5);
            this.tab_data.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tab_data.Location = new System.Drawing.Point(3, 16);
            this.tab_data.Name = "tab_data";
            this.tab_data.SelectedIndex = 0;
            this.tab_data.Size = new System.Drawing.Size(1005, 743);
            this.tab_data.TabIndex = 0;
            this.tab_data.Visible = false;
            // 
            // tab_pg1
            // 
            this.tab_pg1.Controls.Add(this.dg_dataset1);
            this.tab_pg1.Location = new System.Drawing.Point(4, 22);
            this.tab_pg1.Name = "tab_pg1";
            this.tab_pg1.Padding = new System.Windows.Forms.Padding(3);
            this.tab_pg1.Size = new System.Drawing.Size(997, 717);
            this.tab_pg1.TabIndex = 0;
            this.tab_pg1.Text = "Set1";
            this.tab_pg1.UseVisualStyleBackColor = true;
            // 
            // dg_dataset1
            // 
            this.dg_dataset1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_dataset1.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dg_dataset1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_dataset1.ContextMenuStrip = this.contxt_mnu_strip_grid;
            this.dg_dataset1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_dataset1.Location = new System.Drawing.Point(3, 3);
            this.dg_dataset1.Name = "dg_dataset1";
            this.dg_dataset1.Size = new System.Drawing.Size(991, 711);
            this.dg_dataset1.TabIndex = 0;
            this.dg_dataset1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvDataSet1_DataError);
            // 
            // tab_pg2
            // 
            this.tab_pg2.Controls.Add(this.dg_dataset2);
            this.tab_pg2.Location = new System.Drawing.Point(4, 22);
            this.tab_pg2.Name = "tab_pg2";
            this.tab_pg2.Padding = new System.Windows.Forms.Padding(3);
            this.tab_pg2.Size = new System.Drawing.Size(997, 717);
            this.tab_pg2.TabIndex = 1;
            this.tab_pg2.Text = "Set2";
            this.tab_pg2.UseVisualStyleBackColor = true;
            // 
            // dg_dataset2
            // 
            this.dg_dataset2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_dataset2.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dg_dataset2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_dataset2.ContextMenuStrip = this.contxt_mnu_strip_grid;
            this.dg_dataset2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_dataset2.Location = new System.Drawing.Point(3, 3);
            this.dg_dataset2.Name = "dg_dataset2";
            this.dg_dataset2.Size = new System.Drawing.Size(991, 711);
            this.dg_dataset2.TabIndex = 0;
            this.dg_dataset2.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvDataSet2_DataError);
            // 
            // tab_pg3
            // 
            this.tab_pg3.Controls.Add(this.dg_dataset3);
            this.tab_pg3.Location = new System.Drawing.Point(4, 22);
            this.tab_pg3.Name = "tab_pg3";
            this.tab_pg3.Padding = new System.Windows.Forms.Padding(3);
            this.tab_pg3.Size = new System.Drawing.Size(997, 717);
            this.tab_pg3.TabIndex = 2;
            this.tab_pg3.Text = "Set3";
            this.tab_pg3.UseVisualStyleBackColor = true;
            // 
            // dg_dataset3
            // 
            this.dg_dataset3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_dataset3.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dg_dataset3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_dataset3.ContextMenuStrip = this.contxt_mnu_strip_grid;
            this.dg_dataset3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_dataset3.Location = new System.Drawing.Point(3, 3);
            this.dg_dataset3.Name = "dg_dataset3";
            this.dg_dataset3.Size = new System.Drawing.Size(991, 711);
            this.dg_dataset3.TabIndex = 0;
            this.dg_dataset3.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvDataSet3_DataError);
            // 
            // tab_pg4
            // 
            this.tab_pg4.Controls.Add(this.dg_dataset4);
            this.tab_pg4.Location = new System.Drawing.Point(4, 22);
            this.tab_pg4.Name = "tab_pg4";
            this.tab_pg4.Padding = new System.Windows.Forms.Padding(3);
            this.tab_pg4.Size = new System.Drawing.Size(997, 717);
            this.tab_pg4.TabIndex = 3;
            this.tab_pg4.Text = "Set4";
            this.tab_pg4.UseVisualStyleBackColor = true;
            // 
            // dg_dataset4
            // 
            this.dg_dataset4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_dataset4.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dg_dataset4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_dataset4.ContextMenuStrip = this.contxt_mnu_strip_grid;
            this.dg_dataset4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_dataset4.Location = new System.Drawing.Point(3, 3);
            this.dg_dataset4.Name = "dg_dataset4";
            this.dg_dataset4.Size = new System.Drawing.Size(991, 711);
            this.dg_dataset4.TabIndex = 0;
            this.dg_dataset4.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvDataSet4_DataError);
            // 
            // tab_pg5
            // 
            this.tab_pg5.Controls.Add(this.dg_dataset5);
            this.tab_pg5.Location = new System.Drawing.Point(4, 22);
            this.tab_pg5.Name = "tab_pg5";
            this.tab_pg5.Padding = new System.Windows.Forms.Padding(3);
            this.tab_pg5.Size = new System.Drawing.Size(997, 717);
            this.tab_pg5.TabIndex = 4;
            this.tab_pg5.Text = "Set5";
            this.tab_pg5.UseVisualStyleBackColor = true;
            // 
            // dg_dataset5
            // 
            this.dg_dataset5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_dataset5.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dg_dataset5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_dataset5.ContextMenuStrip = this.contxt_mnu_strip_grid;
            this.dg_dataset5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_dataset5.Location = new System.Drawing.Point(3, 3);
            this.dg_dataset5.Name = "dg_dataset5";
            this.dg_dataset5.Size = new System.Drawing.Size(991, 711);
            this.dg_dataset5.TabIndex = 0;
            this.dg_dataset5.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvDataSet5_DataError);
            // 
            // dg_dataset0
            // 
            this.dg_dataset0.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_dataset0.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dg_dataset0.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_dataset0.ContextMenuStrip = this.contxt_mnu_strip_grid;
            this.dg_dataset0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_dataset0.Location = new System.Drawing.Point(3, 16);
            this.dg_dataset0.Name = "dg_dataset0";
            this.dg_dataset0.Size = new System.Drawing.Size(1005, 743);
            this.dg_dataset0.TabIndex = 1;
            this.dg_dataset0.Visible = false;
            this.dg_dataset0.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvDataSet0_DataError);
            // 
            // contxt_mnu_strip_grid
            // 
            this.contxt_mnu_strip_grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.copyWithHeadersToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.contxt_mnu_strip_grid.Name = "contxt_mnu_strip_grid";
            this.contxt_mnu_strip_grid.Size = new System.Drawing.Size(218, 114);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Enabled = false;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.cutToolStripMenuItem.Text = "Cut";
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // copyWithHeadersToolStripMenuItem
            // 
            this.copyWithHeadersToolStripMenuItem.Name = "copyWithHeadersToolStripMenuItem";
            this.copyWithHeadersToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.copyWithHeadersToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.copyWithHeadersToolStripMenuItem.Text = "Copy with Headers";
            this.copyWithHeadersToolStripMenuItem.Click += new System.EventHandler(this.copyWithHeadersToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Enabled = false;
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            // 
            // PvtView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1186, 762);
            this.Controls.Add(this.splitContainer1);
            this.Name = "PvtView";
            this.Text = "PvtView";
            this.Load += new System.EventHandler(this.PvtView_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.gb_sel_test.ResumeLayout(false);
            this.gb_sel_test.PerformLayout();
            this.gb_test_typ.ResumeLayout(false);
            this.tab_data.ResumeLayout(false);
            this.tab_pg1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_dataset1)).EndInit();
            this.tab_pg2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_dataset2)).EndInit();
            this.tab_pg3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_dataset3)).EndInit();
            this.tab_pg4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_dataset4)).EndInit();
            this.tab_pg5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_dataset5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dg_dataset0)).EndInit();
            this.contxt_mnu_strip_grid.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox gb_sel_test;
        private System.Windows.Forms.RadioButton rb_composition;
        private System.Windows.Forms.RadioButton rb_cvd;
        private System.Windows.Forms.RadioButton rb_separator;
        private System.Windows.Forms.RadioButton rb_difflab;
        private System.Windows.Forms.RadioButton rb_cce;
        private System.Windows.Forms.RadioButton rb_zeroflash;
        private System.Windows.Forms.RadioButton rb_saturation;
        private System.Windows.Forms.RadioButton rb_swelling;
        private System.Windows.Forms.RadioButton rb_viscosity;
        private System.Windows.Forms.Button btn_export_pvt_rpt;
        private System.Windows.Forms.GroupBox gb_test_typ;
        private System.Windows.Forms.TabControl tab_data;
        private System.Windows.Forms.TabPage tab_pg1;
        private System.Windows.Forms.DataGridView dg_dataset1;
        private System.Windows.Forms.TabPage tab_pg2;
        private System.Windows.Forms.TabPage tab_pg3;
        private System.Windows.Forms.TabPage tab_pg4;
        private System.Windows.Forms.TabPage tab_pg5;
        private System.Windows.Forms.DataGridView dg_dataset2;
        private System.Windows.Forms.DataGridView dg_dataset3;
        private System.Windows.Forms.DataGridView dg_dataset4;
        private System.Windows.Forms.DataGridView dg_dataset5;
        private System.Windows.Forms.DataGridView dg_dataset0;
        private System.Windows.Forms.ContextMenuStrip contxt_mnu_strip_grid;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyWithHeadersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
    }
}