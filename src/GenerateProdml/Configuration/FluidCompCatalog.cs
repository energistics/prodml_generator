 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace GenerateProdml.Configuration
{
    class FluidCompCatalog : ConfigurationElement
    {
        #region Fields

        private static ConfigurationPropertyCollection _properties;
        private static ConfigurationProperty _propUid;
        private static ConfigurationProperty _propUom;
        private static ConfigurationProperty _propEnergisticsUom;
        private static ConfigurationProperty _propRepEntityVols;
        private static ConfigurationProperty _propDispositionKind;
        private static ConfigurationProperty _propProdFluidKind;
        private static ConfigurationProperty _propXmlElement;
        

        #endregion

        #region Constructor

        static FluidCompCatalog()
        {
            _propUid = new ConfigurationProperty(
                "uid",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);

            _propUom = new ConfigurationProperty(
                "uom",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);

            _propEnergisticsUom = new ConfigurationProperty(
                "energyuom",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);

            _propRepEntityVols = new ConfigurationProperty(
                "repentityvols",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);

            _propDispositionKind = new ConfigurationProperty(
                "dispkind",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);

            _propProdFluidKind = new ConfigurationProperty(
                 "prdfluidkind",
                 typeof(string),
                 null,
                 ConfigurationPropertyOptions.IsRequired);

            _propXmlElement = new ConfigurationProperty(
                "xmlelement",
                typeof(string),
                null,
                ConfigurationPropertyOptions.IsRequired);
            

            _properties = new ConfigurationPropertyCollection();
            _properties.Add(_propUid);
            _properties.Add(_propUom);
            _properties.Add(_propEnergisticsUom);
            _properties.Add(_propRepEntityVols);
            _properties.Add(_propDispositionKind);
            _properties.Add(_propProdFluidKind);
            _properties.Add(_propXmlElement);
        }

        #endregion

        #region Properties

        public string Uid
        {
            get { return (string)base[_propUid]; }
            set { base[_propUid] = value; }
        }

        public string Uom
        {
            get { return (string)base[_propUom]; }
            set { base[_propUom] = value; }
        }

        public string EnergisticsUom
        {
            get { return (string)base[_propEnergisticsUom]; }
            set { base[_propEnergisticsUom] = value; }
        }

        public string RepEntityVols
        {
            get { return (string)base[_propRepEntityVols]; }
            set { base[_propRepEntityVols] = value; }
        }

        public string DispositionKind
        {
            get { return (string)base[_propDispositionKind]; }
            set { base[_propDispositionKind] = value; }
        }

        public string ProdFluidKind
        {
            get { return (string)base[_propProdFluidKind]; }
            set { base[_propProdFluidKind] = value; }
        }

        public string XmlElement
        {
            get { return (string)base[_propXmlElement]; }
            set { base[_propXmlElement] = value; }
        }


        #endregion

        #region Overrides

        protected override ConfigurationPropertyCollection Properties
        {
            get
            {
                return _properties;
            }
        }

        #endregion
    }
}
