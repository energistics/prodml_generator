 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

namespace GenerateProdml.View
{
    public interface IProdmlView
    {
        #region Event Handlers

        event EventHandler cutMenuEventHandler;
        event EventHandler copyMenuEventHandler;
        event EventHandler copyWithHeadersMenuEventHandler;
        event EventHandler deleteMenuEventHandler;
        event EventHandler pasteMenuEventHandler;
        event EventHandler sourceTypeEventHandler;
        event EventHandler dataGridViewModeEventHandler;
        event DataGridViewDataErrorEventHandler dataErrorEventHandler;

        #endregion

        #region DataGridView Maps

        DataGridView dgvEquipRelibltyData { get; set; }
        DataGridView dgvProdInfoData { get; set; }
        DataGridView dgvWellTestData { get; set; }

        #endregion

        #region Source Name Maps

        List<string> srcName { set; }
        string selectedSrcName { get; }

        #endregion

        #region Label Maps

        string equipDowntimeLabel { get; }
        string prodLabel { get; }
        string validWellTestLabel { get; }

        #endregion

        #region LogIn Maps

        bool   isLoginPanel      { get; set; }
        bool   isLoginMessage    { get; set; }
        bool   isTreeEnabled     { get; set; }
        bool   isDgvPanelEnabled { get; set; }
        bool   isCitationEnabled { get; set; }
        bool   isRprtDteEnabled  { get; set; }
        string loginUserName     { get; set; }
        string loginPassword     { get; set; }

        #endregion

        #region Other Maps

        string reportDate { get; set; }
        string citationDesc { get; set; }
        bool isExportHierarchyEntity { get; set; }
        bool isGetDataEnabled { get; set; }
        bool isExportProdmlEnabled { get; set; }
        bool isImportProdmlEnabled { get; set; }
        bool isExpHierarchyEntityEnabled { get; set; }
        bool isSourceDbEnabled { get; set; }
        bool isSourceFilesEnabled { get; set; }
        bool isSourceDb { get; set; }
        bool isSourceFiles { get; set; }
        TreeNode hierarchyTopNode { get; set; }

        #endregion
    }
}
