 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;

namespace GenerateProdml.Utility
{
    class GridUtils
    {
        public void copyToClipboard(DataGridView dataGrid, bool isHeaderInclusive) // To copy the data from the Clipboard
        {
            if (isHeaderInclusive) // For Copy With Headers
            {
                dataGrid.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            }
            else // For Copy Without Headers
            {
                dataGrid.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            }

            DataObject dataObject = dataGrid.GetClipboardContent();

            if (dataObject != null)
            {
                Clipboard.SetDataObject(dataObject);
            }
        }

        public void cutToClipboard(DataGridView dataGrid) // To cut the data from the Clipboard
        {
            copyToClipboard(dataGrid, false);

            foreach (DataGridViewCell dataGridCell in dataGrid.SelectedCells)
            {
                dataGridCell.Value = string.Empty; // Might have to enter a method to delete rows after cut
            }
        }

        //public void pasteToClipboard(DataGridView dataGrid) // Old Paste Code
        //{
        //    bool chkPasteToSelectedCells = true;
        //    //Show Error if no cell is selected
        //    if (dataGrid.SelectedCells.Count == 0)
        //    {
        //        MessageBox.Show("Select a cell to paste", "Paste",
        //        MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //        return;
        //    }

        //    //Get the starting Cell
        //    DataGridViewCell startCell = GetStartCell(dataGrid);
        //    //Get the clipboard value in a dictionary
        //    Dictionary<int, Dictionary<int, string>> cbValue =
        //            ClipBoardValues(Clipboard.GetText());

        //    int iRowIndex = startCell.RowIndex;
        //    foreach (int rowKey in cbValue.Keys)
        //    {
        //        int iColIndex = startCell.ColumnIndex;
        //        foreach (int cellKey in cbValue[rowKey].Keys)
        //        {
        //            //Check if the index is within the limit
        //            if (iColIndex <= dataGrid.Columns.Count - 1
        //            && iRowIndex <= dataGrid.Rows.Count - 1)
        //            {
        //                DataGridViewCell cell = dataGrid[iColIndex, iRowIndex];

        //                //Copy to selected cells if 'chkPasteToSelectedCells' is checked
        //                if ((chkPasteToSelectedCells && cell.Selected) ||
        //                    (!chkPasteToSelectedCells))
        //                    cell.Value = cbValue[rowKey][cellKey];
        //            }
        //            iColIndex++;
        //        }
        //        iRowIndex++;
        //    }
        //}

        public void pasteToClipboard(DataGridView datagrid) // New Paste Code
        {
            DataTable tbl = ((DataTable)datagrid.DataSource).Copy();
            List<string> data = new List<string>(Clipboard.GetText().Split('\n'));
            int colIndex = 0;
            int rowIndex = 0;
            int rowDataIndex = 0;

            if (data.Count > 0 && string.IsNullOrWhiteSpace(data[data.Count - 1]))
            {
                data.RemoveAt(data.Count - 1);
            }

            DataGridViewCell startCell = GetStartCell(datagrid);
            colIndex = startCell.ColumnIndex;
            rowIndex = startCell.RowIndex;

            foreach (string iterationRow in data)
            {
                rowDataIndex = 0;
                string row = iterationRow;
                if (row.EndsWith("\r"))
                {
                    row = row.Substring(0, row.Length - "\r".Length);
                }

                string[] rowData = row.Split(new char[] { '\r', '\x09' });
                DataRow newRow = tbl.NewRow();
 
                for (int i = colIndex; i < tbl.Columns.Count; i++)
                {
                    if(rowDataIndex == rowData.Length)
                    {
                        break;
                    }

                    if (rowData.Length > tbl.Columns.Count)
                    {
                        break;
                    }

                    if (tbl.Rows.Count > rowIndex)
                    {
                        tbl.Rows[rowIndex][i] = rowData[rowDataIndex];
                    }
                    else
                    {
                        newRow[i] = rowData[rowDataIndex];
                    }
                    rowDataIndex++;
                }

                if (tbl.Rows.Count <= rowIndex)
                {
                    tbl.Rows.Add(newRow);
                }
                rowIndex++;
            }
            datagrid.DataSource = null;
            datagrid.DataSource = tbl;
        }

        public DataTable deleteFromClipboard(DataGridView dataGrid) // Delete Selected rows // Requires changing of code
        {
            List<int> rowsToDelete = new List<int>();

            foreach (DataGridViewCell cell in dataGrid.SelectedCells)
            {
                if (rowsToDelete.Contains(cell.RowIndex) == false)
                    rowsToDelete.Add(cell.RowIndex);
            }

            rowsToDelete = rowsToDelete.OrderByDescending(rowIndex => rowIndex).ToList();
            foreach (Int32 rowIndex in rowsToDelete)
            {
                dataGrid.Rows.RemoveAt(rowIndex); //Exception occurred while last blank row was removed, have to add the AddRow feature for the grid in case of the last row
            }

            rowsToDelete.Clear();
            return (DataTable)dataGrid.DataSource;
        }

        private DataGridViewCell GetStartCell(DataGridView dgView) // Requires changing of code
        {
            //get the smallest row,column index
            if (dgView.SelectedCells.Count == 0)
                return null;

            int rowIndex = dgView.Rows.Count - 1;
            int colIndex = dgView.Columns.Count - 1;

            foreach (DataGridViewCell dgvCell in dgView.SelectedCells)
            {
                if (dgvCell.RowIndex < rowIndex)
                    rowIndex = dgvCell.RowIndex;
                if (dgvCell.ColumnIndex < colIndex)
                    colIndex = dgvCell.ColumnIndex;
            }

            return dgView[colIndex, rowIndex];
        }

        private Dictionary<int, Dictionary<int, string>> ClipBoardValues(string clipboardValue) // Requires changing of code
        {
            Dictionary<int, Dictionary<int, string>>
            copyValues = new Dictionary<int, Dictionary<int, string>>();

            String[] lines = clipboardValue.Split('\n');

            for (int i = 0; i <= lines.Length - 1; i++)
            {
                copyValues[i] = new Dictionary<int, string>();
                String[] lineContent = lines[i].Split('\t');

                //if an empty cell value copied, then set the dictionary with an empty string
                //else Set value to dictionary
                if (lineContent.Length == 0)
                    copyValues[i][0] = string.Empty;
                else
                {
                    for (int j = 0; j <= lineContent.Length - 1; j++)
                        copyValues[i][j] = lineContent[j];
                }
            }
            return copyValues;
        }
    }
}
