 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace GenerateProdml.Configuration
{
    public class UserConfig : ConfigurationSection
    {
        private UserConfig() { }

        [ConfigurationProperty("reportdate", DefaultValue = "")]
        public string ReportDate
        {
            get { return this["reportdate"].ToString(); }
            set { this["reportdate"] = value; }
        }

        [ConfigurationProperty("citationdesc", DefaultValue = "")]
        public string CitationDesc
        {
            get { return this["citationdesc"].ToString(); }
            set { this["citationdesc"] = value; }
        }

        [ConfigurationProperty("isexphierarchy", DefaultValue = "N")]
        public string IsExportHierarchy
        {
            get { return this["isexphierarchy"].ToString(); }
            set { this["isexphierarchy"] = value; }
        }

        [ConfigurationProperty("exportpath", DefaultValue = "")]
        public string ExportPath
        {
            get { return this["exportpath"].ToString(); }
            set { this["exportpath"] = value; }
        }

        [ConfigurationProperty("importpath", DefaultValue = "")]
        public string ImportPath
        {
            get { return this["importpath"].ToString(); }
            set { this["importpath"] = value; }
        }

        [ConfigurationProperty("alertdays", DefaultValue = 30)]
        public int AlertDays
        {
            get { return (int)this["alertdays"]; }
            set { this["alertdays"] = value; }
        }
    }
}
