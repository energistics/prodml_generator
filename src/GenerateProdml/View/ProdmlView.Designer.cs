 //
 //License notice
 //  
 // Prodml Generator, version 1.0
 // Copyright 2018 Chevron
 // 
 // Third Party Software
 // 
 // Energistics 
 // The following Energistics (c) products were used in the creation of this work: 
 // 
 // •             PRODML Data Schema Specifications, Version 1.3 
 // •             PRODML Data Schema Specifications, Version 2.0
 // •             PRODML Web Service Specifications, Version 2.1.0.1
 // 
 // All rights in the WITSML™ Standard, the PRODML™ Standard, and the RESQML™ Standard, or
 // any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 // subject to the terms of the Product License Agreement available at 
 // http://www.energistics.org/product-license-agreement. 
 // 
 // Apache
 // Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 // except in compliance with the License. 
 // 
 // You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 // 
 // Unless required by applicable law or agreed to in writing, software distributed under the
 // License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 // either express or implied. 
 // 
 // See the License for the specific language governing permissions and limitations under the
 // License.
 // 
 // All rights reserved. 
 // 
 //

namespace GenerateProdml
{
    partial class ProdmlView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contxt_mnu_strip_grid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyWithHeadersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbl_report_dte = new System.Windows.Forms.Label();
            this.dtp_report_dte = new System.Windows.Forms.DateTimePicker();
            this.lbl_citation_desc = new System.Windows.Forms.Label();
            this.tb_citation_description = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cb_data_src = new System.Windows.Forms.ComboBox();
            this.tble_layout_panel = new System.Windows.Forms.TableLayoutPanel();
            this.pnl_login_info = new System.Windows.Forms.Panel();
            this.lbl_login_msg = new System.Windows.Forms.Label();
            this.btn_login = new System.Windows.Forms.Button();
            this.lbl_password = new System.Windows.Forms.Label();
            this.lbl_user_nme = new System.Windows.Forms.Label();
            this.tb_user_nme = new System.Windows.Forms.TextBox();
            this.tb_password = new System.Windows.Forms.TextBox();
            this.gb_equip_downtime = new System.Windows.Forms.GroupBox();
            this.dg_equip_reliblty = new System.Windows.Forms.DataGridView();
            this.gb_production = new System.Windows.Forms.GroupBox();
            this.dg_prod_info = new System.Windows.Forms.DataGridView();
            this.gb_valid_well_test = new System.Windows.Forms.GroupBox();
            this.dg_well_test = new System.Windows.Forms.DataGridView();
            this.panel_buttons = new System.Windows.Forms.TableLayoutPanel();
            this.btn_exp_prodml = new System.Windows.Forms.Button();
            this.btn_imp_prodml = new System.Windows.Forms.Button();
            this.btn_download_data = new System.Windows.Forms.Button();
            this.btn_exec_qry = new System.Windows.Forms.Button();
            this.cbx_export_rep_xmls = new System.Windows.Forms.CheckBox();
            this.gb_tree = new System.Windows.Forms.GroupBox();
            this.tv_hierarchy = new System.Windows.Forms.TreeView();
            this.rb_src_db = new System.Windows.Forms.RadioButton();
            this.rb_src_files = new System.Windows.Forms.RadioButton();
            this.toolTip_btn = new System.Windows.Forms.ToolTip(this.components);
            this.cbx_user_entry = new System.Windows.Forms.CheckBox();
            this.tlp_Top = new System.Windows.Forms.TableLayoutPanel();
            this.tlp_Main = new System.Windows.Forms.TableLayoutPanel();
            this.contxt_mnu_strip_grid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tble_layout_panel.SuspendLayout();
            this.pnl_login_info.SuspendLayout();
            this.gb_equip_downtime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_equip_reliblty)).BeginInit();
            this.gb_production.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_prod_info)).BeginInit();
            this.gb_valid_well_test.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_well_test)).BeginInit();
            this.panel_buttons.SuspendLayout();
            this.gb_tree.SuspendLayout();
            this.tlp_Top.SuspendLayout();
            this.tlp_Main.SuspendLayout();
            this.SuspendLayout();
            // 
            // contxt_mnu_strip_grid
            // 
            this.contxt_mnu_strip_grid.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contxt_mnu_strip_grid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.copyWithHeadersToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.contxt_mnu_strip_grid.Name = "contxt_mnu_strip_grid";
            this.contxt_mnu_strip_grid.Size = new System.Drawing.Size(218, 114);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Enabled = false;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.cutToolStripMenuItem.Text = "Cut";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // copyWithHeadersToolStripMenuItem
            // 
            this.copyWithHeadersToolStripMenuItem.Name = "copyWithHeadersToolStripMenuItem";
            this.copyWithHeadersToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.copyWithHeadersToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.copyWithHeadersToolStripMenuItem.Text = "Copy with Headers";
            this.copyWithHeadersToolStripMenuItem.Click += new System.EventHandler(this.copyWithHeadersToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Enabled = false;
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // lbl_report_dte
            // 
            this.lbl_report_dte.AutoSize = true;
            this.lbl_report_dte.Location = new System.Drawing.Point(365, 0);
            this.lbl_report_dte.Name = "lbl_report_dte";
            this.lbl_report_dte.Size = new System.Drawing.Size(65, 13);
            this.lbl_report_dte.TabIndex = 63;
            this.lbl_report_dte.Text = "Report Date";
            // 
            // dtp_report_dte
            // 
            this.dtp_report_dte.CustomFormat = "MM/dd/yyyy";
            this.dtp_report_dte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtp_report_dte.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_report_dte.Location = new System.Drawing.Point(365, 25);
            this.dtp_report_dte.Name = "dtp_report_dte";
            this.dtp_report_dte.Size = new System.Drawing.Size(253, 20);
            this.dtp_report_dte.TabIndex = 61;
            this.dtp_report_dte.ValueChanged += new System.EventHandler(this.dtp_report_dte_ValueChanged);
            // 
            // lbl_citation_desc
            // 
            this.lbl_citation_desc.AutoSize = true;
            this.lbl_citation_desc.Location = new System.Drawing.Point(624, 0);
            this.lbl_citation_desc.Name = "lbl_citation_desc";
            this.lbl_citation_desc.Size = new System.Drawing.Size(98, 13);
            this.lbl_citation_desc.TabIndex = 69;
            this.lbl_citation_desc.Text = "Citation Description";
            // 
            // tb_citation_description
            // 
            this.tb_citation_description.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_citation_description.Location = new System.Drawing.Point(624, 25);
            this.tb_citation_description.MaxLength = 100;
            this.tb_citation_description.Name = "tb_citation_description";
            this.tb_citation_description.ShortcutsEnabled = false;
            this.tb_citation_description.Size = new System.Drawing.Size(408, 20);
            this.tb_citation_description.TabIndex = 68;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox1.Image = global::GenerateProdml.Properties.Resources.chevron;
            this.pictureBox1.ImageLocation = "";
            this.pictureBox1.InitialImage = global::GenerateProdml.Properties.Resources.chevron;
            this.pictureBox1.Location = new System.Drawing.Point(1153, 3);
            this.pictureBox1.MaximumSize = new System.Drawing.Size(24, 26);
            this.pictureBox1.MinimumSize = new System.Drawing.Size(24, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 66;
            this.pictureBox1.TabStop = false;
            // 
            // cb_data_src
            // 
            this.cb_data_src.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cb_data_src.FormattingEnabled = true;
            this.cb_data_src.Location = new System.Drawing.Point(3, 25);
            this.cb_data_src.Name = "cb_data_src";
            this.cb_data_src.Size = new System.Drawing.Size(201, 21);
            this.cb_data_src.TabIndex = 62;
            this.cb_data_src.SelectedIndexChanged += new System.EventHandler(this.cb_data_src_SelectedIndexChanged);
            // 
            // tble_layout_panel
            // 
            this.tble_layout_panel.CausesValidation = false;
            this.tble_layout_panel.ColumnCount = 2;
            this.tble_layout_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.81818F));
            this.tble_layout_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 88.18182F));
            this.tble_layout_panel.Controls.Add(this.pnl_login_info, 1, 0);
            this.tble_layout_panel.Controls.Add(this.gb_equip_downtime, 1, 1);
            this.tble_layout_panel.Controls.Add(this.gb_production, 1, 2);
            this.tble_layout_panel.Controls.Add(this.gb_valid_well_test, 1, 3);
            this.tble_layout_panel.Controls.Add(this.panel_buttons, 1, 4);
            this.tble_layout_panel.Controls.Add(this.gb_tree, 0, 1);
            this.tble_layout_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tble_layout_panel.Location = new System.Drawing.Point(3, 79);
            this.tble_layout_panel.Name = "tble_layout_panel";
            this.tble_layout_panel.RowCount = 5;
            this.tble_layout_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7F));
            this.tble_layout_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29F));
            this.tble_layout_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29F));
            this.tble_layout_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29F));
            this.tble_layout_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6F));
            this.tble_layout_panel.Size = new System.Drawing.Size(1178, 679);
            this.tble_layout_panel.TabIndex = 70;
            this.tble_layout_panel.Paint += new System.Windows.Forms.PaintEventHandler(this.tble_layout_panel_Paint);
            // 
            // pnl_login_info
            // 
            this.pnl_login_info.Controls.Add(this.lbl_login_msg);
            this.pnl_login_info.Controls.Add(this.btn_login);
            this.pnl_login_info.Controls.Add(this.lbl_password);
            this.pnl_login_info.Controls.Add(this.lbl_user_nme);
            this.pnl_login_info.Controls.Add(this.tb_user_nme);
            this.pnl_login_info.Controls.Add(this.tb_password);
            this.pnl_login_info.Location = new System.Drawing.Point(142, 3);
            this.pnl_login_info.Name = "pnl_login_info";
            this.pnl_login_info.Size = new System.Drawing.Size(484, 40);
            this.pnl_login_info.TabIndex = 71;
            this.pnl_login_info.Visible = false;
            // 
            // lbl_login_msg
            // 
            this.lbl_login_msg.AutoSize = true;
            this.lbl_login_msg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_login_msg.ForeColor = System.Drawing.Color.Red;
            this.lbl_login_msg.Location = new System.Drawing.Point(65, 26);
            this.lbl_login_msg.Name = "lbl_login_msg";
            this.lbl_login_msg.Size = new System.Drawing.Size(285, 13);
            this.lbl_login_msg.TabIndex = 5;
            this.lbl_login_msg.Text = "Invalid Username or Password. Please Try again.";
            this.lbl_login_msg.Visible = false;
            // 
            // btn_login
            // 
            this.btn_login.Location = new System.Drawing.Point(394, 1);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(90, 23);
            this.btn_login.TabIndex = 4;
            this.btn_login.Text = "Login";
            this.btn_login.UseVisualStyleBackColor = true;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // lbl_password
            // 
            this.lbl_password.AutoSize = true;
            this.lbl_password.Location = new System.Drawing.Point(193, 6);
            this.lbl_password.Name = "lbl_password";
            this.lbl_password.Size = new System.Drawing.Size(56, 13);
            this.lbl_password.TabIndex = 3;
            this.lbl_password.Text = "Password:";
            // 
            // lbl_user_nme
            // 
            this.lbl_user_nme.AutoSize = true;
            this.lbl_user_nme.Location = new System.Drawing.Point(4, 6);
            this.lbl_user_nme.Name = "lbl_user_nme";
            this.lbl_user_nme.Size = new System.Drawing.Size(58, 13);
            this.lbl_user_nme.TabIndex = 2;
            this.lbl_user_nme.Text = "Username:";
            // 
            // tb_user_nme
            // 
            this.tb_user_nme.Location = new System.Drawing.Point(61, 2);
            this.tb_user_nme.MaxLength = 50;
            this.tb_user_nme.Name = "tb_user_nme";
            this.tb_user_nme.Size = new System.Drawing.Size(117, 20);
            this.tb_user_nme.TabIndex = 1;
            // 
            // tb_password
            // 
            this.tb_password.Location = new System.Drawing.Point(248, 2);
            this.tb_password.MaxLength = 50;
            this.tb_password.Name = "tb_password";
            this.tb_password.PasswordChar = '*';
            this.tb_password.Size = new System.Drawing.Size(117, 20);
            this.tb_password.TabIndex = 0;
            // 
            // gb_equip_downtime
            // 
            this.gb_equip_downtime.Controls.Add(this.dg_equip_reliblty);
            this.gb_equip_downtime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb_equip_downtime.Location = new System.Drawing.Point(142, 50);
            this.gb_equip_downtime.Name = "gb_equip_downtime";
            this.gb_equip_downtime.Size = new System.Drawing.Size(1033, 190);
            this.gb_equip_downtime.TabIndex = 0;
            this.gb_equip_downtime.TabStop = false;
            this.gb_equip_downtime.Text = "Equipment \\ Well Downtime";
            // 
            // dg_equip_reliblty
            // 
            this.dg_equip_reliblty.AllowUserToAddRows = false;
            this.dg_equip_reliblty.AllowUserToDeleteRows = false;
            this.dg_equip_reliblty.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_equip_reliblty.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dg_equip_reliblty.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_equip_reliblty.ContextMenuStrip = this.contxt_mnu_strip_grid;
            this.dg_equip_reliblty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_equip_reliblty.Location = new System.Drawing.Point(3, 16);
            this.dg_equip_reliblty.Name = "dg_equip_reliblty";
            this.dg_equip_reliblty.ReadOnly = true;
            this.dg_equip_reliblty.Size = new System.Drawing.Size(1027, 171);
            this.dg_equip_reliblty.TabIndex = 0;
            this.dg_equip_reliblty.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dg_equip_reliblty_DataError);
            // 
            // gb_production
            // 
            this.gb_production.Controls.Add(this.dg_prod_info);
            this.gb_production.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb_production.Location = new System.Drawing.Point(142, 246);
            this.gb_production.Name = "gb_production";
            this.gb_production.Size = new System.Drawing.Size(1033, 190);
            this.gb_production.TabIndex = 1;
            this.gb_production.TabStop = false;
            this.gb_production.Text = "Production";
            // 
            // dg_prod_info
            // 
            this.dg_prod_info.AllowUserToAddRows = false;
            this.dg_prod_info.AllowUserToDeleteRows = false;
            this.dg_prod_info.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_prod_info.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dg_prod_info.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_prod_info.ContextMenuStrip = this.contxt_mnu_strip_grid;
            this.dg_prod_info.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_prod_info.Location = new System.Drawing.Point(3, 16);
            this.dg_prod_info.Name = "dg_prod_info";
            this.dg_prod_info.ReadOnly = true;
            this.dg_prod_info.Size = new System.Drawing.Size(1027, 171);
            this.dg_prod_info.TabIndex = 0;
            this.dg_prod_info.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dg_prod_info_DataError);
            // 
            // gb_valid_well_test
            // 
            this.gb_valid_well_test.Controls.Add(this.dg_well_test);
            this.gb_valid_well_test.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb_valid_well_test.Location = new System.Drawing.Point(142, 442);
            this.gb_valid_well_test.Name = "gb_valid_well_test";
            this.gb_valid_well_test.Size = new System.Drawing.Size(1033, 190);
            this.gb_valid_well_test.TabIndex = 2;
            this.gb_valid_well_test.TabStop = false;
            this.gb_valid_well_test.Text = "Validated WellTests (Latest)";
            // 
            // dg_well_test
            // 
            this.dg_well_test.AllowUserToAddRows = false;
            this.dg_well_test.AllowUserToDeleteRows = false;
            this.dg_well_test.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dg_well_test.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dg_well_test.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_well_test.ContextMenuStrip = this.contxt_mnu_strip_grid;
            this.dg_well_test.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_well_test.Location = new System.Drawing.Point(3, 16);
            this.dg_well_test.Name = "dg_well_test";
            this.dg_well_test.ReadOnly = true;
            this.dg_well_test.Size = new System.Drawing.Size(1027, 171);
            this.dg_well_test.TabIndex = 0;
            this.dg_well_test.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dg_well_test_DataError);
            // 
            // panel_buttons
            // 
            this.panel_buttons.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_buttons.ColumnCount = 5;
            this.panel_buttons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panel_buttons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 181F));
            this.panel_buttons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.panel_buttons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.panel_buttons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 169F));
            this.panel_buttons.Controls.Add(this.btn_exp_prodml, 4, 0);
            this.panel_buttons.Controls.Add(this.btn_imp_prodml, 1, 0);
            this.panel_buttons.Controls.Add(this.btn_download_data, 2, 0);
            this.panel_buttons.Controls.Add(this.btn_exec_qry, 3, 0);
            this.panel_buttons.Controls.Add(this.cbx_export_rep_xmls, 0, 0);
            this.panel_buttons.Location = new System.Drawing.Point(141, 640);
            this.panel_buttons.Margin = new System.Windows.Forms.Padding(2);
            this.panel_buttons.Name = "panel_buttons";
            this.panel_buttons.RowCount = 1;
            this.panel_buttons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panel_buttons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.panel_buttons.Size = new System.Drawing.Size(1035, 37);
            this.panel_buttons.TabIndex = 3;
            this.panel_buttons.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_buttons_Paint);
            // 
            // btn_exp_prodml
            // 
            this.btn_exp_prodml.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_exp_prodml.Location = new System.Drawing.Point(869, 3);
            this.btn_exp_prodml.Name = "btn_exp_prodml";
            this.btn_exp_prodml.Size = new System.Drawing.Size(163, 31);
            this.btn_exp_prodml.TabIndex = 1;
            this.btn_exp_prodml.Text = "Create Report(PRODML)";
            this.btn_exp_prodml.UseVisualStyleBackColor = true;
            this.btn_exp_prodml.Click += new System.EventHandler(this.btn_exp_prodml_Click);
            // 
            // btn_imp_prodml
            // 
            this.btn_imp_prodml.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_imp_prodml.Enabled = false;
            this.btn_imp_prodml.Location = new System.Drawing.Point(328, 3);
            this.btn_imp_prodml.Name = "btn_imp_prodml";
            this.btn_imp_prodml.Size = new System.Drawing.Size(175, 31);
            this.btn_imp_prodml.TabIndex = 73;
            this.btn_imp_prodml.Text = "Save Data (database)";
            this.btn_imp_prodml.UseVisualStyleBackColor = true;
            this.btn_imp_prodml.Visible = false;
            this.btn_imp_prodml.Click += new System.EventHandler(this.btn_imp_prodml_Click);
            // 
            // btn_download_data
            // 
            this.btn_download_data.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_download_data.Location = new System.Drawing.Point(509, 3);
            this.btn_download_data.Name = "btn_download_data";
            this.btn_download_data.Size = new System.Drawing.Size(174, 31);
            this.btn_download_data.TabIndex = 74;
            this.btn_download_data.Text = "Save Data (as CSVs)";
            this.btn_download_data.UseVisualStyleBackColor = true;
            this.btn_download_data.Click += new System.EventHandler(this.btn_download_data_Click);
            // 
            // btn_exec_qry
            // 
            this.btn_exec_qry.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_exec_qry.Location = new System.Drawing.Point(689, 3);
            this.btn_exec_qry.Name = "btn_exec_qry";
            this.btn_exec_qry.Size = new System.Drawing.Size(174, 31);
            this.btn_exec_qry.TabIndex = 0;
            this.btn_exec_qry.Text = "Get Data";
            this.btn_exec_qry.UseVisualStyleBackColor = true;
            this.btn_exec_qry.Click += new System.EventHandler(this.btn_exec_qry_Click);
            // 
            // cbx_export_rep_xmls
            // 
            this.cbx_export_rep_xmls.AutoSize = true;
            this.cbx_export_rep_xmls.Location = new System.Drawing.Point(3, 3);
            this.cbx_export_rep_xmls.Name = "cbx_export_rep_xmls";
            this.cbx_export_rep_xmls.Size = new System.Drawing.Size(159, 17);
            this.cbx_export_rep_xmls.TabIndex = 72;
            this.cbx_export_rep_xmls.Text = "Include Hierarchy and Entity";
            this.cbx_export_rep_xmls.UseVisualStyleBackColor = true;
            // 
            // gb_tree
            // 
            this.gb_tree.Controls.Add(this.tv_hierarchy);
            this.gb_tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb_tree.Location = new System.Drawing.Point(3, 50);
            this.gb_tree.Name = "gb_tree";
            this.tble_layout_panel.SetRowSpan(this.gb_tree, 4);
            this.gb_tree.Size = new System.Drawing.Size(133, 626);
            this.gb_tree.TabIndex = 75;
            this.gb_tree.TabStop = false;
            this.gb_tree.Text = "Facility";
            // 
            // tv_hierarchy
            // 
            this.tv_hierarchy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tv_hierarchy.HideSelection = false;
            this.tv_hierarchy.Location = new System.Drawing.Point(3, 16);
            this.tv_hierarchy.Name = "tv_hierarchy";
            this.tv_hierarchy.Size = new System.Drawing.Size(127, 607);
            this.tv_hierarchy.TabIndex = 76;
            // 
            // rb_src_db
            // 
            this.rb_src_db.AutoSize = true;
            this.rb_src_db.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rb_src_db.Location = new System.Drawing.Point(3, 3);
            this.rb_src_db.Name = "rb_src_db";
            this.rb_src_db.Size = new System.Drawing.Size(201, 16);
            this.rb_src_db.TabIndex = 72;
            this.rb_src_db.TabStop = true;
            this.rb_src_db.Text = "Select data from DB";
            this.rb_src_db.UseVisualStyleBackColor = true;
            this.rb_src_db.CheckedChanged += new System.EventHandler(this.rb_src_db_CheckedChanged);
            // 
            // rb_src_files
            // 
            this.rb_src_files.AutoSize = true;
            this.rb_src_files.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rb_src_files.Location = new System.Drawing.Point(210, 3);
            this.rb_src_files.Name = "rb_src_files";
            this.rb_src_files.Size = new System.Drawing.Size(149, 16);
            this.rb_src_files.TabIndex = 73;
            this.rb_src_files.TabStop = true;
            this.rb_src_files.Text = "Select data from files";
            this.rb_src_files.UseVisualStyleBackColor = true;
            this.rb_src_files.CheckedChanged += new System.EventHandler(this.rb_src_files_CheckedChanged);
            // 
            // toolTip_btn
            // 
            this.toolTip_btn.AutoPopDelay = 5000;
            this.toolTip_btn.InitialDelay = 1000;
            this.toolTip_btn.ReshowDelay = 500;
            // 
            // cbx_user_entry
            // 
            this.cbx_user_entry.AutoSize = true;
            this.cbx_user_entry.Dock = System.Windows.Forms.DockStyle.Right;
            this.cbx_user_entry.Location = new System.Drawing.Point(1103, 25);
            this.cbx_user_entry.Name = "cbx_user_entry";
            this.cbx_user_entry.Size = new System.Drawing.Size(74, 44);
            this.cbx_user_entry.TabIndex = 76;
            this.cbx_user_entry.Text = "User input";
            this.cbx_user_entry.UseVisualStyleBackColor = true;
            this.cbx_user_entry.Visible = false;
            this.cbx_user_entry.CheckedChanged += new System.EventHandler(this.cbx_user_entry_CheckedChanged);
            // 
            // tlp_Top
            // 
            this.tlp_Top.ColumnCount = 5;
            this.tlp_Top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlp_Top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tlp_Top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp_Top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tlp_Top.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tlp_Top.Controls.Add(this.rb_src_db, 0, 0);
            this.tlp_Top.Controls.Add(this.rb_src_files, 1, 0);
            this.tlp_Top.Controls.Add(this.cbx_user_entry, 4, 1);
            this.tlp_Top.Controls.Add(this.cb_data_src, 0, 1);
            this.tlp_Top.Controls.Add(this.pictureBox1, 4, 0);
            this.tlp_Top.Controls.Add(this.dtp_report_dte, 2, 1);
            this.tlp_Top.Controls.Add(this.tb_citation_description, 3, 1);
            this.tlp_Top.Controls.Add(this.lbl_report_dte, 2, 0);
            this.tlp_Top.Controls.Add(this.lbl_citation_desc, 3, 0);
            this.tlp_Top.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp_Top.Location = new System.Drawing.Point(2, 2);
            this.tlp_Top.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tlp_Top.Name = "tlp_Top";
            this.tlp_Top.RowCount = 2;
            this.tlp_Top.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 31.42857F));
            this.tlp_Top.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 68.57143F));
            this.tlp_Top.Size = new System.Drawing.Size(1180, 72);
            this.tlp_Top.TabIndex = 77;
            // 
            // tlp_Main
            // 
            this.tlp_Main.ColumnCount = 1;
            this.tlp_Main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_Main.Controls.Add(this.tlp_Top, 0, 0);
            this.tlp_Main.Controls.Add(this.tble_layout_panel, 0, 1);
            this.tlp_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp_Main.Location = new System.Drawing.Point(0, 0);
            this.tlp_Main.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tlp_Main.Name = "tlp_Main";
            this.tlp_Main.RowCount = 2;
            this.tlp_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlp_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tlp_Main.Size = new System.Drawing.Size(1184, 761);
            this.tlp_Main.TabIndex = 78;
            // 
            // ProdmlView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 761);
            this.Controls.Add(this.tlp_Main);
            this.MinimumSize = new System.Drawing.Size(1200, 800);
            this.Name = "ProdmlView";
            this.Text = "PRODML File Generator";
            this.Load += new System.EventHandler(this.ProdmlView_Load);
            this.contxt_mnu_strip_grid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tble_layout_panel.ResumeLayout(false);
            this.pnl_login_info.ResumeLayout(false);
            this.pnl_login_info.PerformLayout();
            this.gb_equip_downtime.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_equip_reliblty)).EndInit();
            this.gb_production.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_prod_info)).EndInit();
            this.gb_valid_well_test.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_well_test)).EndInit();
            this.panel_buttons.ResumeLayout(false);
            this.panel_buttons.PerformLayout();
            this.gb_tree.ResumeLayout(false);
            this.tlp_Top.ResumeLayout(false);
            this.tlp_Top.PerformLayout();
            this.tlp_Main.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contxt_mnu_strip_grid;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyWithHeadersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.Label lbl_report_dte;
        private System.Windows.Forms.DateTimePicker dtp_report_dte;
        private System.Windows.Forms.Label lbl_citation_desc;
        private System.Windows.Forms.TextBox tb_citation_description;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox cb_data_src;
        private System.Windows.Forms.TableLayoutPanel tble_layout_panel;
        private System.Windows.Forms.GroupBox gb_equip_downtime;
        private System.Windows.Forms.DataGridView dg_equip_reliblty;
        private System.Windows.Forms.GroupBox gb_production;
        private System.Windows.Forms.DataGridView dg_prod_info;
        private System.Windows.Forms.DataGridView dg_well_test;
        private System.Windows.Forms.TableLayoutPanel panel_buttons;
        private System.Windows.Forms.Button btn_exp_prodml;
        private System.Windows.Forms.Button btn_exec_qry;
        private System.Windows.Forms.Label lbl_password;
        private System.Windows.Forms.Label lbl_user_nme;
        private System.Windows.Forms.TextBox tb_user_nme;
        private System.Windows.Forms.TextBox tb_password;
        private System.Windows.Forms.Button btn_login;
        private System.Windows.Forms.Label lbl_login_msg;
        private System.Windows.Forms.CheckBox cbx_export_rep_xmls;
        private System.Windows.Forms.Button btn_imp_prodml;
        private System.Windows.Forms.RadioButton rb_src_db;
        private System.Windows.Forms.RadioButton rb_src_files;
        private System.Windows.Forms.Button btn_download_data;
        private System.Windows.Forms.ToolTip toolTip_btn;
        private System.Windows.Forms.CheckBox cbx_user_entry;
        protected System.Windows.Forms.GroupBox gb_valid_well_test;
        private System.Windows.Forms.TreeView tv_hierarchy;
        private System.Windows.Forms.GroupBox gb_tree;
        private System.Windows.Forms.TableLayoutPanel tlp_Top;
        private System.Windows.Forms.Panel pnl_login_info;
        private System.Windows.Forms.TableLayoutPanel tlp_Main;
    }
}

